/**
 * 
 */
package com.navatar.generic;

import static com.navatar.generic.CommonLib.*;

import java.util.ArrayList;
import java.util.List;

import com.navatar.generic.EnumConstants.excelLabel;
import com.navatar.scripts.SmokeTestCases;

import static com.navatar.generic.BaseLib.*;



public class SmokeCommonVariables {
	
	public static String appName;
	public static String superAdminUserName,superAdminRegistered,superAdminFirstName,superAdminLastName,adminPassword,browserToLaunch,BoxUserEmail,BoxPassword;
	public static String crmUser1FirstName,crmUser1LastName,crmUser1EmailID,crmUserProfile,crmUserLience;
	
	public static String externalUserFirstName,externalUserLastName,externalUserEmailID, externalUserProfile,externalUserLience,externalUserRole;
	
	public static String gmailUserName,gmailUserName2,gmailPassword;
	
	public static String SmokeINS1,SmokeINS1_RecordType;
	public static String SmokeCOM1,SmokeCOM1_RecordType;
	public static String SmokeINS2,SmokeINS2_RecordType;
	
	public static String Smoke_PL1Name,Smoke_PL1CompanyName,Smoke_PL1Stage,Smoke_PL1RecordType;
	
	public static String Smoke_Project1Name,Smoke_Project1Type,Smoke_Project1PipeLineName;
	public static String Smoke_Project2Name,Smoke_Project2Type,Smoke_Project2PipeLineName;
	
	public static String Smoke_Task1Name,Smoke_Task1ProjectName,Smoke_Task1Status,smoke_Task1Start_Date,Smoke_Task1End_Date;
	public static String Smoke_Task2Name,Smoke_Task2ProjectName,Smoke_Task2Status;
	public static String Smoke_Task3Name,Smoke_Task3ProjectName,Smoke_Task3Status;
	public static String Smoke_Task4Name,Smoke_Task4ProjectName,Smoke_Task4Status;
	public static String Smoke_Task5Name,Smoke_Task5ProjectName,Smoke_Task5Status;
	public static String Smoke_Task6Name,Smoke_Task6ProjectName,Smoke_Task6Status;
	
	public static String Smoke_Fund1,Smoke_Fund1RecordType,Smoke_Fund1Type,Smoke_Fund1InvestmentCategory,Smoke_Fund1Size,Smoke_Fund1VintageYear,Smoke_Fund1Contact,Smoke_Fund1Phone,Smoke_Fund1Email,Smoke_Fund1Description;
	
	public static String Smoke_FR1Name,Smoke_FR1FundName,Smoke_FR1LegalName,Smoke_FR1RecordType;
	
	public static String Smoke_PartnerShip1Name,Smoke_PartnerShip1FundName,Smoke_PartnerShip1RecordType;
	
	public static String Smoke_LP1Name,Smoke_LP1InstitutionName,Smoke_LP1RecordType;
	
	public static String SmokeCOMM1_ID,SmokeCOMM1_LPName,SmokeCOMM1_PRName,Smoke_COMM1RecordType;
	
	public static String NavatarStepPageURL;
	
	
	
	public SmokeCommonVariables(Object obj) {
		// TODO Auto-generated constructor stub
		long StartTime = System.currentTimeMillis();
		if(obj instanceof SmokeTestCases){
		System.err.println("smokeExcelPathCommonVariable : "+testCasesFilePath);
		
		//****************************************************************	SuperAdmin And CRM User **********************************************************//
		
		superAdminUserName=ExcelUtils.readDataFromPropertyFile("SuperAdminUsername");
		superAdminRegistered=ExcelUtils.readDataFromPropertyFile("SuperAdminRegistered");
		BoxUserEmail=ExcelUtils.readDataFromPropertyFile("BoxUserEmail");
		BoxPassword=ExcelUtils.readDataFromPropertyFile("boxPassword");
		adminPassword=ExcelUtils.readDataFromPropertyFile("password");
		gmailUserName=ExcelUtils.readDataFromPropertyFile("gmailUserName");
		gmailUserName2=ExcelUtils.readDataFromPropertyFile("gmailUserName2");
		gmailPassword=ExcelUtils.readDataFromPropertyFile("gmailPassword");
		NavatarStepPageURL=ExcelUtils.readDataFromPropertyFile("NavatarStepPageURL");
		superAdminFirstName= ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "AdminUser", excelLabel.User_First_Name); 
		superAdminLastName= ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "AdminUser", excelLabel.User_Last_Name); 
		crmUser1FirstName=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User1", excelLabel.User_First_Name);
		crmUser1LastName=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User1", excelLabel.User_Last_Name);
		crmUser1EmailID=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User1", excelLabel.User_Email);
		crmUserProfile=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User1", excelLabel.User_Profile);
		crmUserLience=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User1", excelLabel.User_License);
		
		externalUserFirstName=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.User_First_Name);
		externalUserLastName=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.User_Last_Name);
		externalUserEmailID=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.User_Email);
		externalUserProfile=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.User_Profile);
		externalUserLience=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.User_License);
		externalUserRole=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.Role);
		
		browserToLaunch = ExcelUtils.readDataFromPropertyFile("Browser");
		appName=ExcelUtils.readDataFromPropertyFile("appName");
		//****************************************************************	Institution,Company **********************************************************//
		
		//COM1............
		SmokeCOM1=ExcelUtils.readData(testCasesFilePath,"Institutions",excelLabel.Variable_Name, "SmokeINS2", excelLabel.Institutions_Name);
		SmokeCOM1_RecordType=ExcelUtils.readData(testCasesFilePath,"Institutions",excelLabel.Variable_Name, "SmokeINS2", excelLabel.Record_Type);
		
		//INS1..............
		SmokeINS1=ExcelUtils.readData(testCasesFilePath,"Institutions",excelLabel.Variable_Name, "SmokeINS1", excelLabel.Institutions_Name);
		SmokeINS1_RecordType=ExcelUtils.readData(testCasesFilePath,"Institutions",excelLabel.Variable_Name, "SmokeINS1", excelLabel.Record_Type);
		SmokeINS2=ExcelUtils.readData(testCasesFilePath,"Institutions",excelLabel.Variable_Name, "SmokeINS2", excelLabel.Institutions_Name);
		SmokeINS2_RecordType=ExcelUtils.readData(testCasesFilePath,"Institutions",excelLabel.Variable_Name, "SmokeINS2", excelLabel.Record_Type);
		
	
		//***************************************** PipeLine *************************************************//
		
		
		Smoke_PL1Name = ExcelUtils.readData(testCasesFilePath,"PipeLine",excelLabel.Variable_Name, "SmokePL1", excelLabel.Pipeline_Name);
		Smoke_PL1CompanyName = ExcelUtils.readData(testCasesFilePath,"PipeLine",excelLabel.Variable_Name, "SmokePL1", excelLabel.Company_Name);
		Smoke_PL1Stage = ExcelUtils.readData(testCasesFilePath,"PipeLine",excelLabel.Variable_Name, "SmokePL1", excelLabel.Stage);

		Smoke_PL1RecordType=ExcelUtils.readData(testCasesFilePath,"PipeLine",excelLabel.Variable_Name, "SmokePL1", excelLabel.Record_Type);
		//***************************************** Project *************************************************//
		
			
		Smoke_Project1Name = ExcelUtils.readData(testCasesFilePath,"Project",excelLabel.Variable_Name, "SmokePR1", excelLabel.Project_Name);
		Smoke_Project1Type = ExcelUtils.readData(testCasesFilePath,"Project",excelLabel.Variable_Name, "SmokePR1", excelLabel.Project_Type);
		Smoke_Project1PipeLineName = ExcelUtils.readData(testCasesFilePath,"Project",excelLabel.Variable_Name, "SmokePR1", excelLabel.Pipeline_Name);

		Smoke_Project2Name = ExcelUtils.readData(testCasesFilePath,"Project",excelLabel.Variable_Name, "SmokePR2", excelLabel.Project_Name);
		Smoke_Project2Type = ExcelUtils.readData(testCasesFilePath,"Project",excelLabel.Variable_Name, "SmokePR2", excelLabel.Project_Type);
		Smoke_Project2PipeLineName = ExcelUtils.readData(testCasesFilePath,"Project",excelLabel.Variable_Name, "SmokePR2", excelLabel.Pipeline_Name);

		//***************************************** Task *************************************************//
		
		Smoke_Task1Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask1", excelLabel.Task_Name);
		smoke_Task1Start_Date=ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask1", excelLabel.Start_Date);
		Smoke_Task1End_Date=ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask1", excelLabel.End_Date);
		Smoke_Task1ProjectName = Smoke_Project1Name;
		Smoke_Task1Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask1", excelLabel.Status);
		
		Smoke_Task2Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask2", excelLabel.Task_Name);
		Smoke_Task2ProjectName = Smoke_Project2Name;
		Smoke_Task2Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask2", excelLabel.Status);
		
		
		Smoke_Task3Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask3", excelLabel.Task_Name);
		Smoke_Task3ProjectName = Smoke_Project1Name;
		Smoke_Task3Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask3", excelLabel.Status);
		
		Smoke_Task4Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask4", excelLabel.Task_Name);
		Smoke_Task4ProjectName = Smoke_Project2Name;
		Smoke_Task4Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask4", excelLabel.Status);
		
		Smoke_Task5Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask5", excelLabel.Task_Name);
		Smoke_Task5ProjectName = Smoke_Project1Name;
		Smoke_Task5Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask5", excelLabel.Status);
		
		Smoke_Task6Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask6", excelLabel.Task_Name);
		Smoke_Task6ProjectName =Smoke_Project2Name;
		Smoke_Task6Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask6", excelLabel.Status);

		
		
		//***************************************** Fund *************************************************//
			
		Smoke_Fund1=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Name);
		Smoke_Fund1Type=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Type);
		Smoke_Fund1InvestmentCategory=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Investment_Category);
		Smoke_Fund1Size=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Size);
		Smoke_Fund1VintageYear=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1",excelLabel.Vintage_Year);
		Smoke_Fund1Contact=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Contact);
		Smoke_Fund1Phone=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Phone);
		Smoke_Fund1Email=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1",excelLabel.Fund_Email);
		Smoke_Fund1Description=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1",excelLabel.Fund_Description);	
		Smoke_Fund1RecordType=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Record_Type);	
		
		//**********************************************************FundRaising***************************/
		
		Smoke_FR1Name = ExcelUtils.readData(testCasesFilePath,"Fundraisings",excelLabel.Variable_Name, "SmokeFR1", excelLabel.FundRaising_Name);
		Smoke_FR1FundName=Smoke_Fund1;	
		Smoke_FR1LegalName=SmokeINS1;
		Smoke_FR1RecordType=ExcelUtils.readData(testCasesFilePath,"Fundraisings",excelLabel.Variable_Name, "SmokeFR1", excelLabel.Record_Type);
		
		//******************************PartnerShip*******************************//
		
		Smoke_PartnerShip1Name=ExcelUtils.readData(testCasesFilePath, "Partnerships", excelLabel.Variable_Name,"SmokeP1", excelLabel.PartnerShip_Name);
		Smoke_PartnerShip1FundName=Smoke_Fund1;
		Smoke_PartnerShip1RecordType=ExcelUtils.readData(testCasesFilePath,"Partnerships",excelLabel.Variable_Name, "SmokeP1", excelLabel.Record_Type);
		

		//****************************LP***********************************//
		
		Smoke_LP1InstitutionName=SmokeINS1;
		Smoke_LP1Name=ExcelUtils.readData(testCasesFilePath, "Institutions", excelLabel.Variable_Name,"SmokeLP1", excelLabel.Institutions_Name);
		Smoke_LP1RecordType=ExcelUtils.readData(testCasesFilePath, "Institutions", excelLabel.Variable_Name,"SmokeLP1", excelLabel.Record_Type);
		//******************************************* Commitment ***************************************
		SmokeCOMM1_ID=ExcelUtils.readData(testCasesFilePath, "Commitments", excelLabel.Variable_Name,"SmokeCOMM1", excelLabel.Commitment_ID);
		SmokeCOMM1_LPName=Smoke_LP1Name;
		SmokeCOMM1_PRName=Smoke_PartnerShip1Name;  	
		Smoke_COMM1RecordType=ExcelUtils.readData(testCasesFilePath,"Commitments",excelLabel.Variable_Name, "SmokeCOMM1", excelLabel.Record_Type);
		
		AppListeners.appLog.info("Done with intialization in Smoke Test Variable. Enjoy the show.\nTotal Time Taken: "+((System.currentTimeMillis()-StartTime)/1000)+" seconds.");
		}
			
	}
		

	
}

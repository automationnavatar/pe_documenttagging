package com.navatar.pageObjects;

public interface FundsPageErrorMessage {
public String yourSettingsWillOnlyAllowTagByPipeline="Your current settings only allow you to tag documents from a Pipeline. Please contact your Navatar Administrator";
public String yourSettingsWillOnlyAllowTagByFund="Your current settings only allow you to tag documents from a Fund. Please contact your Navatar Administrator";
//public static String ErrorMsgBeforeRegistration="Please register for Navatar Investor from the Navatar Investor Manager tab in order to view this information.";
public static String NoWorkSpaceBuildErrorMsg="No Workspace has been built for this Fund.";
public static String ErrorMsgBeforeRegisterCRMUser="Please register for Navatar Investor from the Navatar Investor Manager tab in order to view this information.";
public String DocumentUploadSuccessMsg = "Document(s) uploaded successfully.";
public String clearWorkSpaceMsg="Your Workspace has been cleared successfully. Please Refresh the page.";
public static String ErrorMsgBeforeRegistration="You have not been provided access to the Navatar Investor functionality by your Navatar Administrator.";

}

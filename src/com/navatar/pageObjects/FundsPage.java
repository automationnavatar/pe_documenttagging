package com.navatar.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.action;

import static com.navatar.generic.CommonLib.*;

import java.util.List;

public class FundsPage extends BasePageBusinessLayer {

	public FundsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	@FindBy(xpath = "//span[text()='Fund']/ancestor::article//span[text()='View All']")
	private WebElement fundsPageViewAll_Lightning;
	/**
	 * @return the fundsPageViewAll_Lightning
	 */
	public WebElement getFundsPageViewAll_Lightning(int timeOut) {
		return isDisplayed(driver, fundsPageViewAll_Lightning, "Visibility", timeOut, "View all button on lightning");
		}
	@FindBy(xpath = "//a[@title='Create Drawdown']")
	private WebElement createFundDrawdownButton_Ligh;
	

	
	@FindBy(xpath = "//td[@id='topButtonRow']//input[@title='Create Drawdown']")
	private WebElement createdFundDrawdownButton_classic;
	
	/**
	 * @return the createdFundDrawdownButton_classic
	 */
	public WebElement getCreatedFundDrawdownButton(String mode, int timeOut) {
		if (mode.equalsIgnoreCase(Mode.Classic.toString()))
		return isDisplayed(driver, createdFundDrawdownButton_classic, "Visibility", timeOut, "Fund Drawdown Classic");
	
		else
		return isDisplayed(driver, createFundDrawdownButton_Ligh, "Visibility", timeOut, "Fund Drawdown Lightning");
	
	}
	@FindBy(xpath = "//td[@id='topButtonRow']//input[@title='Create Distribution']")
	private WebElement createFundDistributionButton_classic;
	

	@FindBy(xpath = "//a[@title='Create Distribution']")
	private WebElement createFundDistributionButton_Ligh;
	
	public WebElement getCreatedFundDistributionButton(String mode, int timeOut) {
		if (mode.equalsIgnoreCase(Mode.Classic.toString()))
		return isDisplayed(driver, createFundDistributionButton_classic, "Visibility", timeOut, "Fund Distribution Classic");
	
		else
		return isDisplayed(driver, createFundDistributionButton_Ligh, "Visibility", timeOut, "Fund Distribution Lightning");
	
	}

	@FindBy(xpath="//input[@name='Name']")
	private WebElement fundName_Classic;
	
	@FindBy(xpath="//*[text()='Fund Name']/following-sibling::*//input")
	private WebElement fundName_Lighting;

	/**
	 * @return the fundName
	 */
	public WebElement getFundName(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, fundName_Classic, "Visibility", timeOut, "Fund Name Classic");
		}else{
			return isDisplayed(driver, fundName_Lighting, "Visibility", timeOut, "Fund Name Lighting");
		}
		
	}
	
	@FindBy(xpath="//div[@class='requiredInput']//select")
	private WebElement fundType_Classic;
	
	@FindBy(xpath="//*[text()='Fund Type']/following-sibling::div[@class='slds-form-element__control']//button")
	private WebElement fundType_Lighting;

	/**
	 * @return the fundType
	 */
	public WebElement getFundType(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, fundType_Classic, "Visibility", timeOut, "Fund Type Classic");
		}else{
			return isDisplayed(driver, fundType_Lighting, "Visibility", timeOut, "Fund Type Lighting");
		}
		
	}
	
	@FindBy(xpath="(//div[@class='requiredInput']//select)[2]")
	private WebElement investmentCategory_Classic;
	
	@FindBy(xpath="//*[text()='Investment Category']/following-sibling::div//button")
	private WebElement investmentCategory_Lighting;

	/**
	 * @return the investmentCategory
	 */
	public WebElement getInvestmentCategory(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, investmentCategory_Classic, "Visibility", timeOut, "Investment Category Classic");
		}else{
			return isDisplayed(driver, investmentCategory_Lighting, "Visibility", timeOut, "Investment Category Lighting");
		}
	} 
	
	
	public WebElement getFundNameAtFundPage(String fundName,int timeOut){
		WebElement ele = FindElement(driver,
				"//div[@class='x-panel-bwrap']//a//span[contains(text(),'" + fundName + "')]", "Fund Name",
				action.SCROLLANDBOOLEAN, 60);
		
		return isDisplayed(driver, ele, "Visibility", timeOut, "Select all check box");
	}
	
	public WebElement getFundtPageTextBoxOrRichTextBoxWebElement(String environment,String mode, String labelName, int timeOut) {
		WebElement ele=null;
		String xpath ="",inputXpath="", dateXpath="",finalXpath="",finalLabelName="";
		if(labelName.contains("_")) {
			finalLabelName=labelName.replace("_", " ");
		}else {
			finalLabelName=labelName;
		}
		if(mode.equalsIgnoreCase(Mode.Classic.toString())) {
			xpath="//label[contains(text(),'"+finalLabelName+"')]";
			inputXpath="/../following-sibling::td/input";
			dateXpath="/../following-sibling::td[1]/span/input";
		}else {
			//span[text()='Description']/..//following-sibling::textarea
			xpath="//span[contains(text(),'"+finalLabelName+"')]";
			inputXpath="/..//following-sibling::input";
			dateXpath="/../following-sibling::div/input";
		}
		if(labelName.contains("Date")) {
			finalXpath=xpath+dateXpath;
		}else {
			finalXpath=xpath+inputXpath;
		}
		ele=isDisplayed(driver, FindElement(driver, finalXpath, finalLabelName+" text box in "+mode, action.SCROLLANDBOOLEAN,30), "Visibility", timeOut, finalLabelName+"text box in "+mode);
		return ele;
	}
	
	
	@FindBy(xpath="//iframe[@title='Email Fundraisings']")
	private WebElement emailFundraisingContactFrame_Lightning;
	
	/**
	 * @return the emailFundraisingContactFrame_Lightning
	 */
	public WebElement getEmailFundraisingContactFrame_Lightning(int timeOut) {
		return isDisplayed(driver, emailFundraisingContactFrame_Lightning, "Visibility", timeOut, "email Fundraising Contact Frame Lightning");
	}

	@FindBy(xpath="//input[@title='Email Fundraising Contacts']")
	private WebElement emailFundraisingContactsBtn_Classic;

	@FindBy(xpath="//a[@title='Email Fundraising Contacts']")
	private WebElement emailFundraisingContactsBtn_Lightning;
	/**
	 * @return the emailFundraisingContactsBtn
	 */
	public WebElement getEmailFundraisingContactsBtn(String environment, String mode,int timeOut) {
		if(mode.toString().equalsIgnoreCase(Mode.Lightning.toString())) {
			return isDisplayed(driver, emailFundraisingContactsBtn_Lightning, "Visibility", timeOut, "email fundraising contact button in "+mode);
		}else {
			return isDisplayed(driver, emailFundraisingContactsBtn_Classic, "Visibility", timeOut, "email fundraising contact button in "+mode);
		}
	}
	
	@FindBy(xpath = "//span[text()='Tasks']/..")
	private WebElement tasksTab;
	
	public WebElement gettasksTab(int timeOut) {
		return isDisplayed(driver, tasksTab, "Visibility", timeOut, "tasksTab in funds page");
	}
	
	@FindBy(xpath = "//div[@class='slds-p-around_medium']/p[2]")
	private WebElement NoWorkSpaceBuildErrorMessageOnDocumentTaggingPage;
	
	
	public WebElement getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(int timeOut) {
		return isDisplayed(driver, NoWorkSpaceBuildErrorMessageOnDocumentTaggingPage, "Visibility", timeOut, "No WorkSpace Build Error Message On Document Tagging Page");
	}
	
	
	public WebElement getBuildWorkspaceButton(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "Fundraising";
		} else {
			workspaceSelector = "Investor";
		}
		return isDisplayed(driver, FindElement(driver, "//input[@value='Build "+workspaceSelector+" Workspace']", "Build Workspace Button", action.BOOLEAN, 30), "visibility", 60, "Build Workspace Button");
	}
	
	public WebElement getSizeInMillionTextBox(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//input[@id='txtFundSize"+workspaceSelector+"']", "Size in million", action.BOOLEAN, 30), "visibility", 60, "Size in million");
	}
	
	public WebElement getVintageYear(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//input[@id='txtVintageYr"+workspaceSelector+"']", "Vintage Year", action.BOOLEAN, 30), "visibility", 60, "Vintage Year");
	}
	
	public WebElement getContactTextBox(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//input[@id='txtFundContact"+workspaceSelector+"']", "Contact Text Box", action.BOOLEAN, 30), "visibility", 60, "Contact text Box");
	}
	
	public WebElement getPhoneTextBox(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//input[@id='txtFundPhone"+workspaceSelector+"']", "Contact Text Box", action.BOOLEAN, 30), "visibility", 60, "Contact text Box");
	}
	
	public WebElement getEmailTextBox(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//input[@id='txtFundEmail"+workspaceSelector+"']", "Contact Text Box", action.BOOLEAN, 30), "visibility", 60, "Contact text Box");
	}
	
	public WebElement getDescriptionTextBox(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//textarea[@id='txtFundDesc"+workspaceSelector+"']", "Description Text Box", action.BOOLEAN, 30), "visibility", 60, "Description text Box");
	}
	
	public WebElement getNext1Of3Button(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "FR:BWFR";
		} else {
			workspaceSelector = "Inv:BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//input[contains(@name,'page:form"+workspaceSelector+":CompBuiltWorkspace')][@value='Next']", "Next 1 Of 3", action.BOOLEAN, 30), "visibility", 60, "Next 1 Of 3");
	}
	
	public WebElement getImportFolderTemplateButton(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//a[@title='Import Folder Template'][contains(@onclick,'importBtnclick"+workspaceSelector+"')]", "Import Folder template Button", action.BOOLEAN, 30), "visibility", 60, "Import Folder template Button");
	}
	
	public WebElement getDisplayDropDown(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "FR:BWFR";
		} else {
			workspaceSelector = "Inv:BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//select[@id='page:form"+workspaceSelector+":CompBuiltWorkspace:selectlstId']", "Display Drop Down", action.BOOLEAN, 30), "visibility", 60, "Display Drop Down");
	}
	
	public WebElement getFolderTemplateRadioButton(String folderTemplateName, Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "(//span[@title='"+folderTemplateName+"']/../preceding-sibling::span)[2]/input[contains(@onclick,'selectTemplate"+workspaceSelector+"')]", folderTemplateName+" Template Raido Button", action.BOOLEAN, 30), "visibility", 60, folderTemplateName+" Template Raido Button");
	}
	
	public WebElement getFolderTemplateImportButton(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//a[@id='ImportActive"+workspaceSelector+"']", "Folder Template Import Button", action.BOOLEAN, 30), "visibility", 60, "Folder Template Import Button");
	}
	
	public WebElement getNext2Of3Button(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//a[@onclick='showPopup3"+workspaceSelector+"(event);return false;']", "Next 2 Of 3", action.BOOLEAN, 30), "visibility", 60, "Next 2 Of 3");
	}
	
	public WebElement getDone3Of3Button(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "FR:BWFR";
		} else {
			workspaceSelector = "Inv:BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//input[contains(@name,'page:form"+workspaceSelector+":CompBuiltWorkspace')][@value='Done']", "Next 3 Of 3", action.BOOLEAN, 30), "visibility", 60, "Next 3 Of 3");
	}
	
	public WebElement getInstituionCheckBoxOn3Of3(String instituionName, Workspace workspace, int timeOut){
		return isDisplayed(driver, FindElement(driver, "(//div[@title='"+instituionName+"']/../preceding-sibling::span)[2]/input", instituionName+" Institution Check Box", action.BOOLEAN, timeOut), "Visibility", timeOut, instituionName+" Institution Check Box");
	}
	
	public WebElement getLimitedPartnerCheckBox(String InstitutionName, String LPName, Workspace workspace, int timeOut){
		if(click(driver, FindElement(driver, "//label[@title='"+InstitutionName+"']", InstitutionName+" Instituion Folder", action.BOOLEAN, timeOut), InstitutionName+" Institution Folder", action.BOOLEAN)){
			return isDisplayed(driver, FindElement(driver, "//label[@title='"+InstitutionName+"']/../../../following-sibling::ul/li/div/a//label[@title='"+LPName+"']/../../preceding-sibling::input", LPName+" LP Check Box", action.BOOLEAN, timeOut), "Visibility", timeOut, LPName+" LP Check Box");
		}
		return null;
	}
	
	public WebElement getCongratulationsCloseButton(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//div[text()='Workspace Built ']/following-sibling::div[2]/a[contains(@onclick,'load"+workspaceSelector+"')]", "Congratulations Close Button", action.BOOLEAN, timeOut/2), "visibility", timeOut/2, "Congratulations Close Button");
	}
	
	public WebElement getCongratulationsMessage(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//div[text()='Workspace Built ']/following-sibling::div[2]/a[contains(@onclick,'load"+ workspaceSelector +"')]/../preceding-sibling::div[1]", "Congratulations Close Button", action.BOOLEAN, timeOut/2), "visibility", timeOut/2, "Congratulations message");
	}
	
	public WebElement getCongratulationsHeader(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//div[text()='Workspace Built ']/following-sibling::div[2]/a[contains(@onclick,'load"+ workspaceSelector +"')]/../preceding-sibling::div[2]", "Congratulations Header", action.BOOLEAN, timeOut/2), "visibility", timeOut/2, "Congratulations Header");
	}
	
	public WebElement getCongratulationsCrossIcon(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "BWFR";
		} else {
			workspaceSelector = "BWINV";
		}
		return isDisplayed(driver, FindElement(driver, "//div[text()='Workspace Built ']/following-sibling::div[2]/a[contains(@onclick,'load"+ workspaceSelector +"')]/../preceding-sibling::div[2]/a", "Congratulations cross icon", action.BOOLEAN, timeOut/2), "visibility", timeOut/2, "Congratulations Cross Icon");
	}
	
	public WebElement getInvestorAddedConfirmationCloseButton(Workspace workspace, int timeOut){
		String workspaceSelector = "";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "FR";
		} else {
			workspaceSelector = "INV";
		}
		return isDisplayed(driver, FindElement(driver, "(//div[text()='Confirmation ']/following-sibling::div/a[contains(@onclick,'MIidMIN"+workspaceSelector+"')])[1]", workspace+" view section", action.BOOLEAN, 30), "visibility", 60, workspace+" view section");
	}
	
	public WebElement getUploadIcon(Workspace workspace, int timeOut){
		String workspaceSelector="";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
			workspaceSelector = "fr";
		} else {
			workspaceSelector = "inv";
		}
		return isDisplayed(driver, FindElement(driver, "//div[@id='"+workspaceSelector+"workspace']//a[@title='Upload Documents']", "Upload Icon", action.BOOLEAN, timeOut), "Visibility", timeOut, "Upload Icon");
	}
	@FindBy(xpath="//label[@for='rbtMultipleInstitution']/preceding-sibling::input")
	private WebElement multipleInstituionRadioButton;

	/**
	 * @return the multipleInstituionRadioButton
	 */
	public WebElement getMultipleInstituionRadioButton(int timeOut) {
		return isDisplayed(driver, multipleInstituionRadioButton, "Visibility", timeOut, "Multiple institution radio button");
	}
	
	@FindBy(xpath="//label[@for='rbtBulkUploader']/preceding-sibling::input")
	private WebElement bulkUploaderOrFileSplitterRadioButton;

	/**
	 * @return the bulkUploaderOrFileSplitterRadioButton
	 */
	public WebElement getBulkUploaderOrFileSplitterRadioButton(int timeOut) {
		return isDisplayed(driver, bulkUploaderOrFileSplitterRadioButton, "Visibility", timeOut, "Bulk Uploader or file splitter radio button");
	}
	
	@FindBy(css="#btnSave")
	private WebElement uploadSaveButton;

	/**
	 * @return the uploadSaveButton
	 */
	public WebElement getUploadSaveButton(int timeOut) {
		return isDisplayed(driver, uploadSaveButton, "Visibility", timeOut, "Save Button");
	}
	
	@FindBy(xpath="//a[@title='Update All']")
	private WebElement uploadWinUpdateButton;

	/**
	 * @return the uploadWinUpdateButton
	 */
	public WebElement getUploadWinUpdateButton(int timeOut) {
		return isDisplayed(driver, uploadWinUpdateButton, "Visibility", timeOut, "Update All button");
	}
	
	@FindBy(xpath="//a[@id='btnNext']")
	private WebElement fWR_uploadNextBtn;
	
	@FindBy(xpath="//a[@id='btnNextInvestor']")
	private WebElement INV_uploadNextBtn;

	/**
	 * @return the uploadNextButton
	 */
	public WebElement getUploadNextButton(Workspace workspace,int timeOut) {
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())) {
			return isDisplayed(driver, fWR_uploadNextBtn, "Visibility", timeOut, "Upload Window Next Button");			
		}else {
			return isDisplayed(driver, INV_uploadNextBtn, "Visibility", timeOut, "Upload Window Next Button");	
		}
	}
	
	public List<WebElement> draggedFilesInFileUploadAtCRMSide(){
		return FindElements(driver, "(//div[@id='divselectInvestor']//ul)[2]/li/b", "Dragged Files in Uplod Files At CRM Side");
	}
	
	
	@FindBy(xpath = "//span[text()='Please select a Workspace']")
	private WebElement pleaseSelectWorkSpaceLabelText;
	
	
	public WebElement getPleaseSelectWorkSpaceLabelText(int timeOut) {
		return isDisplayed(driver, pleaseSelectWorkSpaceLabelText, "Visibility", timeOut, "please select workspace label text");	
	}
	
	public WebElement getWorkSpaceRadioButtonOnTagDocumentPopUp(Workspace workspace, int timeOut) {
		String xpath="";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())) {
			 xpath ="//li[contains(text(),'Fundraising Workspace')]//input";
		}else {
			xpath ="//li[contains(text(),'Investor Workspace')]//input";
		}
		return FindElement(driver,xpath,workspace+" radio button xpath ", action.BOOLEAN, timeOut);
		
	}
	
	@FindBy(xpath = "//button[@class='btn btnWhite']")
	private WebElement cancelButtOnPleaseSelectWorkOnTagDocumentPopUp;
	
	public WebElement getCancelButtOnPleaseSelectWorkOnTagDocumentPopUp(int timeOut) {
		return isDisplayed(driver, cancelButtOnPleaseSelectWorkOnTagDocumentPopUp, "Visibility", timeOut, "please select workspace cancel button");	
	}
	
	@FindBy(xpath = "//button[@class='btn btnBlue']")
	private WebElement continueButtOnPleaseSelectWorkOnTagDocumentPopUp;
	
	
	public WebElement getContinueButtOnPleaseSelectWorkOnTagDocumentPopUp(int timeOut) {
		return isDisplayed(driver, continueButtOnPleaseSelectWorkOnTagDocumentPopUp, "Visibility", timeOut, "please select workspace continue button");	
	}
	
	public WebElement getWorkSpaceClearBtn(Workspace workspace,int timeOut) {
		String workSpaceSelector="";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())) {
			workSpaceSelector="fr";
		}else {
			workSpaceSelector="inv";
		}
		return isDisplayed(driver, FindElement(driver, "//div[@id='"+workSpaceSelector+"workspace']//a[@title='Clear Workspace']","workspace clear button", action.BOOLEAN,timeOut), "visibility", timeOut,"workspace close button");
	}
	
	@FindBy(xpath = "//div[@id='create_savebtn']/a[contains(text(),'Yes')]")
	private WebElement clearWorkSpaceYesBtn;
	
	
	public WebElement getClearWorkSpaceYesBtn(int timeOut) {
		  return isDisplayed(driver, clearWorkSpaceYesBtn, "Visibility", timeOut, "clear workspace yes button");
	}
	
	public WebElement getBuildWorkSpace2Of3LabelText(Workspace workspace, int timeOut) {
		String xpath="";
		if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())) {
			xpath="FR";
		}else {
			xpath="INV";
		}
		return isDisplayed(driver, FindElement(driver, "//div[@id='step_2of3BW"+xpath+"']/div[contains(text(),'Build Workspace (Step 2 of 3) ')]","workspace step 2 of 3 popUp", action.BOOLEAN,timeOut), "visibility", timeOut,"workspace step 2 of 3 popUp");
	}
	
	@FindBy(xpath="//div[@id='Name_ileinner']")
	private WebElement fundNameInViewMode_Classic;
	
	@FindBy(xpath="//div//h1/div[contains(text(),'Fund')]/..")
	private WebElement fundNameInViewMode_Lighting;

	/**
	 * @return the fundNameLabel
	 */
	public WebElement getFundNameInViewMode(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, fundNameInViewMode_Classic, "Visibility", timeOut, "Fund Name in View Mode Classic");
		}else{
			return isDisplayed(driver, fundNameInViewMode_Lighting, "Visibility", timeOut, "Fund Name in View Mode Lighting");
		}
		
	}

}

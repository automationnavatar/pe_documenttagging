package com.navatar.pageObjects;

import static com.navatar.generic.AppListeners.appLog;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import static com.navatar.generic.CommonLib.*;

import java.util.ArrayList;
import java.util.List;


public class FundraisingsPageBusinessLayer extends FundraisingsPage {

	public FundraisingsPageBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * @author Azhar Alam
	 * @param environment
	 * @param mode
	 * @param fundraisingName
	 * @param fundName
	 * @param legalName
	 * @return true if able to create FundRaising
	 */
	public boolean createFundRaising(String environment,String mode,String fundraisingName, String fundName, String legalName,String recordType) {
		refresh(driver);
		ThreadSleep(5000);
		if (click(driver, getNewButton(environment,mode,60), "New Button", action.SCROLLANDBOOLEAN)) {
			ThreadSleep(500);
			if (sendKeys(driver, getFundraisingName(environment,mode,60), fundraisingName, "FundRaising Name", action.BOOLEAN)) {
				ThreadSleep(500);
				if (sendKeys(driver, getFundName(environment,mode,60), fundName, "Fund Name", action.BOOLEAN)) {
					ThreadSleep(500);
					if (mode.equalsIgnoreCase(Mode.Lightning.toString())) {
						ThreadSleep(2000);
						if (click(driver,
								FindElement(driver,
										"//*[contains(@class,'slds-listbox__option-text')]/*[@title='"+fundName+"']",
										"Fund Name List", action.THROWEXCEPTION, 30),
								fundName + "   :   Fund Name", action.BOOLEAN)) {
							appLog.info(fundName + "  is present in list.");
						} else {
							appLog.info(fundName + "  is not present in the list.");
						}
					}
					if (sendKeys(driver, getLegalName(environment,mode,60), legalName, "Legal Name", action.BOOLEAN)) {
						ThreadSleep(500);
						if (mode.equalsIgnoreCase(Mode.Lightning.toString())) {
							ThreadSleep(1000);
							if (click(driver,
									FindElement(driver,
											"//*[contains(@class,'slds-listbox__option-text')]/*[@title='"+legalName+"']",
											"Legal Name List", action.THROWEXCEPTION, 30),
									legalName + "   :   Legal Name", action.SCROLLANDBOOLEAN)) {
								appLog.info(legalName + "  is present in list.");
							} else {
								appLog.info(legalName + "  is not present in the list.");
							}
						}
						if (click(driver, getCustomTabSaveBtn(environment,mode,60), "Save Button", action.SCROLLANDBOOLEAN)) {
							ThreadSleep(500);
							
								ThreadSleep(2000);
								String fundraising=null;
								WebElement ele;
								if (Mode.Lightning.toString().equalsIgnoreCase(mode)) {
									String	xpath="//*[contains(text(),'Fundraising')]/..//*[text()='"+fundraisingName+"']";
									 ele = FindElement(driver, xpath, "Header : "+fundraisingName, action.BOOLEAN, 30);
								
								} else {
									ele=getFundraisingNameInViewMode(environment, mode, 60,fundraisingName);
								}
								
								if (ele!=null) {
									appLog.info("Fundraising is created successfully.:" + fundraisingName);
									return true;
								} else {
									appLog.info("FundRaising is not created successfully.:" + fundraisingName);
								}
							
						} else {
							appLog.error("Not able to click on save button");
						}
					} else {
						appLog.error("Not able to enter legal Name");
					}
				} else {
					appLog.error("Not able to enter fund name");
				}
			} else {
				appLog.error("Not able to enter value in fundraiisng text box");
			}
		} else {
			appLog.error("Not able to click on new button so we cannot create fundraising");
		}
		return false;
	}

	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param fundRaising
	 * @return true/false
	 */
	public boolean clickOnCreatedFundRaising(String environment,String mode,String fundRaising){
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
		if (getSelectedOptionOfDropDown(driver, getViewDropdown(60), "View dropdown", "text").equalsIgnoreCase("All")) {
			if (click(driver, getGoButton(60), "Go button", action.BOOLEAN)) {

			}
			else {
				appLog.error("Go button not found");
				return false;
			}
		}
		WebElement CreatedfundRaising = FindElement(driver,
				"//div[@class='x-panel-bwrap']//a//span[contains(text(),'" + fundRaising + "')]", "FundRaising Name",
				action.BOOLEAN, 60);
		if (CreatedfundRaising != null) {
			if (click(driver, CreatedfundRaising, "FundRaising Name", action.SCROLLANDBOOLEAN)) {
				 CreatedfundRaising = FindElement(driver,
							"//div[@class='x-panel-bwrap']//a//span[contains(text(),'" + fundRaising + "')]", "FundRaising Name",
							action.BOOLEAN, 3);
				 click(driver, CreatedfundRaising, "FundRaising Name", action.SCROLLANDBOOLEAN);
				appLog.info("Clicked on fundRaising name.:" + fundRaising);
				return true;
				} else {
				appLog.error("Not able to click on fundRaisng Name");
		
			}
		} else {
			appLog.error("FundRaising Name is not Displaying.:" + fundRaising);
			
		}
	}else{
		if(clickOnAlreadyCreated_Lighting(environment, mode, TabName.FundraisingsTab, fundRaising, 30)){
			appLog.info("Clicked on fundRaising name.:" + fundRaising);
			return true;
		}else{
			appLog.error("Not able to click on fundRaisng Name : "+fundRaising);
		}
	}
		return false;
	
	}

	
	
	
}

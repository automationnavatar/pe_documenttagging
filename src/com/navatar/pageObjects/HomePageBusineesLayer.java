package com.navatar.pageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.relevantcodes.extentreports.LogStatus;
import static com.navatar.generic.CommonLib.*;
public class HomePageBusineesLayer extends HomePage {

	public HomePageBusineesLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @author Ankit Jaiswal
	 * @param environment
	 * @param mode
	 * @return true/false
	 */
	public boolean clickOnSetUpLink(String environment,String mode) {
		boolean flag = false;
		if(mode.equalsIgnoreCase(Mode.Classic.toString())) {
			if(click(driver, getUserMenuTab(20), "user menu tab", action.SCROLLANDBOOLEAN)) {
				appLog.info("clicked on user menu tab");
				log(LogStatus.INFO, "user menu tab", YesNo.No);
				
			}else {
				log(LogStatus.ERROR, "user menu tab", YesNo.Yes);
				return flag;
			}
		}else {
			if(click(driver, getSettingLink_Lighting(20), "setting icon", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.INFO, "setting icon", YesNo.No);
				
			}else {
				log(LogStatus.ERROR, "setting icon", YesNo.Yes);
				return flag;
			}
		}
		if(clickUsingJavaScript(driver, getUserMenuSetupLink(environment, mode, 20), "setup link")) {
			log(LogStatus.INFO, "setup link", YesNo.No);
			flag=true;
		}else {
			log(LogStatus.ERROR,"user setup link",YesNo.Yes);
		}
		return flag;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param navatarQuickLink
	 * @return true/false
	 */
	public boolean clickOnLinkFromNavatarQuickLink(String environment,String mode,NavatarQuickLink navatarQuickLink){
		boolean flag =false;
		if (mode.equalsIgnoreCase(Mode.Lightning.toString())) {
			if (click(driver, getNavatarQuickLink_Lighting(environment, 20), "Navatar Quik Link",
					action.SCROLLANDBOOLEAN)) {
				ThreadSleep(1000);
				switchToFrame(driver, 10, getNavatarQuickLinkFrame_Lighting(environment, 10));
			}
		}else{
			if(getCloseSideBar(5)==null){
				if(click(driver, getOpenSideBar(30), "Open sied bar", action.BOOLEAN)){
					log(LogStatus.INFO, "Opened the side bar.", YesNo.No);
				} else {
//					BaseLib.sa.assertTrue(false, "cannot open the side bar, So cannot check the navatar quick link.");
//					log(LogStatus.ERROR, "cannot open the side bar, So cannot check the navatar quick link.", YesNo.Yes);
				}
			}
			ThreadSleep(1000);
			appLog.info("Inside Classic Frame");
			switchToFrame(driver, 10, getNavatarQuickLinkFrame_Classic(environment, 10));	
		}
		
		WebElement quickLink = FindElement(driver, "//a[contains(text(),'"+navatarQuickLink+"')]", "Navatar Quick Link : "+navatarQuickLink, action.SCROLLANDBOOLEAN, 20);
		if (click(driver, quickLink, "Navatar Quick Link : "+navatarQuickLink, action.SCROLLANDBOOLEAN)) {	
			flag = true;
		}
		
		switchToDefaultContent(driver);
		if (mode.equalsIgnoreCase(Mode.Lightning.toString())) {
			if (click(driver, getNavatarQuickLinkMinimize_Lighting(environment, 20), "Navatar Quik Link Minimize Icon",
					action.SCROLLANDBOOLEAN)) {
				ThreadSleep(1000);
			}
		}
		
		return flag;
		
	}
	
	
	
	
	
	
	
	
	
	
}

package com.navatar.pageObjects;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.navatar.generic.CommonLib;
import com.navatar.generic.EnumConstants.InstitutionPageFieldLabelText;
import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.action;
import com.navatar.generic.EnumConstants.excelLabel;
import static com.navatar.generic.CommonLib.*;

public class PipelinesPage extends BasePageBusinessLayer {

	public PipelinesPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath="//span[text()='Pipeline Stage Logs']/ancestor::article//span[text()='View All']")
	private WebElement pipeLineStageLogViewAll_Lighting;
	
	
	/**
	 * @param environment
	 * @param timeOut
	 * @return
	 */
	public WebElement getPipeLineStageLogViewAll_Lighting(String environment,int timeOut) {
		CommonLib.scrollThroughOutWindow(driver);
		return isDisplayed(driver, pipeLineStageLogViewAll_Lighting, "Visibility", timeOut, "PipeLine Stage Log View All");
	
	}
	
	@FindBy(xpath="//label[text()='Stage']/../following-sibling::td//span/select")
	private WebElement pipeLineStageLabel_Classic;
	
	
	/**
	 * @param environment
	 * @param timeOut
	 * @return
	 */
	public WebElement getPipeLineStageLabel(String environment,String mode,int timeOut) {
		return isDisplayed(driver, pipeLineStageLabel_Classic, "Visibility", timeOut, "PipeLine Stage Label classic");
	
	}
	
	
	public WebElement getpipeLInePageTextBoxAllWebElement(String environment,String mode, String labelName, int timeOut) {
		WebElement ele=null;
		String xpath ="",inputXpath="",finalXpath="",finalLabelName="",xpath1="",xpath2="";
		if(labelName.contains("_")) {
			finalLabelName=labelName.replace("_", " ");
		}else {
			finalLabelName=labelName;
		}
		if(mode.equalsIgnoreCase(Mode.Classic.toString())) {
			
		}else {
			xpath="//label[text()='"+finalLabelName+"']";
			inputXpath="/following-sibling::div/input";
			xpath1="/following-sibling::div/div//input";
			xpath2="/following-sibling::div//button";
			if(labelName.toString().equalsIgnoreCase(InstitutionPageFieldLabelText.Parent_Institution.toString())) {
				inputXpath="/following-sibling::div//input[contains(@placeholder,'Search Institutions')]";
			}
			
		}
		if(labelName.equalsIgnoreCase(excelLabel.Company_Name.toString())) {
			finalXpath=xpath+xpath1;
		}else if (labelName.equalsIgnoreCase(excelLabel.Stage.toString())) {
			finalXpath=xpath+xpath2;
		}else {
			finalXpath=xpath+inputXpath;
		}
		ele=isDisplayed(driver, FindElement(driver, finalXpath, finalLabelName+" text box in "+mode, action.SCROLLANDBOOLEAN,30), "Visibility", timeOut, finalLabelName+"text box in "+mode);
		return ele;
		
	}
	
	
	@FindBy(xpath="//input[@name='save']")
	private WebElement saveButtonClassic;
	
	@FindBy(xpath="//*[@title='Save' or text()='Save' and @title='Save']")
	private WebElement saveButtonLighting;

	/**
	 * @return the saveButton
	 */
	public WebElement getSaveButton(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, saveButtonClassic, "Visibility", timeOut, "Save Button Classic");
		}else{
			return isDisplayed(driver, saveButtonLighting, "Visibility", timeOut, "Save Button Lighting");
		}
		
	}
	
	
	public WebElement getDocumentButton(String openTaskName, int timeOut) {
//		String xpath="//a[text()='"+openTaskName+"']/../../../following-sibling::td//button[text()='Documents']";
		String xpath="//a[text()='"+openTaskName+"']/ancestor::td[contains(@data-label,'Name:')]/following-sibling::td[@class='actions']//button[text()='Documents']";
		return isDisplayed(driver, FindElement(driver, xpath,"task name "+openTaskName+" documents buton", action.SCROLLANDBOOLEAN,timeOut), "Visibility", timeOut,"task name "+openTaskName+" documents buton");
				
	}
	
	
	public WebElement getCompleteLink(String openTaskName, int timeOut) {
		String xpath="//a[text()='"+openTaskName+"']/../../../..//following-sibling::td//*[text()='Complete']";
		
		return isDisplayed(driver, FindElement(driver, xpath,"task name "+openTaskName+" complete link", action.SCROLLANDBOOLEAN,timeOut), "Visibility", timeOut,"task name "+openTaskName+" complete link");
				
	}
	
	
	
	@FindBy(xpath = "//h2[text()='Navatar Documents']")
	private WebElement navatarDocumentsLabelText;


	public WebElement getNavatarDocumentsLabelText(int timeOut) {
		return isDisplayed(driver, navatarDocumentsLabelText, "Visibility", timeOut, "navatar Documents Label Text");
	}

	
	@FindBy(xpath = "//form[@id='pg:frm1']//button[@onclick='getAlltaggedDocuments();' or @title='Refresh']")
	private WebElement refreshButton;


	public WebElement getRefreshButton(int timeOut) {
		return isDisplayed(driver, refreshButton, "Visibility", timeOut, "refresh Button");
	}
	
	public WebElement taskLabelNameOnNavtarDocumentsPopUp(String taskName, int timeOut) {
		String xpath="//label[@id='pg:frm1:lblTaggedField']/span[text()='"+taskName+"']";
		return isDisplayed(driver, FindElement(driver, xpath, "open task name "+taskName+" on navatar documents", action.BOOLEAN,timeOut), "Visibility", timeOut,"open task name "+taskName+" on navatar documents");

	}
	
	@FindBy(xpath = "//footer/button[@title='Cancel']")
	private WebElement cancelButtonOnNavatarDocumentsPopUp;


	public WebElement getCancelButtonOnNavatarDocumentsPopUp(int timeOut) {
		return isDisplayed(driver, cancelButtonOnNavatarDocumentsPopUp, "Visibility", timeOut, "cancel Button On Navatar Documents PopUp");
	}
	
	
	@FindBy(xpath = "//header/button[@title='Close']")
	private WebElement crossIconOnNavatarDocumentPopUp;


	public WebElement getCrossIconOnNavatarDocumentPopUp(int timeOut) {
		return isDisplayed(driver, crossIconOnNavatarDocumentPopUp, "Visibility", timeOut, "cross Icon On Navatar Document PopUp");
	}
	
	
	
	public List<WebElement> getDocumentNameAndActionColumnOnNavatarDocumentPopUp(){
		return FindElements(driver, "//table[@id='pg:frm1:tagDocTable']//tr/th/div", "Document Name And Action Column list On Navatar Document popup");
	}
	
	public List<WebElement> getdroppedFileNamesListInUploadDocument() {
		return FindElements(driver, "//img[@action='modify']/..", "document name list in upload document window");
	}
	
	
	@FindBy(css = "#btnSaveTree")
	private WebElement uploadDocumentSaveBtn;


	public WebElement getUploadDocumentSaveBtn(int timeOut) {
		return isDisplayed(driver, uploadDocumentSaveBtn, "Visibility", timeOut, "upload document button");
	}
	
	
	@FindBy(xpath = "//div[contains(@class,'Confirmation_fancybox')]/div[2]")
	private WebElement uploadDocumentConfirmationPopUpText;


	public WebElement getUploadDocumentConfirmationPopUpText(int timeOut) {
		 return isDisplayed(driver, uploadDocumentConfirmationPopUpText, "Visibility", timeOut, "upload document confirmation popup text");
	}
	
	@FindBy(xpath = "//div[contains(@class,'Confirmation_fancybox')]/div/input")
	private WebElement uploadDocumentConfirmationPopUpCloseBtn;


	public WebElement getUploadDocumentConfirmationPopUpCloseBtn(int timeOut) {
		return isDisplayed(driver, uploadDocumentConfirmationPopUpCloseBtn, "Visibility", timeOut, "upload document confirmation popup close button");
	}
	
	@FindBy(xpath="//div[contains(@class,'ConfirmationDupFiles')]//a[@title='Update All']")
	private WebElement simpleupdateAllButton;

	/**
	 * @return the updateAllButton
	 */
	public WebElement getSimpleupdateAllButton(int timeOut) {
		return isDisplayed(driver, simpleupdateAllButton, "Visibility", timeOut, "simple upload updateAll Button");
	}
	
	@FindBy(css = "input#lbtOnlinImportSave")
	private WebElement tagDocumentsBtn;


	public WebElement getTagDocumentsBtn(int timeOut) {
		return isDisplayed(driver, tagDocumentsBtn, "Visibility", timeOut, "tag documents button");
	}
	
	
	public List<WebElement> getTaggedDocumentListOnNavatarDocumentsPopUp(){
		return FindElements(driver, "//table/tbody[@id='pg:frm1:tagDocTable:tb']/tr/td[1]/a", "tagged document list on navatar document list");
	}
	
	public List<WebElement> getTaggedDocumentRemoveLinkListOnNavatarDocumentsPopUp(){
		return FindElements(driver, "//table/tbody[@id='pg:frm1:tagDocTable:tb']/tr/td[2]/a", "tagged document list on navatar document list");
	}
	
	
	public WebElement getDocumentRemoveLinkOnNavatarDocumentPopUp(String docName, int timeOut) {
		String xpath="//table/tbody[@id='pg:frm1:tagDocTable:tb']/tr/td[1]/a[contains(text(),'"+docName+"')]/../following-sibling::td/a";
		return isDisplayed(driver, FindElement(driver, xpath,docName+" name remove link ", action.BOOLEAN,timeOut), "Visibility", timeOut,docName+" name remove link ");
	}
	
	
	
	public WebElement getHeaderSideButtons(Header header, HeaderSideButton headersidebutton, int timeOut) {
		String xpath="//a[contains(text(),'"+header.toString().replace("_", " ")+"')]/../../..//following-sibling::div//span[text()='"+headersidebutton.toString().replace("_", " ")+"']";
		
		return isDisplayed(driver, FindElement(driver, xpath,header.toString()+"  header side button "+headersidebutton.toString(), action.BOOLEAN,timeOut), "Visibility", timeOut,header.toString()+"  header side button "+headersidebutton.toString());
	
	}
	
	@FindBy(xpath = "//a[@title='Remove']")
	private WebElement navatarDocumentPopUpRemoveButton;


	public WebElement getNavatarDocumentPopUpRemoveButton(int timeOut) {
		return isDisplayed(driver, navatarDocumentPopUpRemoveButton, "Visibility", timeOut, "document remove button");
	}
	
	@FindBy(xpath = "//a[@title='Cancel']")
	private WebElement navatarDocumentPopUpCancelButton;


	public WebElement getNavatarDocumentPopUpCancelButton(int timeOut) {
		return isDisplayed(driver, navatarDocumentPopUpCancelButton, "Visibility", timeOut, "document cancel button");
	}
	
	
	
	@FindBy(xpath = "//div[contains(@class,'DealRoomName_fancybox')]/div[2]/p")
	private WebElement navatarDocumentRemovePopUpErrorMsg;
	
	
	public WebElement getNavatarDocumentRemovePopUpErrorMsg(int timeOut) {
		return isDisplayed(driver, navatarDocumentRemovePopUpErrorMsg, "Visibility", timeOut, "navatar document remove popup error message");
	}

	@FindBy(xpath = "//div[@class='error-container']/div/h3/span")
	private WebElement documentNotFoundErrorMsg;


	public WebElement getDocumentNotFoundErrorMsg(int timeOut) {
		return isDisplayed(driver, documentNotFoundErrorMsg, "Visibility", timeOut, "document not found error message");
	}
	
	@FindBy(xpath = "//a[@data-resin-target='homebutton']")
	private WebElement boxHomeLogoOnDocumentTab;


	public WebElement getBoxHomeLogoOnDocumentTab(int timeOut) {
		return isDisplayed(driver, boxHomeLogoOnDocumentTab, "Visibility", timeOut, "box home page logo xpath on document tab");
	}
	
	
	public WebElement getMoveOrCopyOrCancelButtonOnMoveOrCopyPopUp(Buttons button, int timeOut) {
		String xpath="//button[@data-resin-target='"+button+"']";
		return isDisplayed(driver, FindElement(driver, xpath,"move or copy popup "+button.toString()+" button", action.BOOLEAN,timeOut), "Visibility", timeOut,"move or copy popup "+button.toString()+" button");
	}
	
	@FindBy(xpath = "//div[@aria-label='Rename file' or @aria-label='Rename folder']//input[@class='popup-prompt-input']")
	private WebElement boxRenameTextBoxInDocuments;


	public WebElement getBoxRenameTextBoxInDocuments(int timeOut) {
		return isDisplayed(driver, boxRenameTextBoxInDocuments, "Visibility", timeOut, "Box rename text box in documents tab");
	}
	
	
	public WebElement getBoxRenamePopUpOkayAndCancelInDocuments(Buttons button, int timeOut) {
		String xpath="//div[@aria-label='Rename file' or @aria-label='Rename folder']//button[text()='"+button+"']";
		return isDisplayed(driver, FindElement(driver, xpath,"box rename popup "+button.toString()+" button", action.BOOLEAN,timeOut), "Visibility", timeOut,"box rename popup "+button.toString()+" button");
	}
	
	public WebElement getcreatedPipeLineShowMoreActionIcon(String pipeLineName, int timeOut) {
		String xpath="//a[text()='"+pipeLineName+"']/../../following-sibling::td//a[@title='Show 2 more actions']";
		return isDisplayed(driver, FindElement(driver, xpath,pipeLineName+" show more icon", action.BOOLEAN,timeOut), "Visibility", timeOut,pipeLineName+" show more icon");
	}
	
	@FindBy(xpath = "//button[@name='Edit']")
	private WebElement editButton;


	public WebElement getEditButton(int timeOut) {
		return isDisplayed(driver, editButton, "Visibility", timeOut, "edit button");
	}
	
	
	public WebElement getTagDocumentCustomFieldLink(int timeOut) {
		String xpath="//*[text()='Test Tag Documents']/../following::span//a[text()='Tag Documents']";
		return isDisplayed(driver, FindElement(driver, xpath,"tag document link", action.BOOLEAN,timeOut), "Visibility", timeOut,"tag document link");
	}
	
	
	@FindBy(xpath = "//input[@value='Login']")
	private WebElement BoxLoginButtonOnDocumentPage;


	public WebElement getBoxLoginButtonOnDocumentPage(int timeOut) {
		return isDisplayed(driver,BoxLoginButtonOnDocumentPage, "Visibility", timeOut,"box login button");
	}
	
	@FindBy(xpath = "//input[@value='Create Folder']")
	private WebElement createFolderButtonOnDocumentsPage;


	public WebElement getCreateFolderButtonOnDocumentsPage(int timeOut) {
		return isDisplayed(driver,createFolderButtonOnDocumentsPage, "Visibility", timeOut,"create folder button");
	}
	
	@FindBy(xpath = "//h3[text()='Document not found.']")
	private WebElement documentNotFoundErrorMsgForIP;


	public WebElement getDocumentNotFoundErrorMsgForIP(int timeOut) {
		return isDisplayed(driver,documentNotFoundErrorMsgForIP, "Visibility", timeOut,"document not found for ip");
	}
	@FindBy(xpath = "//div[contains(@class,'ConfirmationDupFiles')]//a[@title='Ignore All']")
	private WebElement IgnoreAllButton;
	
	
	public WebElement getIgnoreAllButton(int timeOut) {
		return isDisplayed(driver, IgnoreAllButton, "Visibility", timeOut, "ignore all Button");
	}
	
}

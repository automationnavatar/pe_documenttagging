package com.navatar.pageObjects;

public interface PipeLineErrorMessage {

	
	public static String uploadDocumentErrorMsg="All documents have been added successfully.";
	public static String tagDocumentErrorMsg="Document(s) tagged successfully.";
	public static String documentNotFoundErrorMsg="Oops! We can't seem to find the page you're looking for.";
	public static String removeTaggedDocumentPopUpErrorMsg="Are you sure you want to remove this tagged document?";
}

package com.navatar.pageObjects;

public interface TaskRayErrorMessage {
	
	public static String uploadFileErrorMsg="File upload failed, upload via Chatter.";
	public static String tagDocumentErrorMessage="Your current settings only allow you to tag documents from a Fund. Please contact your Navatar Administrator.";

}

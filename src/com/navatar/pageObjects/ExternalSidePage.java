package com.navatar.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.navatar.generic.EnumConstants.action;

import static com.navatar.generic.CommonLib.*;

public class ExternalSidePage extends BasePageBusinessLayer {

	public ExternalSidePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public WebElement getOpenAndCompleteTaskLink(Header headerName, int timeOut) {
		String xpath="//a[contains(text(),'"+headerName+"')]";
		WebElement ele= FindElement(driver, xpath,headerName+" xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,headerName+" xpath ");
		
	}
	
	public WebElement getOpenTaskCompleteLink(String taskName, String pipeLineName, int timeOut) {
		String xpath="//a[text()='"+taskName+"']/ancestor::td[contains(@data-label,'Name:')]/following-sibling::td[contains(@data-label,'Deal:')]//*[text()='"+pipeLineName+"']/../../..//following-sibling::td[contains(@data-label,'Action:')]//a[text()='Complete']";
		WebElement ele= FindElement(driver, xpath,taskName+" complete link xpath", action.SCROLLANDBOOLEAN, timeOut);
		return ele;
		
	}
	
}

package com.navatar.pageObjects;

import static com.navatar.generic.AppListeners.appLog;
import static com.navatar.generic.AppListeners.currentlyExecutingTC;
import static com.navatar.generic.CommonLib.*;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Screen;

import com.navatar.generic.BaseLib;
import com.navatar.generic.CommonLib;
import com.navatar.generic.ExcelUtils;
import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.TabName;
import com.navatar.generic.EnumConstants.action;
import com.relevantcodes.extentreports.LogStatus;

public class FundsPageBusinessLayer extends FundsPage implements FundsPageErrorMessage {

	public FundsPageBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @author Ankit Jaiswal
	 * @param environment
	 * @param mode
	 * @param fundName
	 * @param fundType
	 * @param investmentCategory
	 * @param otherLabelFields
	 * @param otherLabelValues
	 * @return true/false
	 */
	public boolean createFund(String environment, String mode, String fundName, String fundType,
			String investmentCategory, String otherLabelFields,String otherLabelValues,String recordType) {
		String labelNames[]=null;
		String labelValue[]=null;
		if(otherLabelFields!=null && otherLabelValues !=null) {
			labelNames= otherLabelFields.split(",");
			labelValue=otherLabelValues.split(",");
		}
		
		if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
			refresh(driver);
			ThreadSleep(10000);
			if(clickUsingJavaScript(driver, getNewButton(environment, mode, 60), "new button")) {
				appLog.info("clicked on new button");
			}else {
				appLog.error("Not able to click on new button so cannot create fund : "+fundName);
				return false;
			}
		}else {
			ThreadSleep(5000);
			if (click(driver, getNewButton(environment,mode,60), "New Button", action.SCROLLANDBOOLEAN)) {
				appLog.info("clicked on new button");
			} else {
				appLog.error("Not able to click on new button so cannot create fund : "+fundName);
				return false;
			}
		}
//		if (click(driver, getNewButton(environment, mode, 60), "New Button", action.BOOLEAN)) {
			ThreadSleep(500);
			if (sendKeys(driver, getFundName(environment, mode, 60), fundName, "Fund Name", action.BOOLEAN)) {
				ThreadSleep(500);
				if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
					if (selectVisibleTextFromDropDown(driver, getFundType(environment, mode, 60), "Fund Type",
							fundType)) {
						ThreadSleep(500);
						if (selectVisibleTextFromDropDown(driver, getInvestmentCategory(environment, mode, 60),
								"Investment Category", investmentCategory)) {
							ThreadSleep(500);
						} else {
							appLog.error("Not able to select investment category");
						}
					} else {
						appLog.error("Not able to select fund type");
					}
				} else {
					if (click(driver, getFundType(environment, mode, 60), "Fund Type ", action.SCROLLANDBOOLEAN)) {
						WebElement fundTypeEle = FindElement(driver,
								"//span[@title='"+fundType+"']/../..", fundType,
								action.SCROLLANDBOOLEAN, 10);
						ThreadSleep(1000);
						if (click(driver, fundTypeEle, fundType, action.SCROLLANDBOOLEAN)) {

						} else {
							appLog.error("Not able to Select Fund Type");
						}
					} else {
						appLog.error("Not able to Click on Fund Type");
					}
					if (click(driver, getInvestmentCategory(environment, mode, 60), "Investment Category",
							action.SCROLLANDBOOLEAN)) {
						WebElement InvsCatgEle = FindElement(driver,
								"//label[text()='Investment Category']/following-sibling::div//span[@title='"+investmentCategory+"']/../..",
								investmentCategory, action.SCROLLANDBOOLEAN, 10);
						ThreadSleep(1000);
						if (click(driver, InvsCatgEle, investmentCategory, action.SCROLLANDBOOLEAN)) {
						
						} else {
							appLog.error("Not able to select Investment Category");
						}
					} else {
						appLog.error("Not able to Click on Investment Category");
					}
				}
				if(labelNames!=null && labelValue!=null) {
					for(int i=0; i<labelNames.length; i++) {
						WebElement ele = getFundtPageTextBoxOrRichTextBoxWebElement(environment, mode, labelNames[i].trim(), 30);
						if(sendKeys(driver, ele, labelValue[i], labelNames[i]+" text box", action.SCROLLANDBOOLEAN)) {
							appLog.info("passed value "+labelValue[i]+" in "+labelNames[i]+" field");
						}else {
							appLog.error("Not able to pass value "+labelValue[i]+" in "+labelNames[i]+" field");
							BaseLib.sa.assertTrue(false, "Not able to pass value "+labelValue[i]+" in "+labelNames[i]+" field");
						}
					}
					
				}
				if (click(driver, getCustomTabSaveBtn(environment, mode, 60), "Save Button", action.BOOLEAN)) {
					ThreadSleep(5000);
					
					WebElement ele;
					if (Mode.Lightning.toString().equalsIgnoreCase(mode)) {
						String	xpath="//*[contains(text(),'Fund')]/..//*[text()='"+fundName+"']";
//						 ele = FindElement(driver, xpath, "Header : "+fundName, action.BOOLEAN, 30);
//						 ele=isDisplayed(driver, ele, "Visibility", 10, "Fund Name in View Mode Lighting");
//						 
						 ele = verifyCreatedItemOnPage(Header.Fund, fundName);
					
					} else {
						ele=getFundNameInViewMode(environment, mode, 60);
					}
					
					if (ele != null) {
						ThreadSleep(2000);
						String fundNameViewMode = ele.getText().trim();
						if (fundNameViewMode.contains(fundName)) {
							appLog.info("Fund is created successfully.:" + fundName);
							if(labelNames!=null && labelValue!=null ) {
								for(int i=0; i<labelNames.length; i++) {
									if(FieldValueVerificationOnFundPage(environment, mode, labelNames[i].replace("_", " ").trim(),labelValue[i])){
										appLog.info(labelNames[i]+" label value "+labelValue[i]+" is matched successfully.");
									}else {
										appLog.info(labelNames[i]+" label value "+labelValue[i]+" is not matched successfully.");
										BaseLib.sa.assertTrue(false, labelNames[i]+" label value "+labelValue[i]+" is not matched.");
									}
								}
							}
							return true;
						} else {
							appLog.error("Fund is not created successfully.:" + fundName);
						}
					} else {
						appLog.error("Not able to find Fund Name in View Mode");
					}
				} else {
					appLog.error("Not able to click on save button");
				}

			} else {
				appLog.error("Not able to enter fund name in text box");
			}
//		} else {
//			appLog.info("Not able to click on new button so we cannot create fund");
//		}
		return false;
	}

	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param fundName
	 * @return true/false
	 */
	public boolean clickOnCreatedFund(String environment, String mode,String fundName) {
		
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
		int i=1;
		if (getSelectedOptionOfDropDown(driver, getViewDropdown(60), "View dropdown", "text").equalsIgnoreCase("All")) {
			if (click(driver, getGoButton(60), "Go button", action.BOOLEAN)) {

			}
			else {
				appLog.error("Go button not found");
				return false;
			}
		}
		else{
			if (selectVisibleTextFromDropDown(driver, getViewDropdown(60),"View dropdown","All") ){
			}
			else {
				appLog.error("All Funds not found in dropdown");
				return false;
			}

		}
	
			WebElement fund = getFundNameAtFundPage(fundName, 20);
			if (fund != null) {
				if (click(driver, fund, "Fund Name", action.SCROLLANDBOOLEAN)) {
					appLog.info("Clicked on fund name : " + fundName);
					return true;
					} 
			} else {
		
				//
				
				while (true) {
					appLog.error("Fund Name is not Displaying on "+i+ " Page:" + fundName);
					if (click(driver, getNextImageonPage(10), "Fund Page Next Button",
							action.SCROLLANDBOOLEAN)) {

						appLog.info("Clicked on Next Button");
						 fund = getFundNameAtFundPage(fundName, 20);
						if (fund != null) {
							if (click(driver, fund, "Fund Name", action.SCROLLANDBOOLEAN)) {
								appLog.info("Clicked on fund name : " + fundName);
								return true;
								
							}
						}

						

					} else {
						appLog.error("Fund Not Available : " + fundName);
						return false;
					}
					i++;
				}
				
				//
				
			}
	}else{
		if(clickOnAlreadyCreated_Lighting(environment, mode, TabName.FundsTab, fundName, 30)){
			appLog.info("Clicked on fund name : " + fundName);
			return true;
		}else{
			appLog.error("Fund Not Available : " + fundName);	
		}
	}
			return false;
		
		
	}
	
	/**
	 * @author Ankit Jaiswal
	 * @param environment
	 * @param mode
	 * @param tabName
	 * @param labelName
	 * @param labelValue
	 * @return true/false
	 */
	public boolean FieldValueVerificationOnFundPage(String environment, String mode,
			String labelName,String labelValue) {
		String xpath = "";
		WebElement ele = null;
		if(labelName.contains(excelLabel.Target_Commitments.toString().replaceAll("_", " "))) {
			labelName=FundPageFieldLabelText.Target_Commitments.toString();
		}
		if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
			if(labelName.contains("Total") || labelName.contains("Target")) {
				xpath="//td[text()='"+labelName+"']/following-sibling::td/div";
			}else {
				xpath = "//td[text()='"+ labelName +"']/../td[2]/div";
			}
		} else {
			
			xpath = "//span[@class='test-id__field-label'][text()='" + labelName
					+ "']/../following-sibling::div//lightning-formatted-text";
		}
		ele = isDisplayed(driver,
				FindElement(driver, xpath, labelName + " label text in " + mode, action.SCROLLANDBOOLEAN, 60),
				"Visibility", 30, labelName + " label text in " + mode);
		if (ele != null) {
			String aa = ele.getText().trim();
			appLog.info("Lable Value is: "+aa);
			if(labelName.contains("Date")) {
				if(verifyDate(aa,null, labelName)) {
					appLog.info("Dtae is verified Successfully");
					return true;
				}else {
					appLog.error(labelName+ " Date is not verified. /t Actual Result: "+aa);
				}
			}else {
				if(aa.contains(labelValue)) {
					appLog.info(labelValue + " Value is matched successfully.");
					return true;
					
				}else {
					appLog.error(labelValue + " Value is not matched. Expected: "+labelValue+" /t Actual : "+aa);
				}
			}
		} else {
			appLog.error(labelName + " Value is not visible so cannot matched  label Value "+labelValue);
		}
		return false;

	}

	
	/**
	 * 
	 * @param step1Of3Data Pass data with respect to the text field boxes on the UI.
	 * @param WithOrWithOutFolderTemplate
	 * @param folderTemplateName if want to import folder structure otherwise null.
	 * @param folderStructureSheetName Sheet in excel which contains the folder structure in case you have to create structure at build time, otherwise null.
	 * @param institutionOrLPName  'InstitutionName' if passing single institution else if pasing multiple institutions 'InstitutionName1<break>InstitutionName2'
	 * 							   'InstituionName/LPName' if passing single LP else if passing multiple LP 'InstitutionName/LPName<break>InstitutionName1/LPName1' 
	 * @param workspace workspace which has to be created.
	 * @param timeOut
	 * @return boolean
	 */
	public boolean buildWorkspace(String[] step1Of3Data, WorkSpaceAction WithOrWithOutFolderTemplate, String folderTemplateName, String folderStructureSheetName, String institutionOrLPName, Workspace workspace, int timeOut){
		boolean flag = true;
		scrollDownThroughWebelement(driver, getWorkSpaceLabel(60), workspace.toString()+" View.");
		ThreadSleep(10000);
		switchToFrame(driver, 50, getFrame(PageName.FundsPage, timeOut));
		System.err.println("Switched to frame.");
		scrollDownThroughWebelement(driver, getWorkspaceSectionView(workspace, timeOut), workspace.toString()+" View.");
		if(click(driver, getBuildWorkspaceButton(workspace, timeOut), workspace.toString()+" Workspace Button", action.BOOLEAN)){
			if(!sendKeys(driver, getSizeInMillionTextBox(workspace, timeOut), step1Of3Data[0], "Size in Million text box", action.BOOLEAN)){
				BaseLib.sa.assertTrue(false,"Not able to pass data to size in million text box.");
			}
			if(!sendKeys(driver, getVintageYear(workspace, timeOut), step1Of3Data[1], "vintage Year", action.BOOLEAN)){
				BaseLib.sa.assertTrue(false,"Not able to pass data to Vintage Year text box.");
			}
			String value = getAttribute(driver, getContactTextBox(workspace, timeOut), "Contact Text Box", "value");
			if(value!=null && !value.isEmpty()){
				appLog.info("value is already present in contact text box.");
			} else {
				if(!sendKeys(driver, getContactTextBox(workspace, timeOut), step1Of3Data[2], "Contact text Box", action.BOOLEAN)){
					flag=false;
				}
			}
			value = getAttribute(driver, getPhoneTextBox(workspace, timeOut), "Phone Text Box", "value");
			if(value!=null && !value.isEmpty()){
				appLog.info("value is already present in contact text box.");
			} else {
				if(!sendKeys(driver, getPhoneTextBox(workspace, timeOut), step1Of3Data[3], "Phone text Box", action.BOOLEAN)){
					flag=false;
				}
			}
			value = getAttribute(driver, getEmailTextBox(workspace, timeOut), "Email Text Box", "value");
			if(value!=null && !value.isEmpty()){
				appLog.info("value is already present in contact text box.");
			} else {
				if(!sendKeys(driver, getEmailTextBox(workspace, timeOut), step1Of3Data[4], "Email text Box", action.BOOLEAN)){
					flag=false;
				}
			}
			value = getAttribute(driver, getDescriptionTextBox(workspace, timeOut), "Description Text Box", "value");
			if(value!=null && !value.isEmpty()){
				appLog.info("value is already present in contact text box.");
			} else {
				if(!sendKeys(driver, getDescriptionTextBox(workspace, timeOut), step1Of3Data[5], "Description text Box", action.BOOLEAN)){
				}
			}
			if(flag){
				if(click(driver, getNext1Of3Button(workspace, timeOut), "Next Button", action.BOOLEAN)){
					scrollDownThroughWebelement(driver, getBuildWorkSpace2Of3LabelText(workspace, 30), workspace+" popup label text");
					if(!importFolderTemplate(folderStructureSheetName, folderTemplateName, WithOrWithOutFolderTemplate, workspace, timeOut)){
						flag=false;
						appLog.error("Folder sructure is not created properly");
						BaseLib.sa.assertTrue(false,"Folder sructure is not created properly");
					} else {
						appLog.info("Folder Structure is created successfully.");
					}
					if(click(driver, getNext2Of3Button(workspace, timeOut), "Next button 2Of3", action.BOOLEAN)){
						ThreadSleep(10000);
						if(selectInstitutionOrLP(institutionOrLPName, workspace, timeOut)){
							if(click(driver, getInvestorAddedConfirmationCloseButton(workspace, timeOut), "Investor Addition confirmation Close Button", action.BOOLEAN));
						}
						if(click(driver, getDone3Of3Button(workspace, timeOut), "Done button", action.BOOLEAN)){
							appLog.info("Workspace has been created.");
							if(click(driver, getCongratulationsCloseButton(workspace, timeOut), "Congratualtions pop close button", action.BOOLEAN)){
								appLog.info("Workspace Created Successfully.");
							} else {
								refresh(driver);
							}
						} else {
							appLog.error("Workspace is not created.");
							BaseLib.sa.assertTrue(false,"Workspace is not created.");
							flag=false;
						}
					} else {
						appLog.error("Next button 2Of3 cannot be clicked, So cannot build the workspace");
						BaseLib.sa.assertTrue(false,"Next button 2Of3 cannot be clicked, So cannot build the workspace");
						flag=false;
					}
				} else {
					appLog.error("Next button cannot be clicked, so cannot build the workspace.");
					BaseLib.sa.assertTrue(false,"Next button cannot be clicked, so cannot build the workspace.");
					flag=false;
				}
			} else {
				appLog.error("Some mantadory fields are not filled so cannot continue with the tc.");
				BaseLib.sa.assertTrue(false,"Some mantadory fields are not filled so cannot continue with the tc.");
			}
		} else {
			appLog.error("Build button is not present on, So cannot create workspace.");
			BaseLib.sa.assertTrue(false,"Build button is not present on, So cannot create workspace.");
			flag=false;
		}
		switchToDefaultContent(driver);
		return flag;
	}
	
	
	/**
	 * @param sheetName
	 * @param folderTemplateName
	 * @param WithOrWithOutFolderTemplate
	 * @param workspace
	 * @param timeOut
	 * @return true/false
	 */
	public boolean importFolderTemplate(String sheetName, String folderTemplateName, WorkSpaceAction WithOrWithOutFolderTemplate, Workspace workspace, int timeOut){
		boolean flag = false;
		NIMPageBusinessLayer nim = new NIMPageBusinessLayer(driver);
		if(WithOrWithOutFolderTemplate.toString().equalsIgnoreCase(WorkSpaceAction.WITHOUTEMPLATE.toString())){
			appLog.info("Building workspace without folder");
			flag=true;
		} else if (WithOrWithOutFolderTemplate.toString().equalsIgnoreCase(WorkSpaceAction.IMPORTFOLDERTEMPLATE.toString())){
			if(click(driver, getImportFolderTemplateButton(workspace, timeOut), "folder template Imprt Button", action.BOOLEAN)){
				if(selectVisibleTextFromDropDown(driver, getDisplayDropDown(workspace, timeOut), "Display Drop Down", "All Templates")){
					if(click(driver, getFolderTemplateRadioButton(folderTemplateName, workspace, timeOut), folderTemplateName+" Folder template radio Button", action.BOOLEAN)){
						if(click(driver, getFolderTemplateImportButton(workspace, timeOut), "Import Button", action.BOOLEAN)){
							appLog.info("Successfully imported folder template: "+folderTemplateName);
							flag=true;
						} else {
							appLog.error("Import button cannot be clicked, So cannot create "+workspace.toString()+" workspace.");
							BaseLib.sa.assertTrue(false,"Import button cannot be clicked, So cannot create "+workspace.toString()+" workspace.");
						}
					} else {
						appLog.error(folderTemplateName+" Folder template is not present in the list.");
						BaseLib.sa.assertTrue(false,folderTemplateName+" Folder template is not present in the list.");
					}
				} else {
					appLog.error("Not able to select Option from the drop down.");
					BaseLib.sa.assertTrue(false,"Not able to select Option from the drop down.");
				}
			} else {
				appLog.error("Import folder template button cannot be clicked, So cannot create workspace.");
				BaseLib.sa.assertTrue(false,"Import folder template button cannot be clicked, So cannot create workspace.");
			}
		} else if (WithOrWithOutFolderTemplate.toString().equalsIgnoreCase(WorkSpaceAction.CREATEFOLDERTEMPLATE.toString())){
			Map<String, String> s = folderStructureInExcel(sheetName);
			Set<String> paths = s.keySet();
			Iterator<String> i = paths.iterator();
			i = paths.iterator();
			FolderType folderType = null;
			while (i.hasNext()) {
				String string = i.next();
				if (string.isEmpty())
					continue;
				System.out.println("\n\n\nCreating folder template\n\n\n");
				if(s.get(string).equalsIgnoreCase("Shared")){
					folderType=FolderType.Shared;
				} else if (s.get(string).equalsIgnoreCase("Common")){
					folderType=FolderType.Common;
				} else if (s.get(string).equalsIgnoreCase("Internal")){
					folderType=FolderType.Internal;
				} else if (s.get(string).equalsIgnoreCase("Standard")){
					folderType=FolderType.Standard;
				}
				List<String> notCreatedFolders = nim.createFolderStructure(string, folderType, workspace, PageName.FundsPage, timeOut);
				if(notCreatedFolders.isEmpty()){
					flag=true;
				} else {
					String folderNames = createStringOutOfList(notCreatedFolders);
					BaseLib.sa.assertTrue(false,"Following folders are not created: "+folderNames);
				}
			}
//			if(verifyFolderStructure(folderStructureInExcel("FolderTemp"), 5)){
//				appLog.info("Folder strucuture is verified on build step 2 of 3.");
//			} else {
//				appLog.error("Folder structure is not verified on build step 2 of 3.");
//			}
		}
		return flag;
	}
	
	
	public boolean importFolderTemplate(String filePath,String sheetName, String folderTemplateName, WorkSpaceAction WithOrWithOutFolderTemplate, Workspace workspace, int timeOut){
		boolean flag = false;
		NIMPageBusinessLayer nim = new NIMPageBusinessLayer(driver);
		if(WithOrWithOutFolderTemplate.toString().equalsIgnoreCase(WorkSpaceAction.WITHOUTEMPLATE.toString())){
			appLog.info("Building workspace without folder");
			flag=true;
		} else if (WithOrWithOutFolderTemplate.toString().equalsIgnoreCase(WorkSpaceAction.IMPORTFOLDERTEMPLATE.toString())){
			if(click(driver, getImportFolderTemplateButton(workspace, timeOut), "folder template Imprt Button", action.BOOLEAN)){
				if(selectVisibleTextFromDropDown(driver, getDisplayDropDown(workspace, timeOut), "Display Drop Down", "All Templates")){
					if(click(driver, getFolderTemplateRadioButton(folderTemplateName, workspace, timeOut), folderTemplateName+" Folder template radio Button", action.BOOLEAN)){
						if(click(driver, getFolderTemplateImportButton(workspace, timeOut), "Import Button", action.BOOLEAN)){
							appLog.info("Successfully imported folder template: "+folderTemplateName);
							flag=true;
						} else {
							appLog.error("Import button cannot be clicked, So cannot create "+workspace.toString()+" workspace.");
							BaseLib.sa.assertTrue(false,"Import button cannot be clicked, So cannot create "+workspace.toString()+" workspace.");
						}
					} else {
						appLog.error(folderTemplateName+" Folder template is not present in the list.");
						BaseLib.sa.assertTrue(false,folderTemplateName+" Folder template is not present in the list.");
					}
				} else {
					appLog.error("Not able to select Option from the drop down.");
					BaseLib.sa.assertTrue(false,"Not able to select Option from the drop down.");
				}
			} else {
				appLog.error("Import folder template button cannot be clicked, So cannot create workspace.");
				BaseLib.sa.assertTrue(false,"Import folder template button cannot be clicked, So cannot create workspace.");
			}
		} else if (WithOrWithOutFolderTemplate.toString().equalsIgnoreCase(WorkSpaceAction.CREATEFOLDERTEMPLATE.toString())){
			Map<String, String> s = folderStructureInExcel(filePath, sheetName);
			Set<String> paths = s.keySet();
			Iterator<String> i = paths.iterator();
			i = paths.iterator();
			FolderType folderType = null;
			while (i.hasNext()) {
				String string = i.next();
				if (string.isEmpty())
					continue;
				System.out.println("\n\n\nCreating folder template\n\n\n");
				if(s.get(string).equalsIgnoreCase("Shared")){
					folderType=FolderType.Shared;
				} else if (s.get(string).equalsIgnoreCase("Common")){
					folderType=FolderType.Common;
				} else if (s.get(string).equalsIgnoreCase("Internal")){
					folderType=FolderType.Internal;
				} else if (s.get(string).equalsIgnoreCase("Standard")){
					folderType=FolderType.Standard;
				}
				List<String> notCreatedFolders = nim.createFolderStructure(string, folderType, workspace, PageName.FundsPage, timeOut);
				if(notCreatedFolders.isEmpty()){
					flag=true;
				} else {
					String folderNames = createStringOutOfList(notCreatedFolders);
					BaseLib.sa.assertTrue(false,"Following folders are not created: "+folderNames);
				}
			}
//			if(verifyFolderStructure(folderStructureInExcel("FolderTemp"), 5)){
//				appLog.info("Folder strucuture is verified on build step 2 of 3.");
//			} else {
//				appLog.error("Folder structure is not verified on build step 2 of 3.");
//			}
		}
		return flag;
	}
	
	/**
	 * @author Ankur Rana
	 * @param institutionOrLPName
	 * @param workspace
	 * @param timeOut
	 * @return true/false
	 */
	public boolean selectInstitutionOrLP(String institutionOrLPName, Workspace workspace, int timeOut){
		boolean flag = true;
		
		if(institutionOrLPName!=null){
			if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
				String[] multipleInstitutions = institutionOrLPName.split("<break>");
				for(int i = 0; i < multipleInstitutions.length; i++){
					if(click(driver, getInstituionCheckBoxOn3Of3(multipleInstitutions[i], workspace, timeOut), multipleInstitutions[i]+" Institution Check Box.", action.BOOLEAN)){
						appLog.info("Successfully Selected Institution '"+multipleInstitutions[i]+"'");
					} else {
						appLog.error("Not able to add '"+multipleInstitutions[i]+"' to the workspace.");
						BaseLib.sa.assertTrue(false,"Not able to add '"+multipleInstitutions[i]+"' to the workspace.");
						flag=false;
					}
				}
			} else {
				String[] multipleLP = institutionOrLPName.split("<break>");
				for(int i = 0; i < multipleLP.length; i++){
					if(click(driver, getLimitedPartnerCheckBox(multipleLP[i].split("/")[0], multipleLP[i].split("/")[1], workspace, timeOut), multipleLP[i]+" LP Check Box.", action.BOOLEAN)){
						appLog.info("Successfully Selected LP '"+multipleLP[i]+"'");
					} else {
						appLog.error("Not able to add '"+multipleLP[i]+"' to the workspace.");
						BaseLib.sa.assertTrue(false,"Not able to add '"+multipleLP[i]+"' to the workspace.");
						flag=false;
					}
				}
			}
		} else {
			appLog.info("Creating workspace without adding instituion or LP.");
		}
		return flag;
	}
	
	public boolean uploadFile(String path, String institutionOrLPName, String dragFromFolder, UploadFileActions uploadFileAddTo, UploadFileActions uploadUpdate, Workspace workspace, PageName pageName, int timeOut){
		String institutionName=null;
		String limitedPartner=null;
		boolean flag = false;
		int counter = 0;
		String dropImage = "DropLoc.JPG";
		CommonLib compare = new CommonLib();
		scrollDownThroughWebelement(driver, getWorkSpaceLabel(60), workspace.toString()+" View.");
		ThreadSleep(10000);
		switchToFrame(driver, 50, getFrame(PageName.FundsPage, timeOut));
		System.err.println("Switched to frame.");
		scrollDownThroughWebelement(driver, getWorkspaceSectionView(workspace, timeOut), workspace.toString()+" view.");
		if(institutionOrLPName!=null){
			if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
				institutionName = institutionOrLPName.split("<break>")[0];
			} else {
				institutionName = institutionOrLPName.split("<break>")[0].split("/")[0];
				limitedPartner = institutionOrLPName.split("<break>")[0].split("/")[1];
			}
		}
		if(verifyFolderPathdummy(path, institutionName, limitedPartner, null, pageName, workspace, timeOut)){
			ThreadSleep(5000);
			for(int z = 0; z == 0; z++)
			if(click(driver, getUploadIcon(workspace, timeOut), "Upload Icon", action.SCROLLANDBOOLEAN)){
				String parentWin = switchOnWindow(driver);
				if(parentWin!=null && !parentWin.isEmpty()){
					if(path.contains("(Common)") || path.contains("(Shared)") || path.contains("(Internal)")){
						appLog.info("Will directly upload the files.");
						flag = true;
					} else {
						if(institutionOrLPName.split("<break>").length>1){
							if(click(driver, getMultipleInstituionRadioButton(timeOut), "mulitple institution radio button", action.BOOLEAN)){
								if(selectMultipleInstitutionOrLP(institutionOrLPName, workspace, timeOut)){
									appLog.info("Successfully selected all the required Insitution or LP.");
								}
							} else {
								appLog.error("Not able to upload files in multiple instituions.");
								BaseLib.sa.assertTrue(false,"Not able to upload files in multiple instituions.");
							}
						} else if (uploadFileAddTo!=null && uploadFileAddTo.toString().equalsIgnoreCase(UploadFileActions.BulkUploaderOrFileSplitter.toString())){
							if(click(driver, getBulkUploaderOrFileSplitterRadioButton(timeOut), "Bulk Upload Radio Button", action.BOOLEAN)){
								dropImage="BulkUpload.jpg";
								flag = true;
							} else {
								appLog.error("Not able to click on bulk upload radio button.");
								BaseLib.sa.assertTrue(false,"Not able to click on bulk upload radio button.");
							}
						}
						System.err.println("Click on next button");
						if(click(driver, getUploadNextButton(workspace, timeOut), "Next Button", action.BOOLEAN)){
							flag = true;
						}
					}
					if(flag){
						if(dragDropFiles(dragFromFolder, dropImage)){
							List<WebElement> files = draggedFilesInFileUploadAtCRMSide();
							if (files.isEmpty()) {
								driver.close();
								driver.switchTo().window(parentWin);
								if (pageName.toString().equalsIgnoreCase(PageName.FundsPage.toString())) {
									System.out.println("Searching for the frame");
									switchToFrame(driver, 30, getFrame(PageName.FundsPage, 30));
								}
								if (counter > 1) {
									appLog.error(
											"Tried Upload for three times but still not able to upload file in folderpath: "
													+ path);
									BaseLib.sa.assertTrue(false,
											"Tried Upload for two times but still not able to upload file in folderpath: "
													+ path);
									return false;
								}
								counter++;
								z--;
								continue;
							}
							List<String> droppedFileNames = new ArrayList<String>();
							List<WebElement> droppedFiles = driver.findElements(By.xpath("//span[@class='File']/following-sibling::b"));
							if(!droppedFiles.isEmpty()){
								for(int i = 0; i < droppedFiles.size(); i++){
									droppedFileNames.add(getText(driver, droppedFiles.get(i), "Dropped Files", action.BOOLEAN).trim());
								}
								Collections.sort(droppedFileNames,compare);
								String previousuploadedFiles = ExcelUtils.readData("filepath", excelLabel.TestCases_Name, currentlyExecutingTC, identifyLabel(path, UploadFileActions.Upload));
								String newlyUploadedFiles = createStringOutOfList(droppedFileNames);
								if(previousuploadedFiles!=null && !previousuploadedFiles.isEmpty()){
									newlyUploadedFiles = previousuploadedFiles+"<break>"+newlyUploadedFiles;
								}
								if (ExcelUtils.writeData(newlyUploadedFiles, "FilePath", excelLabel.TestCases_Name, currentlyExecutingTC, identifyLabel(path, UploadFileActions.Upload))) {
									appLog.info("written uploaded file data to excel");
								}
								else {
									appLog.error("could not write uploaded files information to excel");
								}
							} else {
								appLog.error("0 files are dropped.");
								BaseLib.sa.assertTrue(false,"0 files are dropped.");
							}
							appLog.info("Successfully uploaded files.");
							
							BaseLib.ListofUploadedfiles = new ArrayList<String>();
							for (int j = 0; j < files.size(); j++) {
								BaseLib.ListofUploadedfiles.add(files.get(j).getText().trim());
							}
//							if (!uploadUpdate.toString().equalsIgnoreCase(UploadFileActions.Update.toString())) {
//								if (!path.contains("(Internal)")) {
//									for (int j = 0; j < files.size(); j++) {
//										BaseLib.uniquedocs.add(files.get(j).getText().trim());
//									}
//								}
//							}
							WebElement ele= BaseLib.edriver.findElement(By.cssSelector("#btnSave"));
							scrollDownThroughWebelement(driver, ele, "save button");
							try{
								ele.click();
								appLog.info("Clicked on Save Button");
//								if(click(driver, getUploadSaveButton(timeOut), "Upload Save Button", action.BOOLEAN)){
									if(uploadUpdate.toString().equalsIgnoreCase(UploadFileActions.Update.toString())){
										List<String> duplicateFileName = new ArrayList<String>();
										List<WebElement> duplicateFiles = driver.findElements(By.xpath("//table[@id='GridViewDuplicateFiles']//tr/td[1]//span"));
										if(!duplicateFiles.isEmpty()){
											for(int i = 0; i < duplicateFiles.size(); i++){
												duplicateFileName.add(getAttribute(driver, duplicateFiles.get(i), "Duplicate File Name", "title"));
											}
											String previousUpdatedFiles = ExcelUtils.readData("filepath", excelLabel.TestCases_Name, currentlyExecutingTC, identifyLabel(path, UploadFileActions.Update));
											String newlyupdatedFiles = createStringOutOfList(duplicateFileName);
											if(previousUpdatedFiles!=null && !previousUpdatedFiles.isEmpty()){
												newlyupdatedFiles = previousUpdatedFiles+"<break>"+newlyupdatedFiles;
											}
											ExcelUtils.writeData(newlyupdatedFiles, "filePath", excelLabel.TestCases_Name, currentlyExecutingTC, identifyLabel(path, UploadFileActions.Update));
										} else {
											appLog.error("There is no list of updated file");
											BaseLib.sa.assertTrue(false,"There is no list of updated file");
										}
										ele= BaseLib.edriver.findElement(By.cssSelector("#lnkReplaceAll"));
										scrollDownThroughWebelement(driver, ele, "update all button");
										try{
											ele.click();
											appLog.info("Clicked on update all button ");
											
										}catch(Exception e){
											appLog.error("Update Pop Up in not Displaying.");
											BaseLib.sa.assertTrue(false,"Update Pop Up in not Displaying.");
											flag = false;
										}
									}
									if(isAlertPresent(driver)){
										String alertText = switchToAlertAndGetMessage(driver, timeOut, action.GETTEXT);
										switchToAlertAndAcceptOrDecline(driver, timeOut, action.ACCEPT);
										if(alertText.contains(DocumentUploadSuccessMsg)){
											appLog.info("Upload success msg verified successfully.");
										} else {
											appLog.error("Upload successfull alert message is not verified.Expected: Document(s) uploaded successfully.\tActual: "+alertText);
											BaseLib.sa.assertTrue(false,"Upload successfull alert message is not verified.Expected: Document(s) uploaded successfully.\tActual: "+alertText);
											flag = false;
										}
									} else {
										appLog.info("upload Successfull alert message is not poping up.");
										BaseLib.sa.assertTrue(false,"upload Successfull alert message is not poping up.");
										flag = false;
									}
//								} else {
//									appLog.error("Cannot click on save button.");
//									BaseLib.sa.assertTrue(false,"Cannot click on save button.");
//									flag = false;
//								}
							}catch(Exception e){
								appLog.error("Cannot click on save button.");
								BaseLib.sa.assertTrue(false,"Cannot click on save button.");
								flag = false;
							}
						} else {
							appLog.error("Not able to upload files.");
							BaseLib.sa.assertTrue(false,"Not able to upload files.");
							flag = false;
						}
					}
				}
				driver.switchTo().window(parentWin);
			} else {
				appLog.error("Upload Icon cannot be clicked, So cannot continue with the upload process.");
				BaseLib.sa.assertTrue(false,"Upload Icon cannot be clicked, So cannot continue with the upload process.");
			}
		} else {
			appLog.error(path+" Folder structure is not verified.");
			BaseLib.sa.assertTrue(false,path+" Folder structure is not verified.");
		}
		return flag;
	}
	
	/**
	 * @author Ankur Rana
	 * @param path
	 * @param institutionName
	 * @param limitedPartner
	 * @param fundName
	 * @param PageName
	 * @param Workspace
	 * @param timeOut
	 * @return true/false
	 */
	public boolean verifyFolderPathdummy(String path, String institutionName, String limitedPartner, String fundName, PageName PageName, Workspace Workspace, int timeOut){
		String workspaceSelector="";
		boolean found = true;
		boolean flag = false;
		String middleXpath = "/../../following-sibling::ul/li/div//span[text()='";
		if(Workspace!=null){
			if(PageName.toString().equalsIgnoreCase("FundsPage") && Workspace.toString().equalsIgnoreCase("FundraisingWorkspace")){
				workspaceSelector = "//div[@id='frworkspace']";
				if(institutionName!=null) {
					middleXpath = "/../../../following-sibling::ul/li/div//span[text()='";
				}
			} else if (PageName.toString().equalsIgnoreCase("FundsPage") && Workspace.toString().equalsIgnoreCase("InvestorWorkspace")) {
				workspaceSelector = "//div[@id='invworkspace']";
				if(limitedPartner!=null) {
					middleXpath = "/../../../following-sibling::ul/li/div//span[text()='";
				}
			} else if((PageName.toString().equalsIgnoreCase("InstitutionsPage") || PageName.toString().equalsIgnoreCase("ContactsPage")) && Workspace.toString().equalsIgnoreCase("FundraisingWorkspace")){
				workspaceSelector = "//div[@id='divFrWorkspace']";
			} else if ((PageName.toString().equalsIgnoreCase("InstitutionsPage") || PageName.toString().equalsIgnoreCase("ContactsPage")) && Workspace.toString().equalsIgnoreCase("InvestorWorkspace")) {
				workspaceSelector = "//div[@id='Investorgrid_div']";
			}
		}
		String startingXpath=workspaceSelector+"//span[contains(text(),'All Folders')]/../../../ul/li/div//span[text()='";
		String endingXpath="']";
		String combinedXpath = "";
		if(institutionName!=null){
			if(limitedPartner!=null){
				path =institutionName+"/"+limitedPartner+"/"+path;
			} else {
				path =institutionName+"/"+path;
			}
		}else if(limitedPartner!=null && (PageName.toString().equalsIgnoreCase("CommitmentsPage")||PageName.toString().equalsIgnoreCase("InstitutionsPage"))){
			path = limitedPartner+"/"+path;
		}
		if(PageName.toString().equalsIgnoreCase("ContactsPage")||PageName.toString().equalsIgnoreCase("InstitutionsPage")){
			path = fundName+"/"+path;
		}
		String[] folderToFind = path.split("/");
		
		for(int i = 0 ; i < folderToFind.length ; i++){
			if(i==0){
				combinedXpath = startingXpath+folderToFind[i]+endingXpath;
			} else {
				combinedXpath += middleXpath + folderToFind[i] +endingXpath;
				middleXpath = "/../../following-sibling::ul/li/div//span[text()='";
			}
			if(click(driver, FindElement(driver, combinedXpath, folderToFind[i]+" :Folder", action.BOOLEAN, timeOut), folderToFind[i]+" :Folder", action.SCROLLANDBOOLEAN)){
				appLog.info("Successfully clicked on the folder "+folderToFind[i]);
			} else {
				appLog.error(folderToFind[i]+" :Folder not found.");
				found=false;
				break;
			}
		}
		if(found==false){
			if(isAlertPresent(driver)){
				String msg = switchToAlertAndGetMessage(driver, 30, action.GETTEXT);
				switchToAlertAndAcceptOrDecline(driver, 30, action.ACCEPT);
				if(msg.trim().toLowerCase().contains("error") || msg.trim().toLowerCase().contains("status") || msg.trim().toLowerCase().contains("code")){
					driver.navigate().refresh();
					if(getFrame(PageName, timeOut)!=null){
//						scrollDownThroughWebelement(driver, getDealRoomSection(30), "Deal room view.");
						switchToFrame(driver, 30, getFrame(PageName, timeOut));
					}
					if(verifyFolderPathdummy(path, institutionName, limitedPartner, fundName, PageName, Workspace, timeOut)){
						found=true;
					} else {
						found = false;
					}
				} else {
					String loc = screenshot(currentlyExecutingTC);
					appLog.error("Folder verification failed due to some intermittent issue. Kindly check the screenshot: "+loc);
					BaseLib.sa.assertTrue(false, "Folder verification failed due to some intermittent issue. Kindly check the screenshot: "+loc);
				}
			} else if (FindElement(driver, "//img[@class='poweredByImage']", null, action.BOOLEAN, 0)!=null){
				flag=true;
			} else if (FindElement(driver, "//img[@src='/resource/1511337238000/SiteSamples/img/clock.png']", null, action.BOOLEAN, 0)!=null){
				flag=true;
			} else if (FindElement(driver, "//img[@src='/resource/1511337238000/SiteSamples/img/warning.gif']", null, action.BOOLEAN, 0)!=null){
				flag=true;
			} else if (FindElement(driver, "//span[@class='title'][text()='Error: Error occurred while loading a Visualforce page.']", null, action.BOOLEAN, 0)!=null){
				flag=true;
			}
			if(flag){
				driver.navigate().refresh();
				if(getFrame(PageName, timeOut)!=null){
//					scrollDownThroughWebelement(driver, getDealRoomSection(30), "Deal room view.");
					switchToFrame(driver, 30, getFrame(PageName, timeOut));
				}
				if(verifyFolderPathdummy(path, institutionName, limitedPartner, fundName, PageName, Workspace, timeOut)){
					found=true;
				} else {
					found = false;
				}
			}
			
		}
		return found;
	}
	
	/**
	 * @author Ankit Jaiswa
	 * @param institutionOrLPName
	 * @param workspace
	 * @param timeOut
	 * @return true/false
	 */
	public boolean selectMultipleInstitutionOrLP(String institutionOrLPName, Workspace workspace, int timeOut){
		WebElement ele = null;
		String[] tempVariable;
		boolean returnValue=true;
		tempVariable = institutionOrLPName.split("<break>");
		for(int i = 0; i < tempVariable.length; i++){
			if(workspace.toString().equalsIgnoreCase(Workspace.FundraisingWorkspace.toString())){
				ele = FindElement(driver, "//td[text()='"+tempVariable[i]+"']/preceding-sibling::td/input", tempVariable[i]+" instituion CheckBox", action.BOOLEAN, timeOut);
			} else {
				ele = FindElement(driver, "//a[text()='"+tempVariable[i].split("/")[1]+"']/preceding-sibling::input", " LP checkBox", action.BOOLEAN, timeOut);
			}
			if(!isSelected(driver, ele, tempVariable[i]+" CheckBox")){
				if(click(driver, ele, tempVariable[i]+" CheckBox", action.BOOLEAN)){
					appLog.info("Successfully selected: "+tempVariable[i]);
				} else {
					appLog.error("not able to select: "+tempVariable[i]);
					BaseLib.sa.assertTrue(false,"Not able to select: "+tempVariable[i]);
					returnValue=false;
				}
			} else {
				appLog.info(tempVariable[i]+" is already selected.");
			}
		}

		return returnValue;
	}
	
	/**
	 * @author Ankur Rana
	 * @param dragFromFolder
	 * @param dropLocImageName
	 * @return true/false
	 */
	public boolean dragDropFiles(String dragFromFolder, String dropLocImageName) {
		Screen screen = new Screen();
		try {
			System.err.println(System.getProperty("user.dir")+"\\"+dragFromFolder);
			Process process = Runtime.getRuntime()
					.exec(System.getProperty("user.dir") + "/OpenFolder.exe" + " " + dragFromFolder);
			process.waitFor();
			if (dragFromFolder.contains("\\")) {
				dragFromFolder = (dragFromFolder
						.split(Pattern.quote("\\")))[(dragFromFolder.split(Pattern.quote("\\")).length - 1)];
			}
			process = Runtime.getRuntime().exec(".\\AutoIT\\activateFilesToUpload.exe" + " " + dragFromFolder);
			process.waitFor();
			screen.keyDown(Key.CTRL);
			screen.type("a");
			screen.keyUp(Key.CTRL);
			screen.drag(".\\AutoIT\\Drag.jpg");
			screen.mouseMove(-150, -100);
			Runtime.getRuntime().exec(".\\AutoIT\\UploadWindow.exe");
			try{
				screen.wait(".\\AutoIT\\"+dropLocImageName, 10);
				screen.dropAt(".\\AutoIT\\"+dropLocImageName);
				
			} catch (Exception e){
				System.err.println("After exception is running.");
				screen.wait(".\\"+dropLocImageName, 10);
				screen.dropAt(".\\"+dropLocImageName);
			}
			process = Runtime.getRuntime()
					.exec(System.getProperty("user.dir") + "\\AutoIT\\CloseFolder.exe" + " " + dragFromFolder);
			process.waitFor();
			System.err.println("Successfully cdroped files");
		} catch (FindFailed | IOException | InterruptedException e) {
			appLog.info("Issue with drag and drop");
			return false;
		}
		return true;
	}
	
	/**
	 * @param path
	 * @param uploadFileActions
	 * @return true/false
	 */
	public excelLabel identifyLabel(String path, UploadFileActions uploadFileActions){
		if(path.contains("(Common)")){
			if(uploadFileActions.toString().equalsIgnoreCase(UploadFileActions.Update.toString())){
				return excelLabel.UpdatedFileCommon;
			} else {
				return excelLabel.UploadedFileCommon;
			}
		} else if (path.contains("(Shared)")){
			if(uploadFileActions.toString().equalsIgnoreCase(UploadFileActions.Update.toString())){
				return excelLabel.UpdatedFileShared;
			} else {
				return excelLabel.UploadedFileShared;
			}
		} else if (path.contains("(Internal)")){
			if(uploadFileActions.toString().equalsIgnoreCase(UploadFileActions.Update.toString())){
				return excelLabel.UpdatedFileInternal;
			} else {
				return excelLabel.UploadedFileInternal;
			}
		} else {
			if(uploadFileActions.toString().equalsIgnoreCase(UploadFileActions.Update.toString())){
				return excelLabel.UpdatedFileStandard;
			} else {
				return excelLabel.UploadedFileStandard;
			}
		}
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param workspace
	 * @param timeOut
	 * @return true/false
	 */
	public boolean clearWorkSpace(Workspace workspace,int timeOut) {
		if(click(driver, getWorkSpaceClearBtn(workspace, timeOut), "workspace clear button", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS, "clicked on "+workspace.toString()+" clear button", YesNo.No);
			String parentID=switchOnWindow(driver);
			if(parentID!=null) {
				ThreadSleep(5000);
				if(matchTitle(driver, "Clear Workspace", 60)) {
					log(LogStatus.PASS, "Clear workspace popUp is open ", YesNo.No);
				if(clickUsingCssSelectorPath("div#subFolderDiv > div >a[title='Yes']", "workspace clear Yes button")) {
//				if(clickUsingJavaScript(driver,getClearWorkSpaceYesBtn(30),"yes button")) {
//					if (isAlertPresent(driver)) {
						log(LogStatus.PASS, "Clicked on Yes button", YesNo.No);
						String msg = switchToAlertAndGetMessage(driver, 50, action.GETTEXT);
						switchToAlertAndAcceptOrDecline(driver, 50, action.ACCEPT);
						if(msg.contains(clearWorkSpaceMsg)) {
							log(LogStatus.PASS, "Alert message is matched : "+clearWorkSpaceMsg+" and "+workspace.toString()+" is clear", YesNo.No);
							driver.switchTo().window(parentID);
							return true;
						}else {
							log(LogStatus.FAIL, "clear Workspace Error Message is not matched. Expected: "+clearWorkSpaceMsg, YesNo.Yes);
						}
						driver.switchTo().window(parentID);
//					} else {
//						log(LogStatus.FAIL, workspace.toString()+" clear Workspace alert message is not displayed", YesNo.Yes);
//						driver.close();
//						driver.switchTo().window(parentID);
//					}
					}else {
						log(LogStatus.FAIL, "Not able to click on clear workspace Yes button so cannot clear workspace", YesNo.Yes);
						driver.close();
						driver.switchTo().window(parentID);
					}
					
				}else {
					log(LogStatus.FAIL, "Clear workspace popup title is not matched so cannot clear "+workspace.toString(), YesNo.Yes);
				}
			}else {
				log(LogStatus.FAIL, "No new window is open so cannot clear workspace", YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on workspace clear button so cannot clear workspace", YesNo.Yes);
		}
		return false;
	}
}

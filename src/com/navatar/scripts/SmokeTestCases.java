package com.navatar.scripts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.navatar.generic.BaseLib;
import com.navatar.generic.EmailLib;
import com.navatar.generic.ExcelUtils;
import com.navatar.generic.SmokeCommonVariables;
import com.navatar.generic.SoftAssert;
import com.navatar.generic.EnumConstants.Buttons;
import com.navatar.generic.EnumConstants.PageName;
import com.navatar.generic.EnumConstants.TabName;
import com.navatar.generic.EnumConstants.TaskType;
import com.navatar.generic.EnumConstants.YesNo;
import com.navatar.generic.EnumConstants.action;
import com.navatar.generic.EnumConstants.excelLabel;
import com.navatar.pageObjects.BasePageBusinessLayer;
import com.navatar.pageObjects.BoxPageBusinesslayer;
import com.navatar.pageObjects.CommitmentsPageBusinessLayer;
import com.navatar.pageObjects.ExternalSideBusinessLayer;
import com.navatar.pageObjects.FundraisingsPageBusinessLayer;
import com.navatar.pageObjects.FundsPageBusinessLayer;
import com.navatar.pageObjects.FundsPageErrorMessage;
import com.navatar.pageObjects.HomePageBusineesLayer;
import com.navatar.pageObjects.InstitutionsPageBusinessLayer;
import com.navatar.pageObjects.LoginPageBusinessLayer;
import com.navatar.pageObjects.NIMPageBusinessLayer;
import com.navatar.pageObjects.NIMPageErrorMessage;
import com.navatar.pageObjects.PartnershipsPageBusinessLayer;
import com.navatar.pageObjects.PipeLineErrorMessage;
import com.navatar.pageObjects.PipelinesPageBusinessLayer;
import com.navatar.pageObjects.SetupPageBusinessLayer;
import com.navatar.pageObjects.TaskRayBusinessLayer;
import com.navatar.pageObjects.TaskRayErrorMessage;
import com.relevantcodes.extentreports.LogStatus;
import static com.navatar.generic.CommonLib.*;
import static com.navatar.generic.SmokeCommonVariables.*;
public class SmokeTestCases extends BaseLib {
	String passwordResetLink = null;
	
//	Scanner scn = new Scanner(System.in);
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc001_1_createCRMUserAndResetPassword(String environment, String mode) {
		SetupPageBusinessLayer setup = new SetupPageBusinessLayer(driver);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		HomePageBusineesLayer home = new HomePageBusineesLayer(driver);
		String parentWindow = null;
		String[] splitedUserLastName = removeNumbersFromString(crmUser1LastName);
		String UserLastName = splitedUserLastName[0] + lp.generateRandomNumber();
		String emailId = lp.generateRandomEmailId(gmailUserName);
		ExcelUtils.writeData(testCasesFilePath, UserLastName, "Users", excelLabel.Variable_Name, "User1",excelLabel.User_Last_Name);
		boolean flag = false;
		lp.CRMLogin(superAdminUserName, adminPassword, appName);
		if(home.clickOnTab(environment, mode, TabName.BoxSettings)) {
			log(LogStatus.PASS, "Clicked on Box Settings Tab ", YesNo.No);
			ThreadSleep(10000);
			driver.switchTo().frame(1);
			if(home.getBoxUserEmailId(60)!=null) {
				String aa = home.getBoxUserEmailId(20).getText().trim();
				log(LogStatus.INFO, "Box User Email Id : "+aa, YesNo.No);
				ExcelUtils.writedDataFromPropertyFile("BoxUserEmail", aa);
				log(LogStatus.INFO, "Box User Email Id : "+aa+" is written in credential file", YesNo.No);
			}else {
				log(LogStatus.FAIL, "Box user email id text is not found", YesNo.Yes);
				sa.assertTrue(false, "Box user email id text is not found");
			}
			switchToDefaultContent(driver);
		}else {
			log(LogStatus.FAIL, "Not able to click on box settings tab so cannot write Box User Email Id ", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on box settings tab so cannot write Box User Email Id ");
		}
		if (home.clickOnSetUpLink(environment, mode)) {
			if (mode.equalsIgnoreCase(Mode.Lightning.toString())) {
				parentWindow = switchOnWindow(driver);
				if (parentWindow != null) {
					if (setup.createPEUser(environment, mode, crmUser1FirstName, UserLastName, emailId, crmUserLience,crmUserProfile, null)) {
						log(LogStatus.PASS, "CRM User is created Successfully: " + crmUser1FirstName + " " + UserLastName, YesNo.No);
						ExcelUtils.writeData(testCasesFilePath, emailId, "Users", excelLabel.Variable_Name, "User1",excelLabel.User_Email);
						ExcelUtils.writeData(testCasesFilePath, UserLastName, "Users", excelLabel.Variable_Name, "User1",excelLabel.User_Last_Name);
						flag = true;

					}else {
						log(LogStatus.FAIL, "Not able to create CRM User ", YesNo.No);
						sa.assertTrue(false, "Not able to create CRM User ");
					}
				}else {
					log(LogStatus.SKIP,"No new window is open after click on setup link in lighting mode so cannot create CRM User1",YesNo.Yes);
					sa.assertTrue(false,"No new window is open after click on setup link in lighting mode so cannot create CRM User1");

				}
			}
			if (flag) {
				if (setup.installedPackages(environment, mode, crmUser1FirstName, UserLastName)) {
					log(LogStatus.PASS, "PE Package is installed Successfully in CRM User: " + crmUser1FirstName + " "+ UserLastName, YesNo.No);

				} else {
					log(LogStatus.ERROR,"Not able to install PE package in CRM User1: " + crmUser1FirstName + " " + UserLastName,YesNo.Yes);
					sa.assertTrue(false,"Not able to install PE package in CRM User1: " + crmUser1FirstName + " " + UserLastName);
					
				}
			}else{
				log(LogStatus.ERROR, "could not click on setup link, so cannot create crm user1", YesNo.Yes);
				sa.assertTrue(false, "could not click on setup link, so cannot create crm user1");
			}
			driver.close();
			driver.switchTo().window(parentWindow);
		}else {
			log(LogStatus.FAIL, "Not able to click on setup Link so cannot create CRM User1	", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on setup Link so cannot create CRM User1	");
		}
		lp.CRMlogout(environment, mode);
		closeBrowser();
		config(browserToLaunch);
		lp = new LoginPageBusinessLayer(driver);
		try {
			passwordResetLink = new EmailLib().getResetPasswordLink("passwordreset",gmailUserName,gmailPassword);
		} catch (InterruptedException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		appLog.info("ResetLinkIs: " + passwordResetLink);
		driver.get(passwordResetLink);
		if (lp.setNewPassword()) {
			log(LogStatus.PASS, "Password is set successfully for CRM User1: " + crmUser1FirstName + " " + UserLastName, YesNo.No);
			lp.CRMlogout(environment, mode);
		} else {
			log(LogStatus.ERROR, "Password is not set for CRM User1: " + crmUser1FirstName + " " + UserLastName,YesNo.Yes);
			sa.assertTrue(false, "Password is not set for CRM User1: " + crmUser1FirstName + " " + UserLastName);
		}
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc001_2_AddRemoveTabAndActivateLighting(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		int i=0;
		String addRemoveTabName="Pipelines"+","+"Partnerships"+","+"Commitments"+","+"Funds"+","+"Fundraisings"+","+"Box Settings"+","+"TaskRay"+","+"Navatar Investor Manager";
		String[][] userAndPassword = {{superAdminUserName,adminPassword},{crmUser1EmailID,adminPassword}};
		for (String[] userPass : userAndPassword) {
		lp.CRMLogin(userPass[0], userPass[1], appName);
		if (lp.addTab_Lighting(mode, addRemoveTabName, 5)) {
			log(LogStatus.PASS,"Tab added : "+addRemoveTabName,YesNo.No);
		} else {
			log(LogStatus.FAIL,"Tab not added : "+addRemoveTabName,YesNo.No);
			sa.assertTrue(false, "Tab not added : "+addRemoveTabName);
		}	
		lp.CRMlogout(environment,mode);
		if(i==0) {
			closeBrowser();
			config(browserToLaunch);
			lp = new LoginPageBusinessLayer(driver);
		}
		i++;
	}
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc001_3_createPreConditionData(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		InstitutionsPageBusinessLayer ins = new InstitutionsPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		if (tr.clickOnTab(environment, mode, TabName.InstituitonsTab)) {
			log(LogStatus.PASS, "clicked on institution Tab ", YesNo.No);
			if (ins.createInstitution(environment, mode, SmokeCOM1, SmokeCOM1_RecordType, null, null)) {
				log(LogStatus.PASS,"Company is created : " + SmokeCOM1, YesNo.No);
			} else {
				log(LogStatus.ERROR, "Not able to click on create Company : " + SmokeCOM1, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on create Company : " + SmokeCOM1);
			}
		}else {
			log(LogStatus.ERROR, "Not able to click on institution tab so cannot create compnay "+SmokeCOM1, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on institution tab so cannot create compnay "+SmokeCOM1);
		}
		if (tr.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab ", YesNo.No);
			String[] splitedPipeLineName = removeNumbersFromString(Smoke_PL1Name);
			Smoke_PL1Name = splitedPipeLineName[0] + lp.generateRandomNumber();
			if (pipe.createPipeLine(environment, mode, Smoke_PL1Name, Smoke_PL1CompanyName, Smoke_PL1Stage, null,Smoke_PL1RecordType)) {
				log(LogStatus.PASS,"pipeLine is created : " + Smoke_PL1Name, YesNo.No);
				
			} else {
				log(LogStatus.ERROR, "Not able to click on create pipeLine : " + Smoke_PL1Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on create pipeLine : " + Smoke_PL1Name);
			}
			ExcelUtils.writeData(testCasesFilePath, Smoke_PL1Name, "PipeLine", excelLabel.Variable_Name, "SmokePL1",excelLabel.Pipeline_Name);
			ExcelUtils.writeData(testCasesFilePath, Smoke_PL1Name, "Project", excelLabel.Variable_Name, "SmokePR1",excelLabel.Pipeline_Name);
			ExcelUtils.writeData(testCasesFilePath, Smoke_PL1Name, "Project", excelLabel.Variable_Name, "SmokePR2",excelLabel.Pipeline_Name);
			scv=new SmokeCommonVariables(this);
		}else {
			log(LogStatus.ERROR, "Not able to click on pipeLine tab so cannot create pipeLine "+SmokeCOM1, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on pipeLine tab so cannot create pipeLine "+SmokeCOM1);
		}
		if (tr.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(click(driver, tr.getNewFeaturePopUpDissmissBtn(30), "new feature pop dismiss button", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.INFO,"clicked on new feature popUp dismiss button" , YesNo.No);
			}else {
				log(LogStatus.INFO, "New Feature popUp is not displaying on taskRay Page", YesNo.No);
			}
			if (tr.createProject(environment, mode,Smoke_Project1Name,Smoke_Project1Type, null,Smoke_PL1Name, null, Buttons.SaveClose, 10)) {
				log(LogStatus.PASS,"Project Created : "+Smoke_Project1Name,YesNo.No);
				ThreadSleep(2000);
				switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
				if(tr.createNewTask(environment, mode, Smoke_Task1Name,Smoke_Task1ProjectName,Smoke_Task1Status, null, 30)) {
					log(LogStatus.PASS, "task is created "+Smoke_Task1Name, YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to create task :"+Smoke_Task1Name,YesNo.Yes);
					sa.assertTrue(false, "Not able to create task :"+Smoke_Task1Name);
				}
				switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
				if(tr.createNewTask(environment, mode, Smoke_Task3Name,Smoke_Task3ProjectName,Smoke_Task3Status, null, 30)) {
					log(LogStatus.PASS, "task is created "+Smoke_Task3Name, YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to create task :"+Smoke_Task3Name,YesNo.Yes);
					sa.assertTrue(false, "Not able to create task :"+Smoke_Task3Name);
				}
				switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
				if(tr.createNewTask(environment, mode, Smoke_Task5Name,Smoke_Task5ProjectName,Smoke_Task5Status, null, 30)) {
					log(LogStatus.PASS, "task is created "+Smoke_Task5Name, YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to create task :"+Smoke_Task5Name,YesNo.Yes);
					sa.assertTrue(false, "Not able to create task :"+Smoke_Task5Name);
				}
			} else {
				log(LogStatus.PASS,"Project Not Created : "+Smoke_Project1Name,YesNo.Yes);
				sa.assertTrue(false, "Project Not Created : "+Smoke_Project1Name);
			}
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if (tr.createProject(environment, mode,Smoke_Project2Name,Smoke_Project2Type, null,Smoke_PL1Name, null, Buttons.SaveClose, 10)) {
				log(LogStatus.PASS,"Project Created : "+Smoke_Project2Name,YesNo.No);
				ThreadSleep(2000);
				switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
				if(tr.createNewTask(environment, mode, Smoke_Task2Name,Smoke_Task2ProjectName,Smoke_Task2Status, null, 30)) {
					log(LogStatus.PASS, "task is created "+Smoke_Task2Name, YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to create task :"+Smoke_Task2Name,YesNo.Yes);
					sa.assertTrue(false, "Not able to create task :"+Smoke_Task2Name);
				}
				switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
				if(tr.createNewTask(environment, mode, Smoke_Task4Name,Smoke_Task4ProjectName,Smoke_Task4Status, null, 30)) {
					log(LogStatus.PASS, "task is created "+Smoke_Task4Name, YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to create task :"+Smoke_Task4Name,YesNo.Yes);
					sa.assertTrue(false, "Not able to create task :"+Smoke_Task4Name);
				}
				switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
				if(tr.createNewTask(environment, mode, Smoke_Task6Name,Smoke_Task6ProjectName,Smoke_Task6Status, null, 30)) {
					log(LogStatus.PASS, "task is created "+Smoke_Task6Name, YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to create task :"+Smoke_Task6Name,YesNo.Yes);
					sa.assertTrue(false, "Not able to create task :"+Smoke_Task6Name);
				}
			} else {
				log(LogStatus.PASS,"Project Not Created : "+Smoke_Project2Name,YesNo.Yes);
				sa.assertTrue(false, "Project Not Created : "+Smoke_Project2Name);
			}
		} else {
			log(LogStatus.PASS, "Not able to create project "+Smoke_Project1Name+" and "+Smoke_Project2Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to create project "+Smoke_Project1Name+" and "+Smoke_Project2Name);
		}

		lp.CRMlogout(environment, mode);
		sa.assertAll();
}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc002_VerifyProjectsTaskOpenAndTaskCompleteOnPipeLinePage(String environment, String mode) {

		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe= new PipelinesPageBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		String[] bb= {"Project"," Task: Open","Task: Complete"};
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS,"Clicked on PipeLine Tab", YesNo.No);
			appLog.info("Clicked on PipeLine Tab");
			
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 20), "due deligence tab", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due diligence tab", YesNo.No);
					
					boolean[] aa = {pipe.verifyingHeaderonSubTab(environment, mode, Header.Project,1,10),pipe.verifyingHeaderonSubTab(environment, mode, Header.OpenTask,2,10),
							pipe.verifyingHeaderonSubTab(environment, mode, Header.CompletedTask,1,10)};
					
					for (int i = 0; i < aa.length; i++) {
						if(aa[i]) {
							log(LogStatus.PASS, bb[i]+" header count is displaying on due diligence Page", YesNo.No);
						}else {
							log(LogStatus.FAIL, bb[i]+" header count is not displaying on due diligence Page", YesNo.Yes);
							sa.assertTrue(false, bb[i]+" header count is not displaying on due diligence Page");
						}
					} 
					
					String[] headerNameList= {"Project","Completed","Owner"};
					
					String[][] DataList= {{Smoke_Project2Name,"33.33%",crmUser1FirstName+" "+crmUser1LastName}};
					
				   SoftAssert projectData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Due_Diligence,Header.Project, headerNameList,DataList);						
				   sa.combineAssertions(projectData);
					
				   String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
				   
				   String[][] DataList1= {{Smoke_Task4Name,Smoke_Task4Status,"",crmUser1FirstName+" "+crmUser1LastName,"0","Complete","Documents"},
						   {Smoke_Task2Name,Smoke_Task2Status,"",crmUser1FirstName+" "+crmUser1LastName,"0","Complete","Documents"}};
					
				   SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Due_Diligence,Header.OpenTask, headerNameList1,DataList1);						
				   sa.combineAssertions(TaskOpenData);
				   
				   
				   	String[] headerNameList2= {"Name","End Date","Owner","Documents"};
				   
				   String[][] DataList2= {{Smoke_Task6Name,"",crmUser1FirstName+" "+crmUser1LastName,"0","Documents"}};
					
				   SoftAssert TaskCompletedData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Due_Diligence,Header.CompletedTask, headerNameList2,DataList2);						
				   sa.combineAssertions(TaskCompletedData);
				   
					
				}else {
					log(LogStatus.FAIL, "Not able to click on due diligence tab so cannot verify data on it", YesNo.Yes);
					sa.assertTrue(false, "Not able to click on due diligence tab so cannot verify data on it");
				}
			refresh(driver);
			if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "due evaluation tab", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					boolean[] aa = {pipe.verifyingHeaderonSubTab(environment, mode, Header.Project,1,10),pipe.verifyingHeaderonSubTab(environment, mode, Header.OpenTask,2,10),
							pipe.verifyingHeaderonSubTab(environment, mode, Header.CompletedTask,1,10)};
					
					for (int i = 0; i < aa.length; i++) {
						if(aa[i]) {
							log(LogStatus.PASS, bb[i]+" header count is displaying on deal evaluation Page", YesNo.No);
						}else {
							log(LogStatus.FAIL, bb[i]+" header count is not displaying on deal evaluation Page", YesNo.Yes);
							sa.assertTrue(false, bb[i]+" header count is not displaying on deal evaluation Page");
						}
					}
					String[] headerNameList= {"Project","Completed","Owner"};
					
					String[][] DataList= {{Smoke_Project1Name,"33.33%",crmUser1FirstName+" "+crmUser1LastName}};
					
				   SoftAssert projectData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Deal_Evaluation,Header.Project, headerNameList,DataList);						
				   sa.combineAssertions(projectData);
					
				   String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
				   String[][] DataList1= {{Smoke_Task1Name,Smoke_Task1Status,"",crmUser1FirstName+" "+crmUser1LastName,"0","Complete","Documents"},
						   {Smoke_Task3Name,Smoke_Task3Status,"",crmUser1FirstName+" "+crmUser1LastName,"0","Complete","Documents"}};
					
				   SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Deal_Evaluation,Header.OpenTask, headerNameList1,DataList1);						
				   sa.combineAssertions(TaskOpenData);
				   
					String[] headerNameList2= {"Name","End Date","Owner","Documents"};
					   
					String[][] DataList2= {{Smoke_Task5Name,"",crmUser1FirstName+" "+crmUser1LastName,"0","Documents"}};
				   
					
				   SoftAssert TaskCompletedData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Deal_Evaluation,Header.CompletedTask, headerNameList2,DataList2);						
				   sa.combineAssertions(TaskCompletedData);
					
					
					
				}else {
					log(LogStatus.FAIL, "Not able to click on due evaluation tab so cannot verify data on it", YesNo.Yes);
					sa.assertTrue(false, "Not able to click on due evaluation tab so cannot verify data on it");
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify data", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify data");
			}
		} else {
			log(LogStatus.FAIL, "Not Able to Click on PipeLine Tab so cannot verify data on "+Smoke_PL1Name, YesNo.Yes);
			sa.assertTrue(false, "Not Able to Click on PipeLine Tab so cannot verify data on "+Smoke_PL1Name);
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
		
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc003_inviteUserFromBoxAndResetPassword(String environment, String mode) {
		BoxPageBusinesslayer box = new BoxPageBusinesslayer(driver);
		boolean flag = false;
		box.boxLogin(BoxUserEmail,BoxPassword, YesNo.No);
		if(box.inviteUserFromAdminConsole(crmUser1FirstName+" "+crmUser1LastName, crmUser1EmailID)) {
			log(LogStatus.PASS, "User is invited from box : "+crmUser1FirstName+" "+crmUser1LastName, YesNo.No);
			flag= true;
			
		}else {
			log(LogStatus.FAIL, "Not able to invite user from Box "+crmUser1FirstName+" "+crmUser1LastName, YesNo.Yes);
			sa.assertTrue(false, "Not able to invite user from Box "+crmUser1FirstName+" "+crmUser1LastName);
		}
		box.boxLogOut();
		if(flag) {
			closeBrowser();
			config(browserToLaunch);
			box = new BoxPageBusinesslayer(driver);
			for(int i=0; i<2 ; i++){
				try {
					passwordResetLink = new EmailLib().getEMailContent(gmailUserName, gmailPassword, "noreply@box.com", crmUser1EmailID, crmUser1FirstName+" "+crmUser1LastName+", Your Enterprise for");
				} catch (InterruptedException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				if(passwordResetLink!=null){
					break;
				}
				
			}
			if(passwordResetLink!=null) {
				String[] splitedLink =passwordResetLink.split("Log In Now:");
				System.err.println("---------------0------------------");
				System.err.println(splitedLink[0]);
				System.err.println("---------------1------------------");
				System.err.println(splitedLink[1]);
				String[] bb=splitedLink[1].split("Store your files securely");
				System.err.println("--------------2---------------");
				System.err.println(bb[0]);
				System.err.println("--------------final URL ---------------");
				System.err.println("--------------3---------------");
				System.err.println(bb[1]);
//				String[] bb1=bb[0].split("https://app.box.com/");
				
//				System.err.println(bb1[2]);
				driver.get(bb[0]);
				ThreadSleep(3000);
				if(box.ResetPassword(adminPassword, crmUser1EmailID)) {
					log(LogStatus.PASS, "Password is reset successfully of user "+crmUser1EmailID, YesNo.No);
					ThreadSleep(3000);
					if(matchTitle(driver, "Box | Login", 60)) {
						log(LogStatus.PASS, "Login Page is open after reset password of user "+crmUser1EmailID, YesNo.No);
					}else {
						log(LogStatus.FAIL, "Box Login Page is not open after reset password of user "+crmUser1EmailID, YesNo.Yes);
						sa.assertTrue(false, "Box Login Page is not open after reset password of user "+crmUser1EmailID);
					}
				}else {
					log(LogStatus.FAIL, "Password is not reset of user "+crmUser1EmailID, YesNo.Yes);
					sa.assertTrue(false, "Password is not reset of user "+crmUser1EmailID);
				}
			}else {
				log(LogStatus.FAIL, "Not able to reset password link from gmail so cannot update password of use "+crmUser1EmailID, YesNo.Yes);
				sa.assertTrue(false, "Not able to reset password link from gmail so cannot update password of use "+crmUser1EmailID);
			}
		}else {
			log(LogStatus.FAIL,crmUser1EmailID+" : user is not invited from box so cannot reset password ", YesNo.Yes);
			sa.assertTrue(false, crmUser1EmailID+" : user is not invited from box so cannot reset password ");
		}
		sa.assertAll();
		
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc004_VerifyNavatarDocumentsAndTagDocumentspopUp(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS,"Clicked on PipeLine Tab", YesNo.No);

			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "due evaluation tab", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					for(int i=0; i<2; i++) {
						if(click(driver, pipe.getDocumentButton(Smoke_Task1Name, 30), Smoke_Task1Name+" document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on "+Smoke_Task1Name+" document button", YesNo.No);
							if(i==0) {
								ThreadSleep(5000);
								String[] ss= {"refresh button","task Name Label text","tag or document button"};

								if(pipe.getNavatarDocumentsLabelText(10)!=null) {
									log(LogStatus.PASS, "Navatar Document Label is displaying on due evaluation Page", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Navatar Document Label is not displaying on due evaluation Page", YesNo.Yes);
									sa.assertTrue(false,"Navatar Document Label is not displaying on due evaluation Page");
								}
								if(pipe.getCrossIconOnNavatarDocumentPopUp(10)!=null) {
									log(LogStatus.PASS, "Cross icon is displaying on due evaluation Page", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Cross icon is not displaying on Navatar Document Page", YesNo.Yes);
									sa.assertTrue(false,"Cross icon is not displaying on due Navatar Document Page");
								}

								switchToFrame(driver,30,pipe.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 60));

								WebElement[] ele = {pipe.getRefreshButton(10),pipe.taskLabelNameOnNavtarDocumentsPopUp(Smoke_Task1Name, 40),pipe.getTagOrUploadDocumentsButton(10)};

								for (int i1 = 0; i1 < ele.length; i1++) {
									if(ele[i1]!=null) {
										log(LogStatus.PASS, ss[i1]+" is displaying on Navatar Document Page", YesNo.No);
									}else {
										log(LogStatus.FAIL, ss[i1]+" is not displaying on Navatar Document Page", YesNo.Yes);
										sa.assertTrue(false, ss[i1]+" is not displaying on Navatar Document Page");
									}
								}
								if(compareMultipleList(driver, "Document Name,Action", pipe.getDocumentNameAndActionColumnOnNavatarDocumentPopUp()).isEmpty()) {
									log(LogStatus.PASS, "Column name is verified on Navatar Document popup", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Column name is not verified on Navatar Document popup", YesNo.Yes);
									sa.assertTrue(false, "Column name is not verified on Navatar Document popup");
								}

								switchToDefaultContent(driver);

								if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
								}
							}
							if(i==1) {
								String parentID=pipe.TagOrDocumentBoxLogin(crmUser1EmailID,adminPassword);
								if(parentID!=null) {
									log(LogStatus.PASS, "Successfully Login In Box ", YesNo.No);
									if(pipe.getTagDocumentsLabelText(30).getText().trim().equalsIgnoreCase("Documents")) {
										log(LogStatus.PASS, "Tag Documents label text is visible", YesNo.No);
									}else {
										log(LogStatus.FAIL, "Tag Documents label text is not visible ", YesNo.Yes);
										sa.assertTrue(false, "Tag Documents label text is not visible ");
									}
									if(clickUsingJavaScript(driver, pipe.getTagDocumentsCancelBtn(30), "cancel button")) {
										log(LogStatus.PASS, "clicked on cancel button ", YesNo.No);

									}else {
										log(LogStatus.FAIL, "Not able to click on cancel so cannot close tag document window ", YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel so cannot close tag document window ");
										driver.close();
									}
									driver.switchTo().window(parentID);
									if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
										log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
									}
								}else {
									log(LogStatus.FAIL, "Not able to login in box so cannot veify tag documents popUp",YesNo.Yes);
									sa.assertTrue(false, "Not able to login in box so cannot veify tag documents popUp");
								}
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on "+Smoke_Task1Name+" document button so cannot verify navatar document popup fuctionality ", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on "+Smoke_Task1Name+" document button so cannot verify navatar document popup fuctionality ");
						}

					}
				}else {
					log(LogStatus.FAIL, "Not able to click on due evaluation tab so cannot verify data on it", YesNo.Yes);
					sa.assertTrue(false, "Not able to click on due evaluation tab so cannot verify data on it");
				}
				refresh(driver);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 20), "due deligence tab", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due diligence tab", YesNo.No);
					for(int i=0; i<2; i++) {
						if(click(driver, pipe.getDocumentButton(Smoke_Task2Name, 30), Smoke_Task2Name+" document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on "+Smoke_Task2Name+" document button", YesNo.No);
							if(i==0) {
								String[] ss= {"refresh button","task Name Label text","tag or document button"};

								if(pipe.getNavatarDocumentsLabelText(10)!=null) {
									log(LogStatus.PASS, "Navatar Document Label is displaying on due evaluation Page", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Navatar Document Label is not displaying on due evaluation Page", YesNo.Yes);
									sa.assertTrue(false,"Navatar Document Label is not displaying on due evaluation Page");
								}
								if(pipe.getCrossIconOnNavatarDocumentPopUp(10)!=null) {
									log(LogStatus.PASS, "Cross icon is displaying on due evaluation Page", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Cross icon is not displaying on Navatar Document Page", YesNo.Yes);
									sa.assertTrue(false,"Cross icon is not displaying on due Navatar Document Page");
								}

								switchToFrame(driver,30,pipe.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 60));

								WebElement[] ele = {pipe.getRefreshButton(10),pipe.taskLabelNameOnNavtarDocumentsPopUp(Smoke_Task2Name, 10),pipe.getTagOrUploadDocumentsButton(10)};

								for (int i1 = 0; i1 < ele.length; i1++) {
									if(ele[i1]!=null) {
										log(LogStatus.PASS, ss[i1]+" is displaying on Navatar Document Page", YesNo.No);
									}else {
										log(LogStatus.FAIL, ss[i1]+" is not displaying on Navatar Document Page", YesNo.Yes);
										sa.assertTrue(false, ss[i1]+" is not displaying on Navatar Document Page");
									}
								}
								if(compareMultipleList(driver, "Document Name,Action", pipe.getDocumentNameAndActionColumnOnNavatarDocumentPopUp()).isEmpty()) {
									log(LogStatus.PASS, "Column name is verified on Navatar Document popup", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Column name is not verified on Navatar Document popup", YesNo.Yes);
									sa.assertTrue(false, "Column name is not verified on Navatar Document popup");
								}

							}
							if(i==1) {
								switchToFrame(driver,30,pipe.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 30));
								if(click(driver, pipe.getTagOrUploadDocumentsButton(30), "tag or document button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on tag or document button", YesNo.No);
									ThreadSleep(5000);
									String parentID=switchOnWindow(driver);
									if(parentID!=null) {
										if(pipe.getTagDocumentsLabelText(30).getText().trim().equalsIgnoreCase("Documents")) {
											log(LogStatus.PASS, "Documents label text is visible", YesNo.No);
										}else {
											log(LogStatus.FAIL, "Documents label text is not visible ", YesNo.Yes);
											sa.assertTrue(false, "Documents label text is not visible ");
										}
										if(clickUsingJavaScript(driver,pipe.getTagDocumentsCancelBtn(30),"cancel button")) {
											log(LogStatus.PASS, "clicked on cancel button ", YesNo.No);

										}else {
											log(LogStatus.FAIL, "Not able to click on cancel so cannot close tag document window ", YesNo.Yes);
											sa.assertTrue(false, "Not able to click on cancel so cannot close tag document window ");
											driver.close();
										}
										driver.switchTo().window(parentID);
									}else {
										log(LogStatus.FAIL, "Not able to login in box so cannot veify tag documents popUp",YesNo.Yes);
										sa.assertTrue(false, "Not able to login in box so cannot veify tag documents popUp");
									}
								}
							}
							switchToDefaultContent(driver);
							if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
							}

						}else {
							log(LogStatus.FAIL, "Not able to click on "+Smoke_Task2Name+" document button so cannot verify navatar document popup fuctionality ", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on "+Smoke_Task2Name+" document button so cannot verify navatar document popup fuctionality ");
						}

					}
				}else {
					log(LogStatus.FAIL, "Not able to click on due diligence tab so cannot verify data on it", YesNo.Yes);
					sa.assertTrue(false, "Not able to click on due diligence tab so cannot verify data on it");
				}

			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify data", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify data");
			}
		} else {
			log(LogStatus.FAIL, "Not Able to Click on PipeLine Tab so cannot verify data on "+Smoke_PL1Name, YesNo.Yes);
			sa.assertTrue(false, "Not Able to Click on PipeLine Tab so cannot verify data on "+Smoke_PL1Name);
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Pending, Smoke_Task1Name, Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task1Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							switchToFrame(driver, 60, tr.getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 60));

							String[] ss= {"refresh button","task Name Label text","tag or document button"};


							WebElement[] ele = {pipe.getRefreshButton(10),pipe.taskLabelNameOnNavtarDocumentsPopUp(Smoke_Task2Name, 10),pipe.getTagOrUploadDocumentsButton(10)};

							for (int i1 = 0; i1 < ele.length; i1++) {
								if(ele[i1]!=null) {
									log(LogStatus.PASS, ss[i1]+" is displaying on Navatar Document Page", YesNo.No);
								}else {
									log(LogStatus.FAIL, ss[i1]+" is not displaying on Navatar Document Page", YesNo.Yes);
									sa.assertTrue(false, ss[i1]+" is not displaying on Navatar Document Page");
								}
							}
							if(compareMultipleList(driver, "Document Name,Action", pipe.getDocumentNameAndActionColumnOnNavatarDocumentPopUp()).isEmpty()) {
								log(LogStatus.PASS, "Column name is verified on Navatar Document popup", YesNo.No);
							}else {
								log(LogStatus.FAIL, "Column name is not verified on Navatar Document popup", YesNo.Yes);
								sa.assertTrue(false, "Column name is not verified on Navatar Document popup");
							}
							if(click(driver, pipe.getTagOrUploadDocumentsButton(30), "tag or document button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on tag or document button", YesNo.No);
								ThreadSleep(5000);
								String parentID=switchOnWindow(driver);
								if(parentID!=null) {
									if(pipe.getTagDocumentsLabelText(30).getText().trim().equalsIgnoreCase("Documents")) {
										log(LogStatus.PASS, "Documents label text is visible", YesNo.No);
									}else {
										log(LogStatus.FAIL, "Documents label text is not visible ", YesNo.Yes);
										sa.assertTrue(false, "Documents label text is not visible ");
										
									}
									if(clickUsingJavaScript(driver,pipe.getTagDocumentsCancelBtn(30),"cancel button")) {
										log(LogStatus.PASS, "clicked on cancel button ", YesNo.No);

									}else {
										log(LogStatus.FAIL, "Not able to click on cancel so cannot close tag document window ", YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel so cannot close tag document window ");
										driver.close();
									}
									driver.switchTo().window(parentID);
								}else {
									log(LogStatus.FAIL, "Not able to login in box so cannot veify tag documents popUp",YesNo.Yes);
									sa.assertTrue(false, "Not able to login in box so cannot veify tag documents popUp");
								}
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check popup functionality", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check popup functionality");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task1Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}

	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc005_createFolderUnderPipeLineAndVerifyFolderStructure(String environment, String mode) {
		String[] folderstructre=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.FolderStructure).split("<break>");
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		BoxPageBusinesslayer box = new BoxPageBusinesslayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Documents, 30), "Documents tab", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on documents tab", YesNo.No);
					ThreadSleep(10000);
					switchToFrame(driver, 30, pipe.getFrame(PageName.DocumentsPageFrameOnPipeLinePage, 30));
					ThreadSleep(5000);
					driver.switchTo().frame(0);
					if(click(driver,pipe.getBoxLoginButtonOnDocumentPage(30),"login box ", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on login button", YesNo.No);
						ThreadSleep(5000);
						if(sendKeys(driver, pipe.getBoxUserName(60),crmUser1EmailID,"Box username Text Box", action.BOOLEAN)) {
							log(LogStatus.PASS, "passed value in user name text box : "+crmUser1EmailID, YesNo.No);
							if(sendKeys(driver, pipe.getBoxPasswordTextBox(60), adminPassword,"Box Password Text Box", action.BOOLEAN)) {
								log(LogStatus.PASS, "passed value in password text box : "+adminPassword, YesNo.No);
								if(click(driver, pipe.getBoxAuthorizeButton(60), "Authorize button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on authorized button", YesNo.No);
									ThreadSleep(10000);
									if(clickUsingJavaScript(driver, pipe.getGrantAccessToBoxButton(60), "Grant Access To Box Button")) {
										log(LogStatus.PASS, "clicked on grant access to box button",YesNo.No);
										ThreadSleep(10000);
										if(clickUsingJavaScript(driver, pipe.getCreateFolderButtonOnDocumentsPage(60), "create folder button")) {
											log(LogStatus.PASS, "clicked on create folder button", YesNo.No);
											switchToDefaultContent(driver);
											String[][] aa = {{folderstructre[0]},{folderstructre[1]},{folderstructre[2]}};
											if(box.createFolderStructureInBox(aa).isEmpty()) {
												log(LogStatus.PASS, "Folder is created successfully in Box ", YesNo.No);
											}else {
												log(LogStatus.FAIL, "Folder is not created in Box ", YesNo.Yes);
												sa.assertTrue(false, "Folder is not created in Box ");
											}
										}else {
											log(LogStatus.FAIL, "Not able to click on create folder button so cannot create folder", YesNo.Yes);
											sa.assertTrue(false, "Not able to click on create folder button so cannot create folder");
										}
									}else {
										log(LogStatus.FAIL, "Not able to click on grant access to box button so cannot login in box ", YesNo.Yes);
										sa.assertTrue(false, "Not able to click on grant access to box button so cannot login in box ");
									}
								}else {
									log(LogStatus.FAIL, "Not able to click on authorized button so cannot login in box ", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on authorized button so cannot login in box ");
								}
								
							}else {
								log(LogStatus.FAIL, "Not able to pass value in password text box so cannot login in box ", YesNo.Yes);
								sa.assertTrue(false, "Not able to pass value in password text box so cannot login in box ");
							}
						}else {
							log(LogStatus.FAIL, "Not able to pass value in username text box so cannot login in box ", YesNo.Yes);
							sa.assertTrue(false, "Not able to pass value in username text box so cannot login in box ");
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on login button so cannot create folder", YesNo.Yes);
						sa.assertTrue(false, "Not able to click on login button so cannot create folder");
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on documents tab so cannot create folder in Box ",YesNo.Yes);
					sa.assertTrue(false, "Not able to click on documents tab so cannot create folder in Box ");
				}
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					ThreadSleep(2000);
					if(click(driver, pipe.getDocumentButton(Smoke_Task1Name, 30), Smoke_Task1Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task1Name+" document button", YesNo.No);
						switchToFrame(driver,30,pipe.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 30));
						if(click(driver, pipe.getTagOrUploadDocumentsButton(30), "tag or document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on tag or document button", YesNo.No);
							ThreadSleep(5000);
							String parentID=switchOnWindow(driver);
							if(parentID!=null) {
								for(String s : folderstructre) {
									if(box.verifyFolderStructure(driver, s)) {
										log(LogStatus.PASS, "Folder Structure is verified on Tag or upload page", YesNo.No);
									}else {
										log(LogStatus.PASS, "Folder Structure is not verified on Tag or upload page", YesNo.Yes);
										sa.assertTrue(false, "Folder Structure is not verified on Tag or upload page");
									}
								}
								if(clickUsingJavaScript(driver,pipe.getTagDocumentsCancelBtn(30),"cancel button")) {
									log(LogStatus.PASS, "clicked on cancel button ", YesNo.No);

								}else {
									log(LogStatus.FAIL, "Not able to click on cancel so cannot close tag document window ", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on cancel so cannot close tag document window ");
									driver.close();
								}
								driver.switchTo().window(parentID);
								if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
								}
							}else {
								log(LogStatus.FAIL, "No new window is found so cannot verify folder structure in Navatar Docuemnts",YesNo.Yes);
								sa.assertTrue(false, "No new window is found so cannot verify folder structure in Navatar Docuemnts");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on tag or documents button so cannot verify folder structure in Navatar Docuemnts", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on tag or documents button so cannot verify folder structure in Navatar Docuemnts");
						}

					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot verify folder structure in Navatar Docuemnts", YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot verify folder structure in Navatar Docuemnts");
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot verify folder structure in Navatar Docuemnts", YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot verify folder structure in Navatar Docuemnts");
				}
				refresh(driver);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 20), "due diligence tab", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due diligence tab", YesNo.No);
					if(click(driver, pipe.getDocumentButton(Smoke_Task2Name, 30), Smoke_Task2Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task2Name+" document button", YesNo.No);
						switchToFrame(driver,30,pipe.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 30));
						if(click(driver, pipe.getTagOrUploadDocumentsButton(30), "tag or document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on tag or document button", YesNo.No);
							ThreadSleep(5000);
							String parentID=switchOnWindow(driver);
							if(parentID!=null) {
								for(String s : folderstructre) {
									if(box.verifyFolderStructure(driver, s)) {
										log(LogStatus.PASS, "Folder Structure is verified on Tag or upload page", YesNo.No);
									}else {
										log(LogStatus.PASS, "Folder Structure is not verified on Tag or upload page", YesNo.Yes);
										sa.assertTrue(false, "Folder Structure is not verified on Tag or upload page");
									}

								}
								if(clickUsingJavaScript(driver,pipe.getTagDocumentsCancelBtn(30),"cancel button")) {
									log(LogStatus.PASS, "clicked on cancel button ", YesNo.No);

								}else {
									log(LogStatus.FAIL, "Not able to click on cancel so cannot close tag document window ", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on cancel so cannot close tag document window ");
									driver.close();
								}
								driver.switchTo().window(parentID);
							}else {
								log(LogStatus.FAIL, "No new window is found so cannot verify folder structure in Navatar Docuemnts",YesNo.Yes);
								sa.assertTrue(false, "No new window is found so cannot verify folder structure in Navatar Docuemnts");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on due diligence tab so cannot verify folder structure in Navatar Docuemnts", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on due diligence tab so cannot verify folder structure in Navatar Docuemnts");
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on document button so cannot verify folder structure in Navatar Docuemnts", YesNo.Yes);
						sa.assertTrue(false, "Not able to click on document button so cannot verify folder structure in Navatar Docuemnts");
					}
				} else {
					log(LogStatus.FAIL, "Not able to click on due diligence tab so cannot verify folder structure in Navatar Docuemnts", YesNo.Yes);
					sa.assertTrue(false, "Not able to click on due diligence tab so cannot verify folder structure in Navatar Docuemnts");
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot create folder structure in box ", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot create folder structure in box ");
				exit("Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot create folder structure in box ");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot create folder structure in box ", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot create folder structure in box ");
			exit("Not able to click on created pipeLine tab so cannot create folder structure in box ");
			
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Pending, Smoke_Task1Name, Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task1Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							switchToFrame(driver, 60, tr.getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 60));
							if(click(driver, pipe.getTagOrUploadDocumentsButton(30), "tag or document button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on tag or document button", YesNo.No);
								ThreadSleep(5000);
								String parentID=switchOnWindow(driver);
								if(parentID!=null) {
									for(String s : folderstructre) {
										if(box.verifyFolderStructure(driver, s)) {
											log(LogStatus.PASS, "Folder Structure is verified on Tag or upload page", YesNo.No);
										}else {
											log(LogStatus.PASS, "Folder Structure is not verified on Tag or upload page", YesNo.Yes);
											sa.assertTrue(false, "Folder Structure is not verified on Tag or upload page");
										}

									}
									if(clickUsingJavaScript(driver,pipe.getTagDocumentsCancelBtn(30),"cancel button")) {
										log(LogStatus.PASS, "clicked on cancel button ", YesNo.No);

									}else {
										log(LogStatus.FAIL, "Not able to click on cancel so cannot close tag document window ", YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel so cannot close tag document window ");
										driver.close();
									}
									driver.switchTo().window(parentID);
								}else {
									log(LogStatus.FAIL, "Not able to login in box so cannot veify tag documents popUp",YesNo.Yes);
									sa.assertTrue(false, "Not able to login in box so cannot veify tag documents popUp");
								}
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check popup functionality", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check popup functionality");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task1Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc006_uploadFileInCreatedFolderInDifferentPages(String environment, String mode) {
		String[] folderstructre=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.FolderStructure).split("<break>");
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		String docpath="UploadFiles\\tc006\\Deal_Evaluation";
		String taskRaydocpath="UploadFiles\\tc006\\taskRay";
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					ThreadSleep(2000);
					if(click(driver, pipe.getDocumentButton(Smoke_Task1Name, 30), Smoke_Task1Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task1Name+" document button", YesNo.No);

						if(pipe.tagOrUploadUpdateDocument(environment, mode,PageName.PipelinesPage,Smoke_Task1Name,docpath, null, Smoke_PL1Name,UploadFileActions.Upload,YesNo.Yes)) {
							log(LogStatus.PASS, "File Successfully upload in "+Smoke_Task1Name+" on "+RelatedTab.Deal_Evaluation.toString(), YesNo.No);
						}else {
							log(LogStatus.PASS, "File is not uploaded in "+Smoke_Task1Name+" on "+RelatedTab.Deal_Evaluation.toString(), YesNo.No);
							sa.assertTrue(false, "File is not uploaded in "+Smoke_Task1Name+" on "+RelatedTab.Deal_Evaluation.toString());
						}
						if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot upload document in "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot upload document in "+Smoke_Task1Name);
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot upload document in "+Smoke_Task1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot upload document in "+Smoke_Task1Name);
				}
				String[] taskNAme= {Smoke_Task2Name,Smoke_Task6Name};
				String [] folderPath= {folderstructre[2],folderstructre[0]};
				String [] dragFolderPath= {"UploadFiles\\tc006\\Due_Deligence\\Account","UploadFiles\\tc006\\Due_Deligence\\Company"};
				
				for (int i = 0; i <2; i++) {
					refresh(driver);
					if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 20), "due diligence tab", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on due diligence tab", YesNo.No);
						if(click(driver, pipe.getDocumentButton(taskNAme[i], 30), taskNAme[i]+" document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on "+taskNAme[i]+" document button", YesNo.No);
							if(pipe.tagOrUploadUpdateDocument(environment, mode,PageName.PipelinesPage, taskNAme[i], dragFolderPath[i], folderPath[i],folderPath[i],UploadFileActions.Upload,YesNo.Yes)) {
								log(LogStatus.PASS, "File Successfully upload in "+taskNAme[i]+" on "+RelatedTab.Due_Diligence.toString(), YesNo.No);
							}else {
								log(LogStatus.PASS, "File is not uploaded in "+taskNAme[i]+" on "+RelatedTab.Due_Diligence.toString(), YesNo.No);
								sa.assertTrue(false, "File is not uploaded in "+taskNAme[i]+" on "+RelatedTab.Due_Diligence.toString());
							}
							if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on document button so cannot upload document in "+taskNAme[i], YesNo.Yes);
							sa.assertTrue(false, "Not able to click on document button so cannot upload document in "+taskNAme[i]);
						}
					} else {
						log(LogStatus.FAIL, "Not able to click on due diligence tab so cannot upload document in "+taskNAme[i], YesNo.Yes);
						sa.assertTrue(false, "Not able to click on due diligence tab so cannot upload document in "+taskNAme[i]);
					}
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot create folder structure in box ", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot create folder structure in box ");
				exit("Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot create folder structure in box ");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot create folder structure in box ", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot create folder structure in box ");
			exit("Not able to click on created pipeLine tab so cannot create folder structure in box ");
			
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Pending, Smoke_Task1Name, Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task1Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							if(pipe.tagOrUploadUpdateDocument(environment, mode,PageName.TaskRayPage,Smoke_Task1Name,taskRaydocpath, folderstructre[1], folderstructre[1],UploadFileActions.Upload,YesNo.Yes)) {
								log(LogStatus.PASS, "File Successfully upload in "+Smoke_Task1Name+" on "+PageName.TaskRayPage.toString(), YesNo.No);
							}else {
								log(LogStatus.PASS, "File is not uploaded in "+Smoke_Task1Name+" on "+PageName.TaskRayPage.toString(), YesNo.No);
								sa.assertTrue(false, "File is not uploaded in "+Smoke_Task1Name+" on "+PageName.TaskRayPage.toString());
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check popup functionality", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check popup functionality");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task1Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc007_updateFileInCreatedFolderInDifferentPages(String environment, String mode) {
		String[] folderstructre=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.FolderStructure).split("<break>");
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		String docpath="UploadFiles\\tc006\\Deal_Evaluation";
		String taskRaydocpath="UploadFiles\\tc006\\taskRay";
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					ThreadSleep(2000);
					if(click(driver, pipe.getDocumentButton(Smoke_Task1Name, 30), Smoke_Task1Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task1Name+" document button", YesNo.No);

						if(pipe.tagOrUploadUpdateDocument(environment, mode,PageName.PipelinesPage,Smoke_Task1Name,docpath, null, Smoke_PL1Name,UploadFileActions.IgnoreAll,YesNo.Yes)) {
							log(LogStatus.PASS, "File Successfully is not  updated in "+Smoke_Task1Name+" on "+RelatedTab.Deal_Evaluation.toString(), YesNo.No);
						}else {
							log(LogStatus.PASS, "File is updated in "+Smoke_Task1Name+" on "+RelatedTab.Deal_Evaluation.toString(), YesNo.No);
							sa.assertTrue(false, "File is updated in "+Smoke_Task1Name+" on "+RelatedTab.Deal_Evaluation.toString());
						}
						if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot updated document in "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot updated document in "+Smoke_Task1Name);
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task1Name);
				}
				String[] taskNAme= {Smoke_Task2Name,Smoke_Task6Name};
				String [] folderPath= {folderstructre[2],folderstructre[0]};
				String [] dragFolderPath= {"UploadFiles\\tc006\\Due_Deligence\\Account","UploadFiles\\tc006\\Due_Deligence\\Company"};
				
				for (int i = 0; i <2; i++) {
					refresh(driver);
					if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 20), "due diligence tab", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on due diligence tab", YesNo.No);
						if(click(driver, pipe.getDocumentButton(taskNAme[i], 30), taskNAme[i]+" document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on "+taskNAme[i]+" document button", YesNo.No);
							if(pipe.tagOrUploadUpdateDocument(environment, mode,PageName.PipelinesPage, taskNAme[i], dragFolderPath[i], folderPath[i],folderPath[i],UploadFileActions.Update,YesNo.Yes)) {
								log(LogStatus.PASS, "File Successfully updated in "+taskNAme[i]+" on "+RelatedTab.Due_Diligence.toString(), YesNo.No);
							}else {
								log(LogStatus.PASS, "File is not updateded in "+taskNAme[i]+" on "+RelatedTab.Due_Diligence.toString(), YesNo.No);
								sa.assertTrue(false, "File is not updateded in "+taskNAme[i]+" on "+RelatedTab.Due_Diligence.toString());
							}
							if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on document button so cannot updated document in "+taskNAme[i], YesNo.Yes);
							sa.assertTrue(false, "Not able to click on document button so cannot updated document in "+taskNAme[i]);
						}
					} else {
						log(LogStatus.FAIL, "Not able to click on due diligence tab so cannot updated document in "+taskNAme[i], YesNo.Yes);
						sa.assertTrue(false, "Not able to click on due diligence tab so cannot updated document in "+taskNAme[i]);
					}
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot create folder structure in box ", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot create folder structure in box ");
				exit("Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot create folder structure in box ");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot create folder structure in box ", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot create folder structure in box ");
			exit("Not able to click on created pipeLine tab so cannot create folder structure in box ");
			
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Pending, Smoke_Task1Name, Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task1Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							if(pipe.tagOrUploadUpdateDocument(environment, mode,PageName.TaskRayPage,Smoke_Task1Name,taskRaydocpath, folderstructre[1], folderstructre[1],UploadFileActions.Update,YesNo.Yes)) {
								log(LogStatus.PASS, "File Successfully updated in "+Smoke_Task1Name+" on "+PageName.TaskRayPage.toString(), YesNo.No);
							}else {
								log(LogStatus.PASS, "File is not updateded in "+Smoke_Task1Name+" on "+PageName.TaskRayPage.toString(), YesNo.No);
								sa.assertTrue(false, "File is not updateded in "+Smoke_Task1Name+" on "+PageName.TaskRayPage.toString());
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check popup functionality", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check popup functionality");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task1Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc008_tagDocumentsOnDealEvaluationTab(String environment, String mode) {
		String[] fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName).split("<break>");
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					ThreadSleep(2000);
					if(click(driver, pipe.getDocumentButton(Smoke_Task1Name, 30), Smoke_Task1Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task1Name+" document button", YesNo.No);
						if(pipe.tagDocuments(environment, mode, PageName.PipelinesPage, null, Smoke_PL1Name,null, SelectOption.selectOrDeSelectAll, null).isEmpty()) {
							log(LogStatus.PASS, "Documents is tagged successfully", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to tag documents from "+RelatedTab.Deal_Evaluation+" on task "+Smoke_Task1Name, YesNo.Yes);
							sa.assertTrue(false, "Not able to tag documents from "+RelatedTab.Deal_Evaluation+" on task "+Smoke_Task1Name);
						}
						if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage, fileNames[0]+"<break>"+fileNames[1]).isEmpty()) {
							log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task1Name, YesNo.No);
						}else {
							log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task1Name, YesNo.Yes);
							sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task1Name);
						}
						if(pipe.verifyTaggedDocumentIsSelectedInTagDocumentPopUp(environment, mode, PageName.PipelinesPage, null,Smoke_PL1Name,fileNames[0]+"<break>"+fileNames[1]).isEmpty()) {
							log(LogStatus.PASS, "tagged document is selected for "+Smoke_Task1Name, YesNo.No);
						}else {
							log(LogStatus.PASS, "tagged document is not selected after tagged document for "+Smoke_Task1Name, YesNo.Yes);
							sa.assertTrue(false, "tagged document is not selected after tagged document for "+Smoke_Task1Name);
						}
						if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}
						if(click(driver,pipe.getHeaderSideButtons(Header.OpenTask, HeaderSideButton.Reload, 10), "reload button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "Clicked on "+Smoke_Task1Name+" reload button", YesNo.No);
							  String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
							   String[][] DataList1= {{Smoke_Task1Name,Smoke_Task1Status,"",crmUser1FirstName+" "+crmUser1LastName,"2","Complete","Documents"}};
								
							   SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Deal_Evaluation,Header.OpenTask, headerNameList1,DataList1);						
							   sa.combineAssertions(TaskOpenData);
						}else {
							log(LogStatus.FAIL, "Not able to click on reload button so cannot verify "+Smoke_Task1Name+" data ", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on reload button so cannot verify "+Smoke_Task1Name+" data ");
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot updated document in "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot updated document in "+Smoke_Task1Name);
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task1Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
				exit("Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			exit("Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Pending, Smoke_Task1Name, Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task1Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							switchToDefaultContent(driver);
							if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage, fileNames[0]+"<break>"+fileNames[1]).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task1Name+" on taskRay page", YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task1Name+" on taskRay page", YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task1Name+" on taskRay page");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task1Name+" on taskRay page", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task1Name+" on taskRay page");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task1Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc009_tagDocumentsOnDealEvaluationTabCompleteTask(String environment, String mode) {
		String folderstructre=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.FolderStructure);
		String fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					ThreadSleep(2000);
					if(click(driver, pipe.getDocumentButton(Smoke_Task5Name, 30), Smoke_Task5Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task5Name+" document button", YesNo.No);
						if(pipe.tagDocuments(environment, mode, PageName.PipelinesPage, folderstructre, folderstructre,fileNames, SelectOption.OneByOneSelect, null).isEmpty()) {
							log(LogStatus.PASS, "Documents is tagged successfully in "+Smoke_Task5Name, YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to tag documents from "+RelatedTab.Deal_Evaluation+" on task "+Smoke_Task5Name, YesNo.Yes);
							sa.assertTrue(false, "Not able to tag documents from "+RelatedTab.Deal_Evaluation+" on task "+Smoke_Task5Name);
						}
						if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage,fileNames).isEmpty()) {
							log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task5Name, YesNo.No);
						}else {
							log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task5Name, YesNo.Yes);
							sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task5Name);
						}
						if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}
						if(click(driver,pipe.getHeaderSideButtons(Header.CompletedTask, HeaderSideButton.Reload, 10), "reload button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "Clicked on "+Smoke_Task5Name+" reload button", YesNo.No);
							
							String[] headerNameList2= {"Name","End Date","Owner","Documents"};
							   
							String[][] DataList2= {{Smoke_Task5Name,"",crmUser1FirstName+" "+crmUser1LastName,"1","Documents"}};
						   
							
						   SoftAssert TaskCompletedData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Deal_Evaluation,Header.CompletedTask, headerNameList2,DataList2);						
						   sa.combineAssertions(TaskCompletedData);
						}else {
							log(LogStatus.FAIL, "Not able to click on reload button so cannot verify "+Smoke_Task5Name+" data ", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on reload button so cannot verify "+Smoke_Task5Name+" data ");
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot updated document in "+Smoke_Task5Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot updated document in "+Smoke_Task5Name);
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task5Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task5Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
				exit("Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			exit("Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Completed, Smoke_Task5Name, Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task5Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							switchToDefaultContent(driver);
							if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage,fileNames).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task5Name+" on taskRay page", YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task5Name+" on taskRay page", YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task5Name+" on taskRay page");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task5Name+" on taskRay page", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task5Name+" on taskRay page");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task5Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task5Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task5Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task5Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc010_tagDocumentsOnDueDiligenceTab(String environment, String mode) {
		String[] fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName).split("<break>");
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					ThreadSleep(2000);
					if(click(driver, pipe.getDocumentButton(Smoke_Task2Name, 30), Smoke_Task2Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task2Name+" document button", YesNo.No);
						if(pipe.tagDocuments(environment, mode, PageName.PipelinesPage, null, Smoke_PL1Name,null, SelectOption.selectOrDeSelectAll, null).isEmpty()) {
							log(LogStatus.PASS, "Documents is tagged successfully", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to tag documents from "+RelatedTab.Due_Diligence+" on task "+Smoke_Task2Name, YesNo.Yes);
							sa.assertTrue(false, "Not able to tag documents from "+RelatedTab.Due_Diligence+" on task "+Smoke_Task2Name);
						}
						if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage, fileNames[0]+"<break>"+fileNames[1]).isEmpty()) {
							log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task2Name, YesNo.No);
						}else {
							log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task2Name, YesNo.Yes);
							sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task2Name);
						}
						if(pipe.verifyTaggedDocumentIsSelectedInTagDocumentPopUp(environment, mode, PageName.PipelinesPage, null,Smoke_PL1Name,fileNames[0]+"<break>"+fileNames[1]).isEmpty()) {
							log(LogStatus.PASS, "tagged document is selected for "+Smoke_Task2Name, YesNo.No);
						}else {
							log(LogStatus.PASS, "tagged document is not selected after tagged document for "+Smoke_Task2Name, YesNo.Yes);
							sa.assertTrue(false, "tagged document is not selected after tagged document for "+Smoke_Task2Name);
						}
						if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}
						if(click(driver,pipe.getHeaderSideButtons(Header.OpenTask, HeaderSideButton.Reload, 10), "reload button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "Clicked on "+Smoke_Task2Name+" reload button", YesNo.No);
							  String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
							   String[][] DataList1= {{Smoke_Task2Name,Smoke_Task2Status,"",crmUser1FirstName+" "+crmUser1LastName,"2","Complete","Documents"}};
								
							   SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Due_Diligence,Header.OpenTask, headerNameList1,DataList1);						
							   sa.combineAssertions(TaskOpenData);
						}else {
							log(LogStatus.FAIL, "Not able to click on reload button so cannot verify "+Smoke_Task2Name+" data ", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on reload button so cannot verify "+Smoke_Task2Name+" data ");
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot updated document in "+Smoke_Task2Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot updated document in "+Smoke_Task2Name);
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task1Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
				exit("Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			exit("Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project2Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project2Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Pending, Smoke_Task2Name, Smoke_Project2Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task2Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							switchToDefaultContent(driver);
							if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage, fileNames[0]+"<break>"+fileNames[1]).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task2Name+" on taskRay page", YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task2Name+" on taskRay page", YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task2Name+" on taskRay page");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task2Name+" on taskRay page", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task2Name+" on taskRay page");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task2Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task2Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project2Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project2Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task2Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task2Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc011_tagDocumentsOnDueDiligenceTabCompleteTask(String environment, String mode) {
		String folderstructre=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.FolderStructure);
		String fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					ThreadSleep(2000);
					if(click(driver, pipe.getDocumentButton(Smoke_Task6Name, 30), Smoke_Task6Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task6Name+" document button", YesNo.No);
						if(pipe.tagDocuments(environment, mode, PageName.PipelinesPage, folderstructre, folderstructre,fileNames, SelectOption.OneByOneSelect, null).isEmpty()) {
							log(LogStatus.PASS, "Documents is tagged successfully in "+Smoke_Task6Name, YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to tag documents from "+RelatedTab.Due_Diligence+" on task "+Smoke_Task6Name, YesNo.Yes);
							sa.assertTrue(false, "Not able to tag documents from "+RelatedTab.Due_Diligence+" on task "+Smoke_Task6Name);
						}
						if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage,fileNames).isEmpty()) {
							log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task6Name, YesNo.No);
						}else {
							log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task6Name, YesNo.Yes);
							sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task6Name);
						}
						if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}
						if(click(driver,pipe.getHeaderSideButtons(Header.CompletedTask, HeaderSideButton.Reload, 10), "reload button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "Clicked on "+Smoke_Task6Name+" reload button", YesNo.No);
							
							String[] headerNameList2= {"Name","End Date","Owner","Documents"};
							   
							String[][] DataList2= {{Smoke_Task6Name,"",crmUser1FirstName+" "+crmUser1LastName,"1","Documents"}};
						   
							
						   SoftAssert TaskCompletedData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Due_Diligence,Header.CompletedTask, headerNameList2,DataList2);						
						   sa.combineAssertions(TaskCompletedData);
						}else {
							log(LogStatus.FAIL, "Not able to click on reload button so cannot verify "+Smoke_Task6Name+" data ", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on reload button so cannot verify "+Smoke_Task6Name+" data ");
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot updated document in "+Smoke_Task6Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot updated document in "+Smoke_Task6Name);
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task6Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task6Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
				exit("Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			exit("Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project2Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project2Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Completed, Smoke_Task6Name, Smoke_Project2Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task6Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							switchToDefaultContent(driver);
							if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage,fileNames).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task6Name+" on taskRay page", YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task6Name+" on taskRay page", YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task6Name+" on taskRay page");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task6Name+" on taskRay page", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task6Name+" on taskRay page");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task6Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task6Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project2Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project2Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task6Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task6Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc012_tagDocumentsFromTaskRayPageAndCheckOnDealEvaluation(String environment, String mode) {
		String folderstructre=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.FolderStructure);
		String fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Under_Review, Smoke_Task3Name, Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task3Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							
							if(pipe.tagDocuments(environment, mode, PageName.TaskRayPage, folderstructre, folderstructre,null, SelectOption.selectOrDeSelectAll, null).isEmpty()) {
								log(LogStatus.PASS, "Documents is tagged successfully in "+Smoke_Task3Name, YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to tag documents from "+PageName.TaskRayPage+" on task "+Smoke_Task3Name, YesNo.Yes);
								sa.assertTrue(false, "Not able to tag documents from "+PageName.TaskRayPage+" on task "+Smoke_Task3Name);
							}
							switchToDefaultContent(driver);
							if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage,fileNames).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task3Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task3Name, YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task3Name);
							}
							switchToDefaultContent(driver);
							if(pipe.verifyTaggedDocumentIsSelectedInTagDocumentPopUp(environment, mode, PageName.TaskRayPage, folderstructre, folderstructre,fileNames).isEmpty()) {
								log(LogStatus.PASS, "tagged document is selected for "+Smoke_Task3Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "tagged document is not selected after tagged document for "+Smoke_Task3Name, YesNo.Yes);
								sa.assertTrue(false, "tagged document is not selected after tagged document for "+Smoke_Task3Name);
							}
							if(task.clickOnCreateTaskCrossIcon()) {
								ThreadSleep(5000);
								if(task.verifyDocumentCountOnCreatedTask(TaskType.Under_Review,Smoke_Task3Name,"2")) {
									log(LogStatus.PASS, "Document count is verified on created task "+Smoke_Task3Name, YesNo.No);
								}else {
									log(LogStatus.PASS, "Document count is not verified on created task "+Smoke_Task3Name, YesNo.No);
									sa.assertTrue(false, "Document count is not verified on created task "+Smoke_Task3Name);
								}
							}else {
								log(LogStatus.FAIL, "Not able close created task popup so cannot check document count", YesNo.Yes);
								sa.assertTrue(false, "Not able close created task popup so cannot check document count");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot tag document for "+Smoke_Task3Name+" on taskRay page", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot tag document for "+Smoke_Task3Name+" on taskRay page");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task3Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task3Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task3Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task3Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot tag document from natavar document for task "+Smoke_Task3Name, YesNo.Yes);
			sa.assertTrue(false,"Not able to click on taskRay Tab so cannot tag document from natavar document for task "+Smoke_Task3Name);
			exit("Not able to click on taskRay Tab so cannot tag document from natavar document for task "+Smoke_Task3Name);
		}
		
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on deal evaluation tab", YesNo.No);
					ThreadSleep(2000);
					String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
					String[][] DataList1= {{Smoke_Task3Name,Smoke_Task3Status,"",crmUser1FirstName+" "+crmUser1LastName,"2","Complete","Documents"}};
					
					SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Deal_Evaluation,Header.OpenTask, headerNameList1,DataList1);						
					sa.combineAssertions(TaskOpenData);
					
					
					if(click(driver, pipe.getDocumentButton(Smoke_Task3Name, 30), Smoke_Task3Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task3Name+" document button", YesNo.No);
						
						if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage,fileNames).isEmpty()) {
							log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task3Name, YesNo.No);
						}else {
							log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task3Name, YesNo.Yes);
							sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task3Name);
						}
						   
						if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}   
						   
				
					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot verify tagged document in "+Smoke_Task3Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot verify tagged document in "+Smoke_Task3Name);
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot check tagged document in "+Smoke_Task3Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot check tagged document in "+Smoke_Task3Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify tagged document on "+Smoke_Task3Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify tagged document on "+Smoke_Task3Name);
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on pipeLine tab so cannot verify tagged document on "+Smoke_Task3Name, YesNo.Yes);
			sa.assertTrue(false,"Not able to click on pipeLine tab so cannot verify tagged document on "+Smoke_Task3Name);
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc013_tagDocumentsFromTaskRayPageAndCheckOnDueDiligence(String environment, String mode) {
		String folderstructre=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.FolderStructure);
		String fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project2Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project2Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Under_Review, Smoke_Task4Name, Smoke_Project2Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task4Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							
							if(pipe.tagDocuments(environment, mode, PageName.TaskRayPage, folderstructre, folderstructre,null, SelectOption.selectOrDeSelectAll, null).isEmpty()) {
								log(LogStatus.PASS, "Documents is tagged successfully in "+Smoke_Task4Name, YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to tag documents from "+PageName.TaskRayPage+" on task "+Smoke_Task4Name, YesNo.Yes);
								sa.assertTrue(false, "Not able to tag documents from "+PageName.TaskRayPage+" on task "+Smoke_Task4Name);
							}
							switchToDefaultContent(driver);
							if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage,fileNames).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task4Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task4Name, YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task4Name);
							}
							switchToDefaultContent(driver);
							if(pipe.verifyTaggedDocumentIsSelectedInTagDocumentPopUp(environment, mode, PageName.TaskRayPage, folderstructre, folderstructre,fileNames).isEmpty()) {
								log(LogStatus.PASS, "tagged document is selected for "+Smoke_Task4Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "tagged document is not selected after tagged document for "+Smoke_Task4Name, YesNo.Yes);
								sa.assertTrue(false, "tagged document is not selected after tagged document for "+Smoke_Task4Name);
							}
							
							if(task.clickOnCreateTaskCrossIcon()) {
								ThreadSleep(5000);
								if(task.verifyDocumentCountOnCreatedTask(TaskType.Under_Review,Smoke_Task4Name,"2")) {
									log(LogStatus.PASS, "Document count is verified on created task "+Smoke_Task4Name, YesNo.No);
								}else {
									log(LogStatus.PASS, "Document count is not verified on created task "+Smoke_Task4Name, YesNo.No);
									sa.assertTrue(false, "Document count is not verified on created task "+Smoke_Task4Name);
								}
							}else {
								log(LogStatus.FAIL, "Not able close created task popup so cannot check document count", YesNo.Yes);
								sa.assertTrue(false, "Not able close created task popup so cannot check document count");
							}
							
							
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot tag document for "+Smoke_Task4Name+" on taskRay page", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot tag document for "+Smoke_Task4Name+" on taskRay page");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task4Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task4Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project2Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project2Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task4Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task4Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot tag document from natavar document for task "+Smoke_Task4Name, YesNo.Yes);
			sa.assertTrue(false,"Not able to click on taskRay Tab so cannot tag document from natavar document for task "+Smoke_Task4Name);
			exit("Not able to click on taskRay Tab so cannot tag document from natavar document for task "+Smoke_Task4Name);
		}
		
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due diligence tab", YesNo.No);
					ThreadSleep(2000);
					String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
					String[][] DataList1= {{Smoke_Task4Name,Smoke_Task4Status,"",crmUser1FirstName+" "+crmUser1LastName,"2","Complete","Documents"}};
					
					SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Due_Diligence,Header.OpenTask, headerNameList1,DataList1);						
					sa.combineAssertions(TaskOpenData);
					
					
					if(click(driver, pipe.getDocumentButton(Smoke_Task4Name, 30), Smoke_Task4Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task4Name+" document button", YesNo.No);
						
						if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage,fileNames).isEmpty()) {
							log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task4Name, YesNo.No);
						}else {
							log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task4Name, YesNo.Yes);
							sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task4Name);
						}
						   
						if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}   
						   
				
					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot verify tagged document in "+Smoke_Task4Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot verify tagged document in "+Smoke_Task4Name);
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot check tagged document in "+Smoke_Task4Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot check tagged document in "+Smoke_Task4Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify tagged document on "+Smoke_Task4Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify tagged document on "+Smoke_Task4Name);
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on pipeLine tab so cannot verify tagged document on "+Smoke_Task4Name, YesNo.Yes);
			sa.assertTrue(false,"Not able to click on pipeLine tab so cannot verify tagged document on "+Smoke_Task4Name);
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc014_checkDocumentLinkAndUnTaggedDocumentOnDealEvaluation(String environment, String mode) {
		String docName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					ThreadSleep(2000);
					if(click(driver, pipe.getDocumentButton(Smoke_Task1Name, 30), Smoke_Task1Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task1Name+" document button", YesNo.No);
						
						if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.PipelinesPage, docName, null,crmUser1EmailID,adminPassword,YesNo.No, YesNo.No)) {
							log(LogStatus.PASS, docName+" : Document is open successfully after login in Box", YesNo.No);
							
							if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.PipelinesPage, docName, null,crmUser1EmailID,adminPassword,YesNo.Yes, YesNo.No)) {
								log(LogStatus.PASS, docName+" : Document is open successfully without asking login page", YesNo.No);
								
							}else {
								log(LogStatus.FAIL, docName+" : Document is not open after click on again document Name ", YesNo.Yes);
								sa.assertTrue(false, docName+" : Document is not open after click on again document Name ");
							}
						}else {
							log(LogStatus.FAIL, docName+" : Document is not open", YesNo.Yes);
							sa.assertTrue(false, docName+" : Document is not open");
						}
						switchToFrame(driver,10,pipe.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 10));
						if(click(driver, pipe.getDocumentRemoveLinkOnNavatarDocumentPopUp(docName, 10), docName+" name remove link", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on document Name "+docName+" remove link", YesNo.No);
							ThreadSleep(2000);
							
							String exptError=PipeLineErrorMessage.removeTaggedDocumentPopUpErrorMsg;
							if(pipe.getNavatarDocumentRemovePopUpErrorMsg(30)!=null) {
								String aa = pipe.getNavatarDocumentRemovePopUpErrorMsg(30).getText().trim();
								if(aa.equalsIgnoreCase(exptError)) {
									log(LogStatus.PASS, "Error Message is verified : "+exptError,YesNo.No);
								}else {
									log(LogStatus.FAIL, "Error message is not matched : "+exptError+" Actual : "+aa, YesNo.Yes);
									sa.assertTrue(false, "Error message is not matched : "+exptError+" Actual : "+aa);
								}
							}else {
								log(LogStatus.FAIL, "Remove PopUp error message is not visible so cannot verify error message", YesNo.Yes);
								sa.assertTrue(false, "Remove PopUp error message is not visible so cannot verify error message");
							}
							
							if(click(driver, pipe.getNavatarDocumentPopUpCancelButton(10), docName+" name cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, docName+" document name  cancel button", YesNo.No);
							}else {
								log(LogStatus.PASS,"Not able to click on document name "+docName+" cancel button", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on document name "+docName+" cancel button");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on remove button in document name "+docName, YesNo.Yes);
							sa.assertTrue(false, "Not able to click on remove button in document name "+docName);
						}
						switchToDefaultContent(driver);
						
						if(pipe.removeTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage, docName).isEmpty()) {
							log(LogStatus.PASS, "document successfully deleted : "+docName, YesNo.No);
							
							if(!pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage,docName).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents is removed on navatar document popUp for "+Smoke_Task1Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task1Name, YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task1Name);
							}
							
						}else {
							log(LogStatus.FAIL, "Not able to delete document : "+docName, YesNo.Yes);
							sa.assertTrue(false, "Not able to delete document : "+docName);
						}
						
						if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}
						if(click(driver,pipe.getHeaderSideButtons(Header.OpenTask, HeaderSideButton.Reload, 10), "reload button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "Clicked on "+Smoke_Task1Name+" reload button", YesNo.No);
							  String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
							   String[][] DataList1= {{Smoke_Task1Name,Smoke_Task1Status,"",crmUser1FirstName+" "+crmUser1LastName,"1","Complete","Documents"}};
								
							   SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Deal_Evaluation,Header.OpenTask, headerNameList1,DataList1);						
							   sa.combineAssertions(TaskOpenData);
						}else {
							log(LogStatus.FAIL, "Not able to click on reload button so cannot verify "+Smoke_Task1Name+" data ", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on reload button so cannot verify "+Smoke_Task1Name+" data ");
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot updated document in "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot updated document in "+Smoke_Task1Name);
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task1Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
				exit("Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			exit("Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Pending, Smoke_Task1Name, Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task1Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							switchToDefaultContent(driver);
							if(!pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage,docName).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents is removed on navatar document popUp for "+Smoke_Task1Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task1Name, YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task1Name);
							}
							if(task.clickOnCreateTaskCrossIcon()) {
								ThreadSleep(5000);
								if(task.verifyDocumentCountOnCreatedTask(TaskType.Pending,Smoke_Task1Name,"1")) {
									log(LogStatus.PASS, "Document count is verified on created task "+Smoke_Task1Name, YesNo.No);
								}else {
									log(LogStatus.PASS, "Document count is not verified on created task "+Smoke_Task1Name, YesNo.No);
									sa.assertTrue(false, "Document count is not verified on created task "+Smoke_Task1Name);
								}
							}else {
								log(LogStatus.FAIL, "Not able close created task popup so cannot check document count", YesNo.Yes);
								sa.assertTrue(false, "Not able close created task popup so cannot check document count");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task1Name+" on taskRay page", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task1Name+" on taskRay page");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task1Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc015_checkDocumentLinkAndUnTaggedDocumentOnDueDiligence(String environment, String mode) {
		String docName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					ThreadSleep(2000);
					if(click(driver, pipe.getDocumentButton(Smoke_Task2Name, 30), Smoke_Task2Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task2Name+" document button", YesNo.No);
						
						if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.PipelinesPage, docName, null,crmUser1EmailID,adminPassword,YesNo.No, YesNo.No)) {
							log(LogStatus.PASS, docName+" : Document is open successfully after login in Box", YesNo.No);
							
							if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.PipelinesPage, docName, null,crmUser1EmailID,adminPassword,YesNo.Yes, YesNo.No)) {
								log(LogStatus.PASS, docName+" : Document is open successfully without asking login page", YesNo.No);
								
							}else {
								log(LogStatus.FAIL, docName+" : Document is not open after click on again document Name ", YesNo.Yes);
								sa.assertTrue(false, docName+" : Document is not open after click on again document Name ");
							}
						}else {
							log(LogStatus.FAIL, docName+" : Document is not open", YesNo.Yes);
							sa.assertTrue(false, docName+" : Document is not open");
						}
						switchToFrame(driver,10,pipe.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 10));
						if(click(driver, pipe.getDocumentRemoveLinkOnNavatarDocumentPopUp(docName, 10), docName+" name remove link", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on document Name "+docName+" remove link", YesNo.No);
							ThreadSleep(2000);
							String exptError=PipeLineErrorMessage.removeTaggedDocumentPopUpErrorMsg;
							if(pipe.getNavatarDocumentRemovePopUpErrorMsg(30)!=null) {
								String aa = pipe.getNavatarDocumentRemovePopUpErrorMsg(30).getText().trim();
								if(aa.equalsIgnoreCase(exptError)) {
									log(LogStatus.PASS, "Error Message is verified : "+exptError,YesNo.No);
								}else {
									log(LogStatus.FAIL, "Error message is not matched : "+exptError+" Actual : "+aa, YesNo.Yes);
									sa.assertTrue(false, "Error message is not matched : "+exptError+" Actual : "+aa);
								}
							}else {
								log(LogStatus.FAIL, "Remove PopUp error message is not visible so cannot verify error message", YesNo.Yes);
								sa.assertTrue(false, "Remove PopUp error message is not visible so cannot verify error message");
							}
							
							if(click(driver, pipe.getNavatarDocumentPopUpCancelButton(10), docName+" name cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, docName+" document name  cancel button", YesNo.No);
							}else {
								log(LogStatus.PASS,"Not able to click on document name "+docName+" cancel button", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on document name "+docName+" cancel button");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on remove button in document name "+docName, YesNo.Yes);
							sa.assertTrue(false, "Not able to click on remove button in document name "+docName);
						}
						switchToDefaultContent(driver);
						
						if(pipe.removeTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage, docName).isEmpty()) {
							log(LogStatus.PASS, "document successfully deleted : "+docName, YesNo.No);
							
							if(!pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage,docName).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents is removed on navatar document popUp for "+Smoke_Task2Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task2Name, YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task2Name);
							}
							
						}else {
							log(LogStatus.FAIL, "Not able to delete document : "+docName, YesNo.Yes);
							sa.assertTrue(false, "Not able to delete document : "+docName);
						}
						
						if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}
						if(click(driver,pipe.getHeaderSideButtons(Header.OpenTask, HeaderSideButton.Reload, 10), "reload button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "Clicked on "+Smoke_Task2Name+" reload button", YesNo.No);
							  String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
							   String[][] DataList1= {{Smoke_Task2Name,Smoke_Task1Status,"",crmUser1FirstName+" "+crmUser1LastName,"1","Complete","Documents"}};
								
							   SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Due_Diligence,Header.OpenTask, headerNameList1,DataList1);						
							   sa.combineAssertions(TaskOpenData);
						}else {
							log(LogStatus.FAIL, "Not able to click on reload button so cannot verify "+Smoke_Task2Name+" data ", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on reload button so cannot verify "+Smoke_Task2Name+" data ");
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot updated document in "+Smoke_Task2Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot updated document in "+Smoke_Task2Name);
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task2Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot updated document in "+Smoke_Task2Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
				exit("Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document and open task section ");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			exit("Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project2Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project2Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Pending, Smoke_Task2Name, Smoke_Project2Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task2Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							switchToDefaultContent(driver);
							
							if(task.clickOnCreateTaskCrossIcon()) {
								ThreadSleep(5000);
								if(task.verifyDocumentCountOnCreatedTask(TaskType.Pending,Smoke_Task2Name,"1")) {
									log(LogStatus.PASS, "Document count is verified on created task "+Smoke_Task2Name, YesNo.No);
								}else {
									log(LogStatus.PASS, "Document count is not verified on created task "+Smoke_Task2Name, YesNo.No);
									sa.assertTrue(false, "Document count is not verified on created task "+Smoke_Task2Name);
								}
							}else {
								log(LogStatus.FAIL, "Not able close created task popup so cannot check document count", YesNo.Yes);
								sa.assertTrue(false, "Not able close created task popup so cannot check document count");
							}
							
							
							if(!pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage,docName).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents is removed on navatar document popUp for "+Smoke_Task2Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task2Name, YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task2Name);
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task2Name+" on taskRay page", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task2Name+" on taskRay page");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task2Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task2Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project2Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project2Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task2Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task2Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc016_checkDocumentLinkAndUnTaggedDocumentOnTaskRay(String environment, String mode) {
		String docName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			int i=0;
			String [][] projectNameAndTaskName= {{Smoke_Project1Name,Smoke_Task3Name},{Smoke_Project2Name,Smoke_Task4Name}};
			for(String[] projectNameTaskName : projectNameAndTaskName) {
				switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
				if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
					log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
					if(task.clickOnCreatedProjectInTaskRay(projectNameTaskName[0])) {
						log(LogStatus.PASS, "clicked on created project name "+projectNameTaskName[0], YesNo.No);
						if(task.docubleClickOnCreatedTask(TaskType.Pending, projectNameTaskName[1], projectNameTaskName[0])) {
							log(LogStatus.PASS, "clicked on created task "+projectNameTaskName[1], YesNo.No);
							ThreadSleep(3000);
							if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
								log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
								ThreadSleep(3000);
								switchToDefaultContent(driver);
								
								if(i==0) {
									if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.TaskRayPage, docName, null,crmUser1EmailID,adminPassword,YesNo.No, YesNo.No)) {
										log(LogStatus.PASS, docName+" : Document is open successfully after login in Box", YesNo.No);
										
										if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.TaskRayPage, docName, null,crmUser1EmailID,adminPassword,YesNo.Yes, YesNo.No)) {
											log(LogStatus.PASS, docName+" : Document is open successfully without asking login page", YesNo.No);
											
										}else {
											log(LogStatus.FAIL, docName+" : Document is not open after click on again document Name ", YesNo.Yes);
											sa.assertTrue(false, docName+" : Document is not open after click on again document Name ");
										}
									}else {
										log(LogStatus.FAIL, docName+" : Document is not open", YesNo.Yes);
										sa.assertTrue(false, docName+" : Document is not open");
									}
									switchToFrame(driver, 10, task.getFrame(PageName.TaskRayPage, 60));
									switchToFrame(driver, 10, task.getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 10));
									if(click(driver, pipe.getDocumentRemoveLinkOnNavatarDocumentPopUp(docName, 10), docName+" name remove link", action.SCROLLANDBOOLEAN)) {
										log(LogStatus.PASS, "clicked on document Name "+docName+" remove link", YesNo.No);
										ThreadSleep(2000);
										
										String exptError=PipeLineErrorMessage.removeTaggedDocumentPopUpErrorMsg;
										if(pipe.getNavatarDocumentRemovePopUpErrorMsg(30)!=null) {
											String aa = pipe.getNavatarDocumentRemovePopUpErrorMsg(30).getText().trim();
											if(aa.equalsIgnoreCase(exptError)) {
												log(LogStatus.PASS, "Error Message is verified : "+exptError,YesNo.No);
											}else {
												log(LogStatus.FAIL, "Error message is not matched : "+exptError+" Actual : "+aa, YesNo.Yes);
												sa.assertTrue(false, "Error message is not matched : "+exptError+" Actual : "+aa);
											}
										}else {
											log(LogStatus.FAIL, "Remove PopUp error message is not visible so cannot verify error message", YesNo.Yes);
											sa.assertTrue(false, "Remove PopUp error message is not visible so cannot verify error message");
										}
										
										if(click(driver, pipe.getNavatarDocumentPopUpCancelButton(10), docName+" name cancel button", action.SCROLLANDBOOLEAN)) {
											log(LogStatus.PASS, docName+" document name  cancel button", YesNo.No);
										}else {
											log(LogStatus.PASS,"Not able to click on document name "+docName+" cancel button", YesNo.Yes);
											sa.assertTrue(false, "Not able to click on document name "+docName+" cancel button");
										}
									}else {
										log(LogStatus.FAIL, "Not able to click on remove button in document name "+docName, YesNo.Yes);
										sa.assertTrue(false, "Not able to click on remove button in document name "+docName);
									}
									switchToDefaultContent(driver);
								}
								
								if(pipe.removeTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage, docName).isEmpty()) {
									log(LogStatus.PASS, "document successfully deleted : "+docName, YesNo.No);
									
									if(!pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage,docName).isEmpty()) {
										log(LogStatus.PASS, "Tagged Documents is removed on navatar document popUp for "+projectNameTaskName[1], YesNo.No);
									}else {
										log(LogStatus.PASS, "Tagged Documents is not removed on navatar document popUp for "+projectNameTaskName[1], YesNo.Yes);
										sa.assertTrue(false, "Tagged Documents is not removed on navatar document popUp for "+projectNameTaskName[1]);
									}
									
								}else {
									log(LogStatus.FAIL, "Not able to delete document : "+docName, YesNo.Yes);
									sa.assertTrue(false, "Not able to delete document : "+docName);
								}
								
								if(task.clickOnCreateTaskCrossIcon()) {
									ThreadSleep(5000);
									if(task.verifyDocumentCountOnCreatedTask(TaskType.Under_Review,projectNameTaskName[1],"1")) {
										log(LogStatus.PASS, "Document count is verified on created task "+projectNameTaskName[1], YesNo.No);
									}else {
										log(LogStatus.PASS, "Document count is not verified on created task "+projectNameTaskName[1], YesNo.No);
										sa.assertTrue(false, "Document count is not verified on created task "+projectNameTaskName[1]);
									}
								}else {
									log(LogStatus.FAIL, "Not able close created task popup so cannot check document count", YesNo.Yes);
									sa.assertTrue(false, "Not able close created task popup so cannot check document count");
								}
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check tagged document for "+projectNameTaskName[1]+" on taskRay page", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check tagged document for "+projectNameTaskName[1]+" on taskRay page");
							}
							
						}else {
							log(LogStatus.FAIL, "Not able to click on created task "+projectNameTaskName[1], YesNo.Yes);
							sa.assertTrue(false, "Not able to click on created task "+projectNameTaskName[1]);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on created project "+projectNameTaskName[1], YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created project "+projectNameTaskName[1]);
					}
					
				}else {
					log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+projectNameTaskName[1], YesNo.Yes);
					sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+projectNameTaskName[1]);
				}
				i++;
				switchToDefaultContent(driver);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					ThreadSleep(2000);
					  String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
					   String[][] DataList1= {{Smoke_Task3Name,Smoke_Task3Status,"",crmUser1FirstName+" "+crmUser1LastName,"1","Complete","Documents"}};
						
					   SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Deal_Evaluation,Header.OpenTask, headerNameList1,DataList1);						
					   sa.combineAssertions(TaskOpenData);
					   
					   if(click(driver, pipe.getDocumentButton(Smoke_Task3Name, 30), Smoke_Task3Name+" document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on "+Smoke_Task3Name+" document button", YesNo.No);
					   
							if(!pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage,docName).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents is removed on navatar document popUp for "+Smoke_Task3Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task3Name, YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task3Name);
							}
					   
						}else {
							log(LogStatus.FAIL, "Not able to click on documents button so cannot  verify document count on navatar document", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on documents button so cannot verify document count on navatar document");
						}
					   
					   if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}
				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot verify document count open task section", YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot cannot verify document count open task section");
				}
				refresh(driver);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due Diligence tab", YesNo.No);
					ThreadSleep(2000);
					  String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
					   String[][] DataList1= {{Smoke_Task4Name,Smoke_Task4Status,"",crmUser1FirstName+" "+crmUser1LastName,"1","Complete","Documents"}};
						
					   SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Due_Diligence,Header.OpenTask, headerNameList1,DataList1);						
					   sa.combineAssertions(TaskOpenData);
					   
					   if(click(driver, pipe.getDocumentButton(Smoke_Task4Name, 30), Smoke_Task4Name+" document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on "+Smoke_Task4Name+" document button", YesNo.No);
					   
							if(!pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.PipelinesPage,docName).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents is removed on navatar document popUp for "+Smoke_Task4Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task4Name, YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents is not removed on navatar document popUp for "+Smoke_Task4Name);
							}
					   
						}else {
							log(LogStatus.FAIL, "Not able to click on documents button so cannot  verify document count on navatar document", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on documents button so cannot verify document count on navatar document");
						}
					   
					   if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
						}
				}else {
					log(LogStatus.FAIL, "Not able to click on deal Diligence tab so cannot verify document count open task section", YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal Diligence tab so cannot cannot verify document count open task section");
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify document count open task section ", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify document count open task section");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot verify document count open task section ", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on created pipeLine tab so cannot verify document count open task section ");
			
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc017_deleteDocumentAndFolderFromBox(String environment, String mode) {
		String docName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
		String[] FolderName = ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.FolderStructure).split("<break>");
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Documents, 30), "documents tab", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on documents tab", YesNo.No);
					if(pipe.deleteFileOrFolderInBoxFromDocumentTab(null, FolderName[1], BoxActions.Trash).isEmpty()) {
						log(LogStatus.PASS, FolderName[1]+" folder is deleted successfully",YesNo.No);
					}else {
						log(LogStatus.FAIL, FolderName[1]+" folder is not deleted", YesNo.Yes);
						sa.assertTrue(false, FolderName[1]+" folder is not deleted");
					}
					ThreadSleep(5000);
					if(pipe.deleteFileOrFolderInBoxFromDocumentTab(FolderName[0],docName, BoxActions.Trash).isEmpty()) {
						log(LogStatus.PASS, docName+" document is deleted from folder "+FolderName[0],YesNo.No);
					}else {
						log(LogStatus.FAIL, docName+" document is not deleted from folder "+FolderName[0], YesNo.Yes);
						sa.assertTrue(false,docName+" document is not deleted from folder "+FolderName[0]);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on documents tab so cannot delete file in box from document tab", YesNo.Yes);
					sa.assertTrue(false, "Not able to click on documents tab so cannot delete file in box from document tab");
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot delete file in box from document tab", YesNo.Yes);
				sa.assertTrue(false,"Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot delete file in box from document tab");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on  pipeLine tab so cannot delete file in box from document tab", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on  pipeLine tab so cannot delete file in box from document tab");

		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}

	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc018_checkDocumentLinkAndCountAfterDelete(String environment, String mode) {
		String[] fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName).split("<break>");
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				
				String[][] checkdata = {{Smoke_Task3Name,Smoke_Task3Status,Smoke_Task5Name,fileNames[1],fileNames[2]},
						{Smoke_Task4Name,Smoke_Task4Status,Smoke_Task6Name,fileNames[1],fileNames[3]}};
				RelatedTab[] tabs = {RelatedTab.Deal_Evaluation,RelatedTab.Due_Diligence};
				YesNo[] boxLogin = {YesNo.No,YesNo.Yes};
				int i=0;
				
				for(String[] str : checkdata) {
					if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, tabs[i], 30), "deal evaluation tab", 
							action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
						ThreadSleep(2000);
						
						String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
						String[][] DataList1= {{str[0],str[1],"",crmUser1FirstName+" "+crmUser1LastName,"1","Complete","Documents"}};
						
						SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,tabs[i],Header.OpenTask, headerNameList1,DataList1);						
						sa.combineAssertions(TaskOpenData);
						
						
						
						String[] headerNameList2= {"Name","End Date","Owner","Documents"};
						
						String[][] DataList2= {{str[2],"",crmUser1FirstName+" "+crmUser1LastName,"1","Documents"}};
						
						
						SoftAssert TaskCompletedData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,tabs[i],Header.CompletedTask, headerNameList2,DataList2);						
						sa.combineAssertions(TaskCompletedData);
						
						if(click(driver, pipe.getDocumentButton(str[0], 30), str[0]+" document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on "+str[0]+" document button", YesNo.No);
							if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.PipelinesPage, str[3], null,crmUser1EmailID,adminPassword,boxLogin[i], YesNo.Yes)) {
								log(LogStatus.PASS, str[3]+" : Document error message is verifed ", YesNo.No);
							}else {
								log(LogStatus.FAIL, str[3]+" : Document is not verified", YesNo.Yes);
								sa.assertTrue(false, str[3]+" : Document is not verified");
							}
							if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on documents button so cannot updated document in "+Smoke_Task3Name, YesNo.Yes);
							sa.assertTrue(false, "Not able to click on documents button so cannot updated document in "+Smoke_Task3Name);
						}
						refresh(driver);
						ThreadSleep(2000);
						click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, tabs[i], 30), tabs[i]+" tab", action.SCROLLANDBOOLEAN);
						if(click(driver, pipe.getDocumentButton(str[2], 30), str[2]+" document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on "+str[2]+" document button", YesNo.No);
							if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.PipelinesPage,str[4], null,crmUser1EmailID,adminPassword,YesNo.Yes, YesNo.Yes)) {
								log(LogStatus.PASS, str[4]+" : Document error message is verifed ", YesNo.No);
							}else {
								log(LogStatus.FAIL, str[4]+" : Document is not verified", YesNo.Yes);
								sa.assertTrue(false, str[4]+" : Document is not verified");
							}
							if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on documents button so cannot updated document in "+str[2], YesNo.Yes);
							sa.assertTrue(false, "Not able to click on documents button so cannot updated document in "+str[2]);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on "+tabs[i]+" tab so cannot check document count and error message on task : "+Smoke_Task3Name+" and "+Smoke_Task5Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on "+tabs[i]+" tab so cannot check document count and error message on task : "+Smoke_Task3Name+" and "+Smoke_Task5Name);
					}
					if(i==0) {
						refresh(driver);
					}
					i++;
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot check document count and error message on task : "+Smoke_Task3Name+" and "+Smoke_Task5Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot check document count and error message on task : "+Smoke_Task3Name+" and "+Smoke_Task5Name);
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section ", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document and open task section");
			
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
					if(task.docubleClickOnCreatedTask(TaskType.Under_Review, Smoke_Task3Name, Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task3Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
							log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
							ThreadSleep(3000);
							switchToDefaultContent(driver);
							
							if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.TaskRayPage,fileNames[1], null,crmUser1EmailID,adminPassword,YesNo.Yes, YesNo.Yes)) {
								log(LogStatus.PASS, fileNames[1]+" : Document error message is verifed ", YesNo.No);
							}else {
								log(LogStatus.FAIL, fileNames[1]+" : Document is not verified", YesNo.Yes);
								sa.assertTrue(false,fileNames[1]+" : Document is not verified");
							}
							
							
						}else {
							log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task3Name+" on taskRay page", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check tagged document for "+Smoke_Task3Name+" on taskRay page");
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task3Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task3Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task3Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task3Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check deleted document from box in created task "+Smoke_Task3Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check deleted document from box in created task "+Smoke_Task3Name);
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc019_moveFileAndFolderInBoxAndCheckImpact(String environment, String mode) {
	String[] folderstructre=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.FolderStructure).split("<break>");
	String fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
	LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
	TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
	PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
	TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
	lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
	// scn.nextLine();
	if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
		log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
		if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
			log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
			if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Documents, 30), "documents tab", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on documents tab", YesNo.No);
				
				if(pipe.MoveOrCopyFileOrFolderInBoxFromDocumentTab(null, folderstructre[1], fileNames, BoxActions.MoveOrCopy).isEmpty()) {
					log(LogStatus.PASS, "Move document "+fileNames+" in "+folderstructre[1]+" successfully",YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to move document "+fileNames+" in "+folderstructre[1], YesNo.Yes);
					sa.assertTrue(false, "Not able to move document "+fileNames+" in "+folderstructre[1]);
				}
				
				if(pipe.MoveOrCopyFileOrFolderInBoxFromDocumentTab(null, folderstructre[1], folderstructre[0], BoxActions.MoveOrCopy).isEmpty()) {
					log(LogStatus.PASS, "Move Folder "+folderstructre[0]+" in "+folderstructre[1]+" successfully",YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to move folder "+folderstructre[0]+" in "+folderstructre[1], YesNo.Yes);
					sa.assertTrue(false, "Not able to move folder "+folderstructre[0]+" in "+folderstructre[1]);
				}
				
				
			}else {
				log(LogStatus.FAIL, "Not able to click on documents tab so cannot move or copy document", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on documents tab so cannot move or copy document");
			}
			refresh(driver);
			if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "deal evaluation tab", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
				ThreadSleep(2000);
				if(click(driver, pipe.getDocumentButton(Smoke_Task1Name, 30), Smoke_Task1Name+" document button", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on "+Smoke_Task1Name+" document button", YesNo.No);
					
					if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.PipelinesPage, fileNames, null,crmUser1EmailID,adminPassword,YesNo.No, YesNo.No)) {
						log(LogStatus.PASS, fileNames+" : Document is open successfully after login in Box", YesNo.No);
						
					}else {
						log(LogStatus.FAIL, fileNames+" : Document is not open", YesNo.Yes);
						sa.assertTrue(false, fileNames+" : Document is not open");
					}
					
					if(pipe.verifyFolderStructureInTagDocumentPopUp(environment, mode, PageName.PipelinesPage, folderstructre[1]+"/"+folderstructre[0],null).isEmpty()) {
						log(LogStatus.PASS, "Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.No);
					}else {
						log(LogStatus.FAIL, "Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Folder strcuture is verified on navtar document for task "+Smoke_Task1Name);
					}
					
					if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
					}else {
						log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
						sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
					}
			
				}else {
					log(LogStatus.FAIL, "Not able to click on documents button so cannot check document in task "+Smoke_Task1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on documents button so cannot check document in task "+Smoke_Task1Name);
				}

			}else {
				log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot check document in task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot check document in task "+Smoke_Task1Name);
			}
			refresh(driver);
			if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 30), "deal evaluation tab", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
				ThreadSleep(2000);
				if(click(driver, pipe.getDocumentButton(Smoke_Task2Name, 30), Smoke_Task2Name+" document button", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on "+Smoke_Task2Name+" document button", YesNo.No);
					
					if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.PipelinesPage, fileNames, null,crmUser1EmailID,adminPassword,YesNo.Yes, YesNo.No)) {
						log(LogStatus.PASS, fileNames+" : Document is open successfully after login in Box", YesNo.No);
						
					}else {
						log(LogStatus.FAIL, fileNames+" : Document is not open", YesNo.Yes);
						sa.assertTrue(false, fileNames+" : Document is not open");
					}
					
					if(pipe.verifyFolderStructureInTagDocumentPopUp(environment, mode, PageName.PipelinesPage, folderstructre[1]+"/"+folderstructre[0],null).isEmpty()) {
						log(LogStatus.PASS, "Folder strcuture is verified on navtar document for task "+Smoke_Task2Name, YesNo.No);
					}else {
						log(LogStatus.FAIL, "Folder strcuture is verified on navtar document for task "+Smoke_Task2Name, YesNo.Yes);
						sa.assertTrue(false, "Folder strcuture is verified on navtar document for task "+Smoke_Task2Name);
					}
					
					if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
					}else {
						log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
						sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
					}
			
				}else {
					log(LogStatus.FAIL, "Not able to click on documents button so cannot check document in task "+Smoke_Task2Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on documents button so cannot check document in task "+Smoke_Task2Name);
				}

			}else {
				log(LogStatus.FAIL, "Not able to click on Due Diligence tab so cannot check document in task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on Due Diligence tab so cannot check document in task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot move file and folder in box from document tab", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot move file and folder in box from document tab");
			exit("Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot move file and folder in box from document tab");
		}
	} else {
		log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot move file and folder in box from document tab", YesNo.Yes);
		sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot move file and folder in box from document tab");
		exit("Not able to click on created pipeLine tab so cannot move file and folder in box from document tab");
		
	}
	if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
		log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
		ThreadSleep(10000);
		switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
		if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
			log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
			if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
				log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
				if(task.docubleClickOnCreatedTask(TaskType.Pending, Smoke_Task1Name, Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created task "+Smoke_Task1Name, YesNo.No);
					ThreadSleep(3000);
					if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
						log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
						ThreadSleep(3000);
						switchToDefaultContent(driver);
						
						if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.TaskRayPage, fileNames, null,crmUser1EmailID,adminPassword,YesNo.Yes, YesNo.No)) {
							log(LogStatus.PASS, fileNames+" : Document is open successfully after login in Box", YesNo.No);
							
						}else {
							log(LogStatus.FAIL, fileNames+" : Document is not open", YesNo.Yes);
							sa.assertTrue(false, fileNames+" : Document is not open");
						}
						
						if(pipe.verifyFolderStructureInTagDocumentPopUp(environment, mode, PageName.TaskRayPage, folderstructre[1]+"/"+folderstructre[0],null).isEmpty()) {
							log(LogStatus.PASS, "Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.No);
						}else {
							log(LogStatus.FAIL, "Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.Yes);
							sa.assertTrue(false, "Folder strcuture is verified on navtar document for task "+Smoke_Task1Name);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check move file and folders in tag document popup", YesNo.Yes);
						sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check move file and folders in tag document popup");
					}
					
				}else {
					log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created task "+Smoke_Task1Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
			}
			
		}else {
			log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
			sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name);
		}
	}else {
		log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check move file and folders in tag document popup", YesNo.Yes);
		sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check move file and folders in tag document popup");
	}
	lp.CRMlogout(environment, mode);
	sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc020_renameFileAndFolderInBoxAndCheckImpact(String environment, String mode) {
	String folderstructre=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.FolderStructure);
	String fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
	String[] UpdatefileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.updateFileName).split("<break>");
	LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
	TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
	PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
	TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
	lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
	// scn.nextLine();
	if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
		log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
		if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
			log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
			if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Documents, 30), "documents tab", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on documents tab", YesNo.No);
				
				if(pipe.renameFileOrFolderInBoxFromDocumentTab(null,folderstructre,UpdatefileNames[0], BoxActions.Rename).isEmpty()) {
					log(LogStatus.PASS, "rename Folder "+folderstructre+" to "+UpdatefileNames[0]+" successfully",YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to rename folder "+folderstructre+" to "+UpdatefileNames[0], YesNo.Yes);
					sa.assertTrue(false, "Not able to rename folder "+folderstructre+" to "+UpdatefileNames[0]);
				}
				
				
				if(pipe.renameFileOrFolderInBoxFromDocumentTab(UpdatefileNames[0],fileNames,UpdatefileNames[1].split("\\.")[0],BoxActions.Rename).isEmpty()) {
					log(LogStatus.PASS, "Rename document "+fileNames+" to "+UpdatefileNames[1]+" successfully",YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to rename document "+fileNames+" to "+UpdatefileNames[1], YesNo.Yes);
					sa.assertTrue(false, "Not able to rename document "+fileNames+" to "+UpdatefileNames[1]);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on documents tab so cannot rename document", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on documents tab so cannot rename document");
			}
			refresh(driver);
			if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "deal evaluation tab", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
				ThreadSleep(2000);
				if(click(driver, pipe.getDocumentButton(Smoke_Task1Name, 30), Smoke_Task1Name+" document button", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on "+Smoke_Task1Name+" document button", YesNo.No);
					
					if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.PipelinesPage, fileNames,UpdatefileNames[1],crmUser1EmailID,adminPassword,YesNo.No, YesNo.No)) {
						log(LogStatus.PASS, fileNames+" : Document is open successfully after login in Box", YesNo.No);
						
					}else {
						log(LogStatus.FAIL, fileNames+" : Document is not open", YesNo.Yes);
						sa.assertTrue(false, fileNames+" : Document is not open");
					}
					
					if(pipe.verifyFolderStructureInTagDocumentPopUp(environment, mode, PageName.PipelinesPage, UpdatefileNames[0],null).isEmpty()) {
						log(LogStatus.PASS, UpdatefileNames[0]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.No);
					}else {
						log(LogStatus.FAIL, UpdatefileNames[0]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, UpdatefileNames[0]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name);
					}
					
					if(pipe.verifyFolderStructureInTagDocumentPopUp(environment, mode, PageName.PipelinesPage, UpdatefileNames[0],UpdatefileNames[1]).isEmpty()) {
						log(LogStatus.PASS, UpdatefileNames[1]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.No);
					}else {
						log(LogStatus.FAIL, UpdatefileNames[1]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, UpdatefileNames[1]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name);
					}
					
					
					
					if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
					}else {
						log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
						sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
					}
			
				}else {
					log(LogStatus.FAIL, "Not able to click on documents button so cannot check document in task "+Smoke_Task1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on documents button so cannot check document in task "+Smoke_Task1Name);
				}

			}else {
				log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot check document in task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot check document in task "+Smoke_Task1Name);
			}
			refresh(driver);
			if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Due_Diligence, 30), "deal evaluation tab", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
				ThreadSleep(2000);
				if(click(driver, pipe.getDocumentButton(Smoke_Task2Name, 30), Smoke_Task2Name+" document button", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on "+Smoke_Task2Name+" document button", YesNo.No);
					
					if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.PipelinesPage, fileNames, UpdatefileNames[1],crmUser1EmailID,adminPassword,YesNo.Yes, YesNo.No)) {
						log(LogStatus.PASS, fileNames+" : Document is open successfully after login in Box", YesNo.No);
						
					}else {
						log(LogStatus.FAIL, fileNames+" : Document is not open", YesNo.Yes);
						sa.assertTrue(false, fileNames+" : Document is not open");
					}
					
					if(pipe.verifyFolderStructureInTagDocumentPopUp(environment, mode, PageName.PipelinesPage, UpdatefileNames[0],null).isEmpty()) {
						log(LogStatus.PASS, UpdatefileNames[0]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.No);
					}else {
						log(LogStatus.FAIL, UpdatefileNames[0]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, UpdatefileNames[0]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name);
					}
					
					if(pipe.verifyFolderStructureInTagDocumentPopUp(environment, mode, PageName.PipelinesPage, UpdatefileNames[0],UpdatefileNames[1]).isEmpty()) {
						log(LogStatus.PASS, UpdatefileNames[1]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.No);
					}else {
						log(LogStatus.FAIL, UpdatefileNames[1]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, UpdatefileNames[1]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name);
					}
					
					if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
					}else {
						log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
						sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
					}
			
				}else {
					log(LogStatus.FAIL, "Not able to click on documents button so cannot check document in task "+Smoke_Task2Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on documents button so cannot check document in task "+Smoke_Task2Name);
				}

			}else {
				log(LogStatus.FAIL, "Not able to click on Due Diligence tab so cannot check document in task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on Due Diligence tab so cannot check document in task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot move file and folder in box from document tab", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot move file and folder in box from document tab");
			exit("Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot move file and folder in box from document tab");
		}
	} else {
		log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot move file and folder in box from document tab", YesNo.Yes);
		sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot move file and folder in box from document tab");
		exit("Not able to click on created pipeLine tab so cannot move file and folder in box from document tab");
		
	}
	if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
		log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
		ThreadSleep(10000);
		switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
		if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
			log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
			if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
				log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
				if(task.docubleClickOnCreatedTask(TaskType.Pending, Smoke_Task1Name, Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created task "+Smoke_Task1Name, YesNo.No);
					ThreadSleep(3000);
					if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
						log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
						ThreadSleep(3000);
						switchToDefaultContent(driver);
						
						if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.TaskRayPage, fileNames, UpdatefileNames[1],crmUser1EmailID,adminPassword,YesNo.Yes, YesNo.No)) {
							log(LogStatus.PASS, fileNames+" : Document is open successfully after login in Box", YesNo.No);
							
						}else {
							log(LogStatus.FAIL, fileNames+" : Document is not open", YesNo.Yes);
							sa.assertTrue(false, fileNames+" : Document is not open");
						}
						
						if(pipe.verifyFolderStructureInTagDocumentPopUp(environment, mode, PageName.TaskRayPage, UpdatefileNames[0],null).isEmpty()) {
							log(LogStatus.PASS, UpdatefileNames[0]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.No);
						}else {
							log(LogStatus.FAIL, UpdatefileNames[0]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.Yes);
							sa.assertTrue(false, UpdatefileNames[0]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name);
						}
						
						if(pipe.verifyFolderStructureInTagDocumentPopUp(environment, mode, PageName.TaskRayPage, UpdatefileNames[0],UpdatefileNames[1]).isEmpty()) {
							log(LogStatus.PASS, UpdatefileNames[1]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.No);
						}else {
							log(LogStatus.FAIL, UpdatefileNames[1]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name, YesNo.Yes);
							sa.assertTrue(false, UpdatefileNames[1]+" : Folder strcuture is verified on navtar document for task "+Smoke_Task1Name);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check move file and folders in tag document popup", YesNo.Yes);
						sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check move file and folders in tag document popup");
					}
					
				}else {
					log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created task "+Smoke_Task1Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
			}
			
		}else {
			log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
			sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name);
		}
	}else {
		log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check move file and folders in tag document popup", YesNo.Yes);
		sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check move file and folders in tag document popup");
	}
	lp.CRMlogout(environment, mode);
	sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc021_checkErrorMsgUnderTaskDetailsPopUp(String environment, String mode) {
	LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
	TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
	TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
	lp.CRMLogin(superAdminUserName, adminPassword, appName);
	// scn.nextLine();
	if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
		log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
		ThreadSleep(10000);
		switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
		task.clickOnBoardFilters(BoardFilters.My_Projects);
		if(task.clickOnBoardFilters(BoardFilters.Single_Project_Search)) {
			log(LogStatus.PASS, "CLicked on single project search side menu", YesNo.No);
			if(task.searchSingleProject(Smoke_Project1Name)) {
				log(LogStatus.PASS, "clicked on search a single project and search project "+Smoke_Project1Name, YesNo.No);
					ThreadSleep(1000);
					if(task.docubleClickOnCreatedTask(TaskType.Pending, Smoke_Task1Name, Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created task "+Smoke_Task1Name, YesNo.No);
						ThreadSleep(3000);
						if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Files, 60)) {
							log(LogStatus.PASS, "clicked on Files tab", YesNo.No);
							ThreadSleep(3000);
							String docPath=System.getProperty("user.dir")+"\\UploadFiles\\tc021\\tc021.pdf";
							if(click(driver, task.getUploadFilesButtonInCreatedTaskFilesTab(30), "upload file xpath", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "Passed file in upload files "+docPath, YesNo.No);
								ThreadSleep(1000);
								if(uploadFile(docPath)) {
									ThreadSleep(5000);
									if(task.getErrorMsgOnFileTab(30)!=null) {
										String ss = task.getErrorMsgOnFileTab(30).getText().trim();
										if(ss.equalsIgnoreCase(TaskRayErrorMessage.uploadFileErrorMsg)) {
											log(LogStatus.PASS, "Error Message is verified : "+TaskRayErrorMessage.uploadFileErrorMsg, YesNo.No);
										}else {
											log(LogStatus.FAIL,  "Error Message is not verified : "+TaskRayErrorMessage.uploadFileErrorMsg, YesNo.Yes);
											sa.assertTrue(false, "Error Message is not verified : "+TaskRayErrorMessage.uploadFileErrorMsg);
										}
									}else {
										log(LogStatus.FAIL, "Error Message is not found : "+TaskRayErrorMessage.uploadFileErrorMsg,YesNo.Yes);
										sa.assertTrue(false, "Error Message is not found : "+TaskRayErrorMessage.uploadFileErrorMsg);
									}
								}else {
									log(LogStatus.FAIL, "Not able to upload files "+docPath+" so cannot check error message ", YesNo.Yes);
									sa.assertTrue(false, "Not able to upload files "+docPath+" so cannot check error message ");
								}
							}else {
								log(LogStatus.FAIL, "Not able to pass file path in upload files "+docPath+" so cannot check error message under task details ", YesNo.Yes);
								sa.assertTrue(false, "Not able to pass file path in upload files "+docPath+" so cannot check error message under task details ");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on Files tab so cannot check error message under task details ", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on Files tab so cannot check error message under task details ");
						}

					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created task "+Smoke_Task1Name);
					}
			}else {
				log(LogStatus.FAIL, "Not able to click on search a project side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on search a project side tab so cannot click on created task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on single project search side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
			sa.assertTrue(false,  "Not able to click on single project search side tab so cannot click on created task "+Smoke_Task1Name);
		}
	}else {
		log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check error message under task details ", YesNo.Yes);
		sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check error message under task details");
	}
	switchToDefaultContent(driver);
	lp.CRMlogout(environment, mode);
	sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc022_createExternalUserAndResetPassword(String environment, String mode) {
		SetupPageBusinessLayer setup = new SetupPageBusinessLayer(driver);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		HomePageBusineesLayer home = new HomePageBusineesLayer(driver);
		BasePageBusinessLayer bp = new BasePageBusinessLayer(driver);
		String parentWindow = null;
		String[] splitedUserLastName = removeNumbersFromString(crmUser1LastName);
		String UserLastName = splitedUserLastName[0] + lp.generateRandomNumber();
		lp.CRMLogin(superAdminUserName, adminPassword, appName);
		// scn.nextLine();
		String emailId = lp.generateRandomEmailId(gmailUserName);
		ExcelUtils.writeData(testCasesFilePath, UserLastName, "Users", excelLabel.Variable_Name, "User2",excelLabel.User_Last_Name);
		if (home.clickOnSetUpLink(environment, mode)) {
			if (mode.equalsIgnoreCase(Mode.Lightning.toString())) {
				parentWindow = switchOnWindow(driver);
				if (parentWindow != null) {
					if (setup.createPEUser(environment, mode, externalUserFirstName, UserLastName, emailId, externalUserLience,externalUserProfile,externalUserRole)) {
						log(LogStatus.PASS, "External User is created Successfully: " + externalUserFirstName + " " + UserLastName, YesNo.No);
						ExcelUtils.writeData(testCasesFilePath, emailId, "Users", excelLabel.Variable_Name, "User2",excelLabel.User_Email);
						ExcelUtils.writeData(testCasesFilePath, UserLastName, "Users", excelLabel.Variable_Name, "User2",excelLabel.User_Last_Name);

					}else {
						log(LogStatus.FAIL, "Not able to create External User ", YesNo.No);
						sa.assertTrue(false, "Not able to create External User ");
					}
				}else {
					log(LogStatus.SKIP,"No new window is open after click on setup link in lighting mode so cannot create External User",YesNo.Yes);
					sa.assertTrue(false,"No new window is open after click on setup link in lighting mode so cannot create External User");

				}
			}
			driver.close();
			driver.switchTo().window(parentWindow);
		}else {
			log(LogStatus.FAIL, "Not able to click on setup Link so cannot create External User", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on setup Link so cannot create External User");
		}
		lp.CRMlogout(environment, mode);
		closeBrowser();
		config(browserToLaunch);
		lp = new LoginPageBusinessLayer(driver);
		bp = new BasePageBusinessLayer(driver);
		try {
			passwordResetLink = new EmailLib().getResetPasswordLink("passwordreset",gmailUserName,gmailPassword);
		} catch (InterruptedException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		appLog.info("ResetLinkIs: " + passwordResetLink);
		driver.get(passwordResetLink);
		if (lp.setNewPassword()) {
			log(LogStatus.PASS, "Password is set successfully for External User1: " + crmUser1FirstName + " " + UserLastName, YesNo.No);
			ThreadSleep(5000);
			if(click(driver, lp.getAppLuncherXpath(60),"app luncher link", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on app luncher ", YesNo.No);
				ThreadSleep(3000);
				if(click(driver, lp.getViewAllLink(60),"view link", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on view link", YesNo.No);
				
					String [] ss = {"Box Settings","Home","My Tasks","TaskRay Tasks"};
					for (String string : ss) {
						if(bp.getallItemsInAppLuncherPopUp(string, 20)!=null) {
							log(LogStatus.PASS, string+" items is displaying in app luncher ", YesNo.No);
						}else {
							log(LogStatus.FAIL,string+" items is not displaying in app luncher ",YesNo.Yes);
							sa.assertTrue(false, string+" items is not displaying in app luncher ");
						}
					}
					if(click(driver, bp.getAppLuncherPopUpcrossIcon(10), "cross icon", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on cross icon", YesNo.No);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on view all  link so cannot check apps",YesNo.Yes);
					sa.assertTrue(false, "Not able to click on  view all link so cannot check apps");
				}
			}else {
				log(LogStatus.FAIL, "Not able to app luncher link so cannot check apps",YesNo.Yes);
				sa.assertTrue(false, "Not able to app luncher link so cannot check apps");
			}
			lp.CRMlogout(environment, mode);
		} else {
			log(LogStatus.ERROR, "Password is not set for External User1: " + crmUser1FirstName + " " + UserLastName,YesNo.Yes);
			sa.assertTrue(false, "Password is not set for External User1: " + crmUser1FirstName + " " + UserLastName);
		}
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc023_changeOwnerOfCreatedTask(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
					switchToDefaultContent(driver);
					if(task.changeOwnerOfCreatedTask(Smoke_Project1Name, TaskType.Pending,Smoke_Task1Name,externalUserFirstName+" "+externalUserLastName)) {
						log(LogStatus.PASS, "owner chnage "+externalUserFirstName+" "+externalUserLastName+" of task "+Smoke_Task1Name, YesNo.No);
					}else {
						log(LogStatus.FAIL,  "Not able to chnage owner "+externalUserFirstName+" "+externalUserLastName+" of task "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to chnage owner "+externalUserFirstName+" "+externalUserLastName+" of task "+Smoke_Task1Name);
					}
					if(task.changeOwnerOfCreatedTask(Smoke_Project1Name, TaskType.Pending,Smoke_Task3Name,externalUserFirstName+" "+externalUserLastName)) {
						log(LogStatus.PASS, "owner chnage "+externalUserFirstName+" "+externalUserLastName+" of task "+Smoke_Task3Name, YesNo.No);
					}else {
						log(LogStatus.FAIL,  "Not able to chnage owner "+externalUserFirstName+" "+externalUserLastName+" of task "+Smoke_Task3Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to chnage owner "+externalUserFirstName+" "+externalUserLastName+" of task "+Smoke_Task3Name);
					}
					
					if(task.changeOwnerOfCreatedTask(Smoke_Project1Name, TaskType.Pending,Smoke_Task5Name,externalUserFirstName+" "+externalUserLastName)) {
						log(LogStatus.PASS, "owner chnage "+externalUserFirstName+" "+externalUserLastName+" of task "+Smoke_Task5Name, YesNo.No);
					}else {
						log(LogStatus.FAIL,  "Not able to chnage owner "+externalUserFirstName+" "+externalUserLastName+" of task "+Smoke_Task5Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to chnage owner "+externalUserFirstName+" "+externalUserLastName+" of task "+Smoke_Task5Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task5Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task5Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc024_checkOpenCompleteTaskAndDocumentFunctionality(String environment, String mode) {
		String[] fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName).split("<break>");
		String updatedfileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.updateFileName);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		ExternalSideBusinessLayer ex = new ExternalSideBusinessLayer(driver);
		lp.CRMLogin(externalUserEmailID, adminPassword, "Deal Management");
		List<String> endDate = new ArrayList<>();
		ThreadSleep(5000);
		if(ex.clickOnOpenOrCompleteHeaderLink(Header.OpenTask,60)) {
			log(LogStatus.PASS, "clicked on open task header ", YesNo.No);
			ThreadSleep(5000);
			if(ex.verifyOpenOrCompleteTask(Smoke_Task1Name+","+Smoke_Task2Name+","+Smoke_Task3Name+","+Smoke_Task4Name,Smoke_PL1Name+","+Smoke_PL1Name+","+Smoke_PL1Name+","+Smoke_PL1Name,endDate).isEmpty()) {
				log(LogStatus.PASS,"open task is verified ",YesNo.No);
			}else {
				log(LogStatus.FAIL, "open task is not verified ", YesNo.Yes);
				sa.assertTrue(false, "open task is not verified ");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on open task header so cannot check created open task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on open task header so cannot check created open task");
		}
		if(ex.clickOnTab(environment, mode, TabName.MyTasks)) {
			log(LogStatus.PASS, "clicked on my task tab", YesNo.No);
			ThreadSleep(5000);
			if(ex.clickOnOpenOrCompleteHeaderLink(Header.CompletedTask,60)) {
				log(LogStatus.PASS, "clicked on compeleted task header ", YesNo.No);
				ThreadSleep(5000);
				if(ex.verifyOpenOrCompleteTask(Smoke_Task5Name+","+Smoke_Task6Name,Smoke_PL1Name+","+Smoke_PL1Name,endDate).isEmpty()) {
					log(LogStatus.PASS,"Completed task is verified ",YesNo.No);
				}else {
					log(LogStatus.FAIL, "Completed task is not verified ", YesNo.Yes);
					sa.assertTrue(false, "Completed task is not verified ");
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on Completed task header so cannot check created Completed task", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on Completed task header so cannot check created Completed task");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on my task tab so cannot verify complete tasks", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on my task tab so cannot verify complete tasks");
		}
		if(ex.clickOnTab(environment, mode, TabName.MyTasks)) {
			log(LogStatus.PASS, "clicked on my task tab", YesNo.No);
			ThreadSleep(5000);
			if(ex.clickOnOpenOrCompleteHeaderLink(Header.OpenTask,60)) {
				log(LogStatus.PASS, "clicked on open task header ", YesNo.No);
				ThreadSleep(5000);
				if(ex.verifyDocumentFunctionality(environment, mode, Smoke_Task1Name,fileNames[0],updatedfileNames,crmUser1EmailID,adminPassword,YesNo.No,YesNo.No)) {
					log(LogStatus.PASS,"document is verified : "+fileNames[0], YesNo.No);
				}else {
					log(LogStatus.FAIL,"Not able to check document : "+fileNames[0],YesNo.Yes);
					sa.assertTrue(false, "Not able to check document : "+fileNames[0]);
				}
				
				if(ex.verifyDocumentFunctionality(environment, mode, Smoke_Task3Name,fileNames[1],null,crmUser1EmailID,adminPassword,YesNo.Yes,YesNo.Yes)) {
					log(LogStatus.PASS,"document is verified : "+fileNames[1], YesNo.No);
				}else {
					log(LogStatus.FAIL,"Not able to check document : "+fileNames[1],YesNo.Yes);
					sa.assertTrue(false, "Not able to check document : "+fileNames[1]);
				}
				
				
			}else {
				log(LogStatus.FAIL, "Not able to click on open task header so cannot check document on task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on open task header so cannot check doument on task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on my task tab so cannot verify document  on task "+Smoke_Task1Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on my task tab so cannot verify document on task "+Smoke_Task1Name);
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc025_changeuserPipeLineTaskProjectNameAndCheckImpact(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		SetupPageBusinessLayer setup = new SetupPageBusinessLayer(driver);
		String todayDate = getDateAccToTimeZone("America/New_York", "M/d/yyyy");
		String FiveDayeAfterDate=previousOrForwardDate(5, "M/d/yyyy");
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(pipe.updatePipeLineName(environment, mode,Smoke_PL1Name,Smoke_PL1Name+"Update")) {
					log(LogStatus.PASS, "pipeLine Name is updated : "+Smoke_PL1Name+"Update", YesNo.No);
					ExcelUtils.writeData(testCasesFilePath, Smoke_PL1Name+"Update", "PipeLine", excelLabel.Variable_Name, "SmokePL1",excelLabel.Pipeline_Name);
					
					
				}else {
					log(LogStatus.FAIL, "Not able to update pipeLine Name : "+Smoke_PL1Name+"Update", YesNo.Yes);
					sa.assertTrue(false, "Not able to update pipeLine Name : "+Smoke_PL1Name+"Update");
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+"so cannot update pipeLine Name "+Smoke_PL1Name+"Update", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+"so cannot update pipeLine Name "+Smoke_PL1Name+"Update");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on pipeLine tab so cannot update pipeLine Name "+Smoke_PL1Name+"Update", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on pipeLine tab so cannot update pipeLine Name "+Smoke_PL1Name+"Update");
		}
		if(setup.updatecreatedUserDetails(environment, mode, crmUser1LastName+"Update", crmUser1EmailID)) {
			log(LogStatus.PASS, "crm user last name is updated : "+crmUser1LastName+"Update", YesNo.No);
			
			ExcelUtils.writeData(testCasesFilePath, crmUser1LastName+"Update", "Users", excelLabel.Variable_Name, "User1",excelLabel.User_Last_Name);
			
		}else {
			log(LogStatus.FAIL, "Not able update user last name "+crmUser1LastName+"Update", YesNo.Yes);
			sa.assertTrue(false, "Not able update user last name "+crmUser1LastName+"Update");
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project2Name, YesNo.No);
				
					switchToDefaultContent(driver);
					if(task.updatecreatedTaskDeatils(Smoke_Project1Name, TaskType.Pending,Smoke_Task1Name,Smoke_Task1Name+"Update",externalUserFirstName+" "+externalUserLastName, todayDate,FiveDayeAfterDate, Buttons.Save)) {
						log(LogStatus.PASS, "task details are updated "+Smoke_Task1Name,YesNo.No);
						
						ExcelUtils.writeData(testCasesFilePath, Smoke_Task1Name+"Update", "Task", excelLabel.Variable_Name, "SmokeTask1",excelLabel.Task_Name);
						
						ExcelUtils.writeData(testCasesFilePath, todayDate, "Task", excelLabel.Variable_Name, "SmokeTask1",excelLabel.Start_Date);
						ExcelUtils.writeData(testCasesFilePath, FiveDayeAfterDate, "Task", excelLabel.Variable_Name, "SmokeTask1",excelLabel.End_Date);
						ThreadSleep(5000);
						switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
						switchToFrame(driver, 60, tr.getFrame(PageName.createNewTaskDetailsPopUpFrame, 60));
						if(task.getCreateTaskEndDateInViewMode(20)!=null) {
							String aa = task.getCreateTaskEndDateInViewMode(20).getText().trim();
							ExcelUtils.writeData(testCasesFilePath, aa, "Task", excelLabel.Variable_Name, "SmokeTask1",excelLabel.End_Date);
						}
						switchToDefaultContent(driver);
						tr.clickOnCreateTaskCrossIcon();
					}else {
						log(LogStatus.FAIL,"task details are updated "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "task details are updated "+Smoke_Task1Name);
					}
					
					if(task.changeOwnerOfCreatedTask(Smoke_Project1Name, TaskType.Pending,Smoke_Task3Name,crmUser1FirstName+" "+crmUser1LastName+"Update")) {
						log(LogStatus.PASS, "owner chnage "+crmUser1FirstName+" "+crmUser1LastName+"Update of task "+Smoke_Task3Name, YesNo.No);
					}else {
						log(LogStatus.FAIL,  "Not able to chnage owner "+crmUser1FirstName+" "+crmUser1LastName+"Update of task "+Smoke_Task3Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to chnage owner "+crmUser1FirstName+" "+crmUser1LastName+"Update of task "+Smoke_Task3Name);
					}
					
					if(task.changeOwnerOfCreatedTask(Smoke_Project1Name, TaskType.Pending,Smoke_Task5Name,crmUser1FirstName+" "+crmUser1LastName+"Update")) {
						log(LogStatus.PASS, "owner chnage "+crmUser1FirstName+" "+crmUser1LastName+"Update of task "+Smoke_Task5Name, YesNo.No);
					}else {
						log(LogStatus.FAIL,  "Not able to chnage owner "+crmUser1FirstName+" "+crmUser1LastName+"Update of task "+Smoke_Task5Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to chnage owner "+crmUser1FirstName+" "+crmUser1LastName+"Update of task "+Smoke_Task5Name);
					}
					if(task.updateCreatedProjectName(environment, mode, Smoke_Project1Name, Smoke_Project1Name+"Update")) {
						log(LogStatus.PASS, "project name is updated : "+Smoke_Project1Name+"Update",YesNo.No);
						
						ExcelUtils.writeData(testCasesFilePath, Smoke_Project1Name+"Update", "Project", excelLabel.Variable_Name, "SmokePR1",excelLabel.Project_Name);
						
					}else {
						log(LogStatus.FAIL,"project name is not updated : "+Smoke_Project1Name+"Update", YesNo.Yes);
						sa.assertTrue(false, "project name is not updated : "+Smoke_Project1Name+"Update");
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side Tab so cannot update task details and project name ", YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side Tab so cannot update task details and project name ");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot update task details and project name ", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot update task details and project name ");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc026_verifyChangeDetailsAndStatusOnPipeLinePage(String environment, String mode) {

		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe= new PipelinesPageBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS,"Clicked on PipeLine Tab", YesNo.No);
			appLog.info("Clicked on PipeLine Tab");
			
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "due evaluation tab", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					
					String[] headerNameList1= {"Name","Status","End Date","Owner","Documents","Action"};
					String[][] DataList1= {{Smoke_Task1Name,Smoke_Task1Status,Smoke_Task1End_Date,externalUserFirstName+" "+externalUserLastName,"1","Complete","Documents"},
							{Smoke_Task3Name,Smoke_Task3Status,"",crmUser1FirstName+" "+crmUser1LastName,"1","Complete","Documents"}};

					SoftAssert TaskOpenData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Deal_Evaluation,Header.OpenTask, headerNameList1,DataList1);						
					sa.combineAssertions(TaskOpenData);
					
					
					if(click(driver,pipe.getCompleteLink(Smoke_Task3Name, 30), "complete link", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Clicked on task "+Smoke_Task3Name+" complete link", YesNo.No);
						ThreadSleep(10000);
						String[] headerNameList2= {"Name","End Date","Owner","Documents"};
						
						String[][] DataList2= {{Smoke_Task3Name,"",crmUser1FirstName+" "+crmUser1LastName,"1","Documents"}};
						
						click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "due evaluation tab", action.SCROLLANDBOOLEAN);
						
						SoftAssert TaskCompletedData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Deal_Evaluation,Header.CompletedTask, headerNameList2,DataList2);						
						sa.combineAssertions(TaskCompletedData);
						
					}else {
						log(LogStatus.FAIL, "Not able to click on task "+Smoke_Task3Name+" complete link so cannot verify in completed task ", YesNo.Yes);
						sa.assertTrue(false, "Not able to click on task "+Smoke_Task3Name+" complete link so cannot verify in completed task ");
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on due evaluation tab so cannot verify data on it", YesNo.Yes);
					sa.assertTrue(false, "Not able to click on due evaluation tab so cannot verify data on it");
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify data", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot verify data");
			}
		} else {
			log(LogStatus.FAIL, "Not Able to Click on PipeLine Tab so cannot verify data on "+Smoke_PL1Name, YesNo.Yes);
			sa.assertTrue(false, "Not Able to Click on PipeLine Tab so cannot verify data on "+Smoke_PL1Name);
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
		
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc027_verifyChangeDetailAndStatusOfTaskAtExternalSide(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		ExternalSideBusinessLayer ex = new ExternalSideBusinessLayer(driver);
		List<String> endDate= new ArrayList<String>();
		endDate.add(Smoke_Task1End_Date);
		lp.CRMLogin(externalUserEmailID, adminPassword, "Deal Management");
	
		if(ex.clickOnOpenOrCompleteHeaderLink(Header.OpenTask,60)) {
			log(LogStatus.PASS, "clicked on open task header ", YesNo.No);
			ThreadSleep(5000);
			if(ex.verifyOpenOrCompleteTask(Smoke_Task1Name,Smoke_PL1Name,endDate).isEmpty()) {
				log(LogStatus.PASS,"open task is verified ",YesNo.No);
			}else {
				log(LogStatus.FAIL, "open task is not verified ", YesNo.Yes);
				sa.assertTrue(false, "open task is not verified ");
			}
			
			if(click(driver, ex.getOpenTaskCompleteLink(Smoke_Task1Name, Smoke_PL1Name,30),"task complete link", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS,"clicked on task : "+Smoke_Task1Name+" complete link ",YesNo.No);
			}else {
				log(LogStatus.FAIL, "Not able to click on task : "+Smoke_Task1Name+" complete link ",YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on task : "+Smoke_Task1Name+" complete link ");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on open task header so cannot check created open task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on open task header so cannot check created open task");
		}
		if(ex.clickOnTab(environment, mode, TabName.MyTasks)) {
			log(LogStatus.PASS, "clicked on my task tab", YesNo.No);
			
			if(ex.clickOnOpenOrCompleteHeaderLink(Header.CompletedTask,60)) {
				log(LogStatus.PASS, "clicked on compeleted task header ", YesNo.No);
				ThreadSleep(5000);
				if(ex.verifyOpenOrCompleteTask(Smoke_Task1Name,Smoke_PL1Name,endDate).isEmpty()) {
					log(LogStatus.PASS,"Completed task is verified "+Smoke_Task1Name,YesNo.No);
				}else {
					log(LogStatus.FAIL, "Completed task is not verified "+Smoke_Task1Name, YesNo.Yes);
					sa.assertTrue(false, "Completed task is not verified "+Smoke_Task1Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on Completed task header so cannot check created Completed task", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on Completed task header so cannot check created Completed task");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on my task tab so cannot verify complete tasks", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on my task tab so cannot verify complete tasks");
		}

		lp.CRMlogout(environment, mode);
		sa.assertAll();

	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc028_createCustomFormulaField(String environment, String mode) {
		SetupPageBusinessLayer setup = new SetupPageBusinessLayer(driver);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		HomePageBusineesLayer home = new HomePageBusineesLayer(driver);
		String formula=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.Formula);
		String parentWindow = null;
		lp.CRMLogin(superAdminUserName, adminPassword, appName);
		if (home.clickOnSetUpLink(environment, mode)) {
				parentWindow = switchOnWindow(driver);
				if (parentWindow != null) {
					ThreadSleep(5000);
					if(setup.addCustomFieldforFormula(environment, mode, object.Pipeline,"Fields & Relationships","Formula","Test Tag Documents", "Text", formula)) {
						log(LogStatus.PASS, "Custom Formula Field is created for PipeLine", YesNo.No);
					}else {
						log(LogStatus.FAIL, "Not able to create custom formula field for pipeLine", YesNo.Yes);
						sa.assertTrue(false, "Not able to create custom formula field for pipeLine");
					}
					driver.close();
					driver.switchTo().window(parentWindow);	
				}else {
					log(LogStatus.SKIP,"No new window is open after click on setup link in lighting mode so cannot create custom field for pipeLine",YesNo.Yes);
					sa.assertTrue(false,"No new window is open after click on setup link in lighting mode so cannot create custom field for pipeLine");

				}
			
		}else {
			log(LogStatus.FAIL, "Not able to click on setup Link so cannot create custom field for pipeLine", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on setup Link so cannot create custom field for pipeLine");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc029_tagDocumentsFromFieldLevelTagging(String environment, String mode) {
		String fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				 ThreadSleep(5000);
				 if(click(driver, pipe.getTagDocumentCustomFieldLink(50),"tag document custom link ", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "Clicked on Tag document link ", YesNo.No);
					String parentId = switchOnWindow(driver);
					if(parentId!=null) {
						ThreadSleep(10000);
						
						String firstWindowId = null;
						Set<String> lst = driver.getWindowHandles();
						Iterator<String> I1 = lst.iterator();
						while (I1.hasNext()) {
							String windowID = I1.next();
							if (windowID.equalsIgnoreCase(parentId)) {
								appLog.info("Parent Id is Matched");
							} else {
								firstWindowId = windowID;
								appLog.info("Storged child Window Id");
								break;
							}
						}
						String[] windowsId= {parentId,firstWindowId};
						Smoke_PL1Name = ExcelUtils.readData(testCasesFilePath,"Project",excelLabel.Variable_Name, "SmokePR1", excelLabel.Pipeline_Name);
						if(pipe.tagDocuments(environment, mode, PageName.FormulaFieldTagDocuments, null, Smoke_PL1Name,null, SelectOption.OneByOneSelect, windowsId).isEmpty()) {
							log(LogStatus.PASS, "Documents is tagged successfully : "+fileNames, YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to tag documents from tag document link : "+fileNames, YesNo.Yes);
							sa.assertTrue(false, "Not able to tag documents from tag document link "+fileNames);
						}
						if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.FormulaFieldTagDocuments, fileNames).isEmpty()) {
							log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp page "+fileNames, YesNo.No);
						}else {
							log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp page "+fileNames, YesNo.Yes);
							sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp page "+fileNames);
						}
						driver.close();
						driver.switchTo().window(parentId);
					}else {
						log(LogStatus.FAIL, "Not able to switch on Tag document window so cannot tag document "+fileNames, YesNo.Yes);
						sa.assertTrue(false, "Not able to switch on Tag document window so cannot tag document "+fileNames);
					}
				 }else {
					log(LogStatus.FAIL, "Not able to click on tag document link so cannot tag document "+fileNames, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on tag document link so cannot tag document "+fileNames);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document page", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot tag documents and verify document in navatar document page");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document page", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot tag documents and verify document in navatar document page ");
			
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc030_removeTaggedDocumentsFromFieldLevelTagging(String environment, String mode) {
		String fileNames=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.uploadFileName);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		// scn.nextLine();
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				 ThreadSleep(5000);
				 if(click(driver, pipe.getTagDocumentCustomFieldLink(50),"tag document custom link ", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "Clicked on Tag document link ", YesNo.No);
					String parentId = switchOnWindow(driver);
					if(parentId!=null) {
						ThreadSleep(10000);
						driver.switchTo().frame(0);
						if(click(driver, pipe.getDocumentRemoveLinkOnNavatarDocumentPopUp(fileNames, 10), fileNames+" name remove link", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on document Name "+fileNames+" remove link", YesNo.No);
							ThreadSleep(2000);
							String exptError=PipeLineErrorMessage.removeTaggedDocumentPopUpErrorMsg;
							if(pipe.getNavatarDocumentRemovePopUpErrorMsg(30)!=null) {
								String aa = pipe.getNavatarDocumentRemovePopUpErrorMsg(30).getText().trim();
								if(aa.equalsIgnoreCase(exptError)) {
									log(LogStatus.PASS, "Error Message is verified : "+exptError,YesNo.No);
								}else {
									log(LogStatus.FAIL, "Error message is not matched : "+exptError+" Actual : "+aa, YesNo.Yes);
									sa.assertTrue(false, "Error message is not matched : "+exptError+" Actual : "+aa);
								}
							}else {
								log(LogStatus.FAIL, "Remove PopUp error message is not visible so cannot verify error message", YesNo.Yes);
								sa.assertTrue(false, "Remove PopUp error message is not visible so cannot verify error message");
							}
							if(click(driver, pipe.getNavatarDocumentPopUpCancelButton(10), fileNames+" name cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, fileNames+" document name  cancel button", YesNo.No);
							}else {
								log(LogStatus.PASS,"Not able to click on document name "+fileNames+" cancel button", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on document name "+fileNames+" cancel button");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on remove button in document name "+fileNames, YesNo.Yes);
							sa.assertTrue(false, "Not able to click on remove button in document name "+fileNames);
						}
						switchToDefaultContent(driver);
						if(pipe.removeTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.FormulaFieldTagDocuments, fileNames).isEmpty()) {
							log(LogStatus.PASS, "document successfully deleted : "+fileNames, YesNo.No);
							
							if(!pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.FormulaFieldTagDocuments,fileNames).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents is removed on navatar document page ", YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents is not removed on navatar document page", YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents is not removed on navatar document page");
							}
							
						}else {
							log(LogStatus.FAIL, "Not able to delete document : "+fileNames, YesNo.Yes);
							sa.assertTrue(false, "Not able to delete document : "+fileNames);
						}
						driver.close();
						driver.switchTo().window(parentId);
						
					}else {
						log(LogStatus.FAIL, "Not able to switch on Tag document window so cannot tag document "+fileNames, YesNo.Yes);
						sa.assertTrue(false, "Not able to switch on Tag document window so cannot tag document "+fileNames);
					}
				 }else {
					log(LogStatus.FAIL, "Not able to click on tag document link so cannot tag document "+fileNames, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on tag document link so cannot tag document "+fileNames);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot remove tagged document", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine "+Smoke_PL1Name+" so cannot remove tagged document");
			}
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot remove tagged document", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot remove tagged document");
			
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc031_createPreConditionForIP(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		InstitutionsPageBusinessLayer ip = new InstitutionsPageBusinessLayer(driver);
		FundsPageBusinessLayer fp = new FundsPageBusinessLayer(driver);
		FundraisingsPageBusinessLayer frp = new FundraisingsPageBusinessLayer(driver);
		PartnershipsPageBusinessLayer pp = new PartnershipsPageBusinessLayer(driver);
		CommitmentsPageBusinessLayer cp = new CommitmentsPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		
		if (lp.clickOnTab(environment, mode, TabName.InstituitonsTab)) {
			if (ip.createInstitution(environment, mode, SmokeINS1, SmokeINS1_RecordType, null, null)) {
				log(LogStatus.PASS, "successfully created inst "+SmokeINS1, YesNo.No);
			}
			else {
				log(LogStatus.ERROR,"could not create inst "+SmokeINS1, YesNo.Yes);
				sa.assertTrue(false, "could not create inst "+SmokeINS1);
			}
		}
		else {
			log(LogStatus.ERROR,"inst tab is not clickable", YesNo.Yes);
			sa.assertTrue(false, "inst tab is not clickable");
		}
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.FundsTab)) {
			if (fp.createFund(environment, mode, Smoke_Fund1, Smoke_Fund1Type, Smoke_Fund1InvestmentCategory, null, null,Smoke_Fund1RecordType)) {
				log(LogStatus.PASS, "successfully created fund "+Smoke_Fund1, YesNo.No);
			}
			else {
				log(LogStatus.ERROR,"could not create fund "+Smoke_Fund1, YesNo.Yes);
				sa.assertTrue(false, "could not create fund "+Smoke_Fund1);
			}
		}
		else {
			log(LogStatus.ERROR,"funds tab is not clickable", YesNo.Yes);
			sa.assertTrue(false, "funds tab is not clickable");
		}
		
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.FundraisingsTab)) {
			if (frp.createFundRaising(environment, mode, Smoke_FR1Name, Smoke_Fund1, SmokeINS1,Smoke_FR1RecordType)) {
				log(LogStatus.PASS, "successfully created "+Smoke_FR1Name, YesNo.No);
			}
			else {
				log(LogStatus.ERROR,"could not create "+Smoke_FR1Name, YesNo.Yes);
				sa.assertTrue(false, "could not create "+Smoke_FR1Name);
			}
		}
		else {
			log(LogStatus.FAIL, "FundraisingsTab is not clickable", YesNo.Yes);
			sa.assertTrue(false, "FundraisingsTab is not clickable");
		}
		
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.InstituitonsTab)) {
			if (ip.createLimitedPartner(environment, mode, Smoke_LP1Name, SmokeINS1,Smoke_LP1RecordType)) {
				log(LogStatus.PASS, "successfully created "+Smoke_LP1Name, YesNo.No);
			}
			else {
				log(LogStatus.ERROR,"could not create "+Smoke_LP1Name, YesNo.Yes);
				sa.assertTrue(false, "could not create "+Smoke_LP1Name);
			}
		}
		else {
			log(LogStatus.ERROR,"InstituitonsTab is not clickable", YesNo.Yes);
			sa.assertTrue(false, "InstituitonsTab is not clickable");
		}
		
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.PartnershipsTab)) {
			if (pp.createPartnership(environment, mode, Smoke_PartnerShip1Name, Smoke_Fund1,Smoke_PartnerShip1RecordType)) {
				log(LogStatus.PASS, "successfully created partnerhip "+Smoke_PartnerShip1Name, YesNo.No);
			}
			else {
				log(LogStatus.ERROR,"could not create "+Smoke_PartnerShip1Name, YesNo.Yes);
				sa.assertTrue(false, "could not create partership "+Smoke_PartnerShip1Name);
			}
		}
		else {
			log(LogStatus.ERROR,"PartnershipsTab is not clickable", YesNo.Yes);
			sa.assertTrue(false, "PartnershipsTab is not clickable");
		}
		
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.CommitmentsTab)) {
			if (cp.createCommitment(environment, mode, Smoke_LP1Name, Smoke_PartnerShip1Name, "SmokeCOMM1",Smoke_COMM1RecordType,null)) {
				log(LogStatus.PASS, "successfully created commitment",YesNo.No);
			}
			else {
				log(LogStatus.ERROR,"could not create commitment", YesNo.Yes);
				sa.assertTrue(false, "could not create commitment");
			}
		}
		else {
			log(LogStatus.FAIL, "CommitmentsTab is not clickable", YesNo.No);
			sa.assertTrue(false, "CommitmentsTab is not clickable");
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS, "Clicked on TaskRay Tab ", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
					log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
					if(click(driver, task.getCreatedProjectMoreActions(20), "more actions link", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Clicked on project More Actions : "+Smoke_Project1Name, YesNo.No);
						ThreadSleep(1000);
						if(click(driver, task.getCreatedProjectEditButton(30), "edit project link", action.BOOLEAN)) {
							log(LogStatus.PASS, "Clicked on edit project link", YesNo.No);
							ThreadSleep(5000);
							if(switchToFrame(driver, 30, task.getFrame(PageName.ProjectDetailsPoPUp, 60))) {
								if(click(driver, task.getCreatedProjectPopUpEditBtn(30), "edit button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on edit button", YesNo.No);
									ThreadSleep(3000);
									
									if(sendKeys(driver, task.getPipeLineTextBox(30),"", "pipeline text box ", action.SCROLLANDBOOLEAN)) {
										log(LogStatus.PASS, "pipeline text box is blanked", YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to remove value in pipeline text box", YesNo.Yes);
										sa.assertTrue(false, "Not able to remove value in pipeline text box");
									}
									if(sendKeys(driver, task.getFundTextBox(30),Smoke_Fund1, "fund text box ", action.SCROLLANDBOOLEAN)) {
										log(LogStatus.PASS, "passed value in fund text box: "+Smoke_Fund1, YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to enter value in fund name text box : "+Smoke_Fund1, YesNo.Yes);
										sa.assertTrue(false,"Not able to enter value in fund name text box : "+Smoke_Fund1);
									}
									
									
									
									
									if(click(driver, task.getButton(PageName.NewProjectPopUp, Buttons.SaveClose,30),"button save close button",action.SCROLLANDBOOLEAN)) {
										log(LogStatus.PASS, "Clicked on save close button", YesNo.No);
										
									}else {
										log(LogStatus.FAIL, "Not able to click on save close button so cannot update project : "+Smoke_Project1Name, YesNo.Yes);
										sa.assertTrue(false, "Not able to click on save close button so cannot update project : "+Smoke_Project1Name);
									}
								}else {
									log(LogStatus.FAIL, "Not able to click on edit button of project  : "+Smoke_Project1Name, YesNo.Yes);
									sa.assertTrue(false, "Not able to click on edit button of project  : "+Smoke_Project1Name);
								}
							}else {
								log(LogStatus.FAIL, "Not able to switch project details popUp frame so cannot edit project name "+Smoke_Project1Name, YesNo.Yes);
								sa.assertTrue(false, "Not able to switch project details popUp frame so cannot edit project name "+Smoke_Project1Name);
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on edit project : "+Smoke_Project1Name, YesNo.Yes);
							sa.assertTrue(false, "Not able to click on edit project : "+Smoke_Project1Name);
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on more action of project : "+Smoke_Project1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on more action of project : "+Smoke_Project1Name);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side Tab so cannot update task details and project name ", YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side Tab so cannot update task details and project name ");
			}
		}
		else {
			log(LogStatus.ERROR,"Not able to click on task ray tab so cannot edit project : "+Smoke_Project1Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on task ray tab so cannot edit project : "+Smoke_Project1Name);
		}
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on Fund Tab", YesNo.No);
			if (fp.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "Clicked on created Fund : "+Smoke_Fund1, YesNo.No);
				if(click(driver, fp.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
					if (fp.clickOnDocumentLinkInFrontOfTask(environment, mode, Smoke_Task1Name)) {
						refresh(driver);
						switchToFrame(driver, 20, fp.getFrame(PageName.NavatarInvestorManager, 20));	
						if (click(driver, fp.getTagOrUploadDocumentsButton(40), "tag docs", action.BOOLEAN)) {
							if (isAlertPresent(driver)) {
								String msg=switchToAlertAndGetMessage(driver, 20, action.GETTEXT);
								switchToAlertAndAcceptOrDecline(driver, 20, action.ACCEPT);
								if (msg.contains(FundsPageErrorMessage.yourSettingsWillOnlyAllowTagByPipeline)) {
									appLog.info("successfully verified tag docs error message");
								}
								else {
									appLog.error("could not verify tag docs error message");
									log(LogStatus.ERROR, "could not verify tag docs error message", YesNo.Yes);
									sa.assertTrue(false, "could not verify tag docs error message");
								}
							}
							else {
								appLog.error("no error message found on clicking of tag docs button");
								log(LogStatus.ERROR, "no error message found on clicking of tag docs button", YesNo.Yes);
								sa.assertTrue(false, "no error message found on clicking of tag docs button");
							}
						}
						else {
							appLog.error("tag docs button is not clickable");
							log(LogStatus.ERROR, "tag docs button is not clickable", YesNo.Yes);
							sa.assertTrue(false, "tag docs button is not clickable");
						}
					}
					else {
						appLog.error("could not click on document link in front of task");
						log(LogStatus.ERROR, "could not click on document link in front of task", YesNo.Yes);
						sa.assertTrue(false, "could not click on document link in front of task");
					}
				}
				else {
					appLog.error("tasks detail page tab is not clickable");
					log(LogStatus.ERROR, "tasks detail page tab is not clickable", YesNo.Yes);
					sa.assertTrue(false, "tasks detail page tab is not clickable");
				}
			}
			else {
				appLog.error("not found fund "+Smoke_Fund1);
				log(LogStatus.ERROR, "not found fund "+Smoke_Fund1, YesNo.Yes);
				sa.assertTrue(false, "not found fund "+Smoke_Fund1);
			}
		}
		else {
			appLog.error("funds tab is not clickable");
			log(LogStatus.ERROR, "funds tab is not clickable", YesNo.Yes);
			sa.assertTrue(false, "funds tab is not clickable");
		}
		switchToDefaultContent(driver);
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc032_verifyErrorMessageOnPipeLinePage(String environment, String mode) {
		BasePageBusinessLayer bp = new BasePageBusinessLayer(driver);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		lp.CRMLogin(superAdminUserName, adminPassword, appName);
		driver.get("https://"+NavatarStepPageURL);
		log(LogStatus.PASS, "Passed navatar setup URL", YesNo.No);
		if (bp.clickOnCreatedNavatarStepId_Lighting(environment, mode, TabName.NavatarSetup, 30)) {
			log(LogStatus.PASS, "clicked on navtar setup id", YesNo.No);
			if (click(driver, bp.getEditButton(environment, mode, 60), "edit button", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "Clicked on edit button ", YesNo.No);
				if (click(driver, bp.getdocumentTagDropdown(environment, mode, 50),"tagging dropdown", action.SCROLLANDBOOLEAN)) {
					if (click(driver,FindElement(driver, "//*[@title='IP' or @data-value='IP']", "IP",action.SCROLLANDBOOLEAN,30) ,"IP", action.BOOLEAN)) {
						log(LogStatus.PASS, "IP is selected from drop down list", YesNo.No);
						if (click(driver, bp.getCustomTabSaveBtn(environment, mode, 20), "save", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "Clicked on save button ", YesNo.No);
							ThreadSleep(3000);
							if (bp.getdocumentTagDropdownValueViewMode(environment, mode,"IP",30)!=null) {
								log(LogStatus.INFO, "IP is successfully selected", YesNo.No);
							}
							else {
								log(LogStatus.ERROR, "could not select DR in document tag dropdown", YesNo.Yes);
								sa.assertTrue(false, "could not select DR in document tag dropdown");
							}
						}
						else {
							log(LogStatus.ERROR,"could not click on save button", YesNo.Yes);
							sa.assertTrue(false, "could not click on save button");
						}
					}
					else {
						log(LogStatus.ERROR,"could not find DR in dropdown", YesNo.Yes);
						sa.assertTrue(false, "could not find DR in dropdown");
					}
				}
				else {
					log(LogStatus.ERROR,"document tagging dropdown is not clickable", YesNo.Yes);
					sa.assertTrue(false, "document tagging dropdown is not clickable");
				}
			}
			else {
				log(LogStatus.ERROR,"edit button is not clickable", YesNo.Yes);
				sa.assertTrue(false, "edit button is not clickable");
			}
		}
		else {
			log(LogStatus.ERROR,"could not find already created id of Navatar Setup", YesNo.Yes);
			sa.assertTrue(false, "could not find already created id of Navatar Setup");
		}
		switchToDefaultContent(driver);
		lp.CRMlogout(environment, mode);
		driver.close();
		config(browserToLaunch);
		bp = new BasePageBusinessLayer(driver);
		lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS, "Clicked on TaskRay Tab ", YesNo.No);
			ThreadSleep(10000);
			if(task.updatePipeLineNameOrFundName(Smoke_Project1Name,Smoke_PL1Name,"")) {
				log(LogStatus.PASS, "Pipe Line Name is updated in taskRay project : "+Smoke_Project1Name, YesNo.No);
			}else {
				log(LogStatus.FAIL, "Pipe Line Name is not updated in taskRay project : "+Smoke_Project1Name, YesNo.Yes);
				sa.assertTrue(false, "Pipe Line Name is not updated in taskRay project : "+Smoke_Project1Name);
				
			}
		}
		else {
			log(LogStatus.ERROR,"Not able to click on task ray tab so cannot edit project : "+Smoke_Project1Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on task ray tab so cannot edit project : "+Smoke_Project1Name);
		}
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.Pipelines)) {
			log(LogStatus.PASS, "clicked on pipeLine Tab", YesNo.No);
			if(pipe.clickOnCreatedPipeLine(environment, mode, Smoke_PL1Name)) {
				log(LogStatus.PASS, "clicked on created pipeLine "+Smoke_PL1Name, YesNo.No);
				if(click(driver, pipe.getRelatedTab(environment, mode, PageName.PipelinesPage, RelatedTab.Deal_Evaluation, 30), "deal evaluation tab", 
						action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on due evaluation tab", YesNo.No);
					ThreadSleep(2000);
					if(click(driver, pipe.getDocumentButton(Smoke_Task1Name, 30), Smoke_Task1Name+" document button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on "+Smoke_Task1Name+" document button", YesNo.No);
						String errorMsg=TaskRayErrorMessage.tagDocumentErrorMessage;
						switchToFrame(driver,30,pipe.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 60));
						if(clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(60), "tag document button")) {
							log(LogStatus.PASS, "clicked on tag document button", YesNo.No);
							ThreadSleep(1000);
							 if(isAlertPresent(driver)) {
								 String msg = switchToAlertAndGetMessage(driver, 30, action.GETTEXT);
									switchToAlertAndAcceptOrDecline(driver, 30, action.ACCEPT);
									if(msg.equalsIgnoreCase(errorMsg)) {
										appLog.info("Error Message is verified: "+errorMsg);
									}else {
										appLog.error("Error message is not matched. Expected: "+errorMsg+" Actual Result: "+msg);
										sa.assertTrue(false, "Error message is not matched. Expected: "+errorMsg+" Actual Result: "+msg);
									}
							 }else {
								appLog.error("Alert is not present so cannot check error message : "+errorMsg);
								sa.assertTrue(false, "Alert is not present so cannot check error message : "+errorMsg);
							}
						}else {
							log(LogStatus.FAIL,"Not able to click on tag document button so cannot check error message : "+errorMsg,YesNo.Yes);
							sa.assertTrue(false, "Not able to click on tag document button so cannot check error message : "+errorMsg);
						}
						switchToDefaultContent(driver);
						if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on documents button so cannot upload document in "+Smoke_Task1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on documents button so cannot upload document in "+Smoke_Task1Name);
					}

				}else {
					log(LogStatus.FAIL, "Not able to click on deal evaluation tab so cannot upload document in "+Smoke_Task1Name, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on deal evaluation tab so cannot upload document in "+Smoke_Task1Name);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created pipeLine : "+Smoke_PL1Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created pipeLine : "+Smoke_PL1Name);
			}
			
		} else {
			log(LogStatus.FAIL, "Not able to click on created pipeLine tab so cannot check error message on tag document ", YesNo.Yes);
			sa.assertTrue(false,"Not able to click on created pipeLine tab so cannot create error message on tag document ");
			
		}
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS, "Clicked on TaskRay Tab ", YesNo.No);
			ThreadSleep(10000);
			if(task.updatePipeLineNameOrFundName(Smoke_Project1Name,"",Smoke_Fund1)) {
				log(LogStatus.PASS, "Pipe Line Name is updated in taskRay project : "+Smoke_Fund1, YesNo.No);
			}else {
				log(LogStatus.FAIL, "Pipe Line Name is not updated in taskRay project : "+Smoke_Fund1, YesNo.Yes);
				sa.assertTrue(false, "Pipe Line Name is not updated in taskRay project : "+Smoke_Fund1);
				
			}
		}
		else {
			log(LogStatus.ERROR,"Not able to click on task ray tab so cannot edit project : "+Smoke_Project1Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on task ray tab so cannot edit project : "+Smoke_Project1Name);
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc033_verifyErrorMessageAtTagDocumentBeforeRegistration(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		NIMPageBusinessLayer nim = new NIMPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		if(superAdminRegistered.equalsIgnoreCase("No")) {
			lp.CRMLogin(superAdminUserName, adminPassword, appName);
			if(lp.clickOnTab(environment, mode, TabName.NIMTab)) {
				log(LogStatus.PASS, "clicked on NIM tab", YesNo.No);
				ThreadSleep(10000);
				driver.switchTo().frame(nim.getNIMTabParentFrame_Lightning());
				ThreadSleep(5000);
				if(switchToFrame(driver, 30,lp.getFrame(PageName.NavatarInvestorManager, 30))) {
					if(nim.getStartButton(60)!=null) {
						log(LogStatus.PASS, "NIM Registration popUp is displaying", YesNo.No);
					}else {
						log(LogStatus.FAIL, "NIM Registration popUp is not displaying on NIM page", YesNo.Yes);
						sa.assertTrue(false, "NIM Registration popUp is not displaying on NIM page");
						
					}
				}else {
					log(LogStatus.FAIL, "Not able to switch in nim page frame so cannot check registration popUp", YesNo.Yes);
					sa.assertTrue(false, "Not able to switch in nim page frame so cannot check registration popUp");
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on NIM Tab so cannot check registration popUp", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on NIM Tab so cannot check registration popUp");
			}
			switchToDefaultContent(driver);
			if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
				log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
				if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
					log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
					if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
						if (fund.clickOnDocumentLinkInFrontOfTask(environment, mode, Smoke_Task3Name)) {
							String errorMsg=FundsPageErrorMessage.ErrorMsgBeforeRegistration;
							switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 20));	
							if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
								if (isAlertPresent(driver)) {
									String msg=switchToAlertAndGetMessage(driver, 20, action.GETTEXT);
									switchToAlertAndAcceptOrDecline(driver, 20, action.ACCEPT);
									if (msg.contains(errorMsg)) {
										appLog.info("successfully verified tag docs error message : "+errorMsg);
									}
									else {
										log(LogStatus.ERROR, "could not verify tag docs error message: "+errorMsg, YesNo.Yes);
										sa.assertTrue(false, "could not verify tag docs error message: "+errorMsg);
									}
								}
								else {
									log(LogStatus.ERROR, "Alert is not found so cannot check error message : "+errorMsg, YesNo.Yes);
									sa.assertTrue(false, "Alert is not found so cannot check error message : "+errorMsg);
								}
								switchToDefaultContent(driver);
								if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
								}
							}else {
								log(LogStatus.ERROR, "tag docs button is not clickable", YesNo.Yes);
								sa.assertTrue(false, "tag docs button is not clickable");
							}
						}
						else {
							log(LogStatus.ERROR, "could not click on document link in front of task", YesNo.Yes);
							sa.assertTrue(false, "could not click on document link in front of task");
						}
					}
					else {
						log(LogStatus.ERROR, "tasks detail page tab is not clickable", YesNo.Yes);
						sa.assertTrue(false, "tasks detail page tab is not clickable");
					}
					
				}else {
					log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1, YesNo.Yes);
					sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on fund tab so cannot check error message on task tab", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on fund tab so cannot check error message on task tab");
			}
			lp.CRMlogout(environment, mode);
			driver.close();
			config(browserToLaunch);
			lp = new LoginPageBusinessLayer(driver);
			pipe = new PipelinesPageBusinessLayer(driver);
			nim = new NIMPageBusinessLayer(driver);
			fund = new FundsPageBusinessLayer(driver);
		}
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		String nimerrorMsg=NIMPageErrorMessage.errorMessageBeforeGivingInternalUserAccess;
		if(lp.clickOnTab(environment, mode, TabName.NIMTab)) {
			log(LogStatus.PASS, "clicked on NIM tab", YesNo.No);
			ThreadSleep(10000);
			driver.switchTo().frame(nim.getNIMTabParentFrame_Lightning());
			ThreadSleep(5000);
			if(switchToFrame(driver, 60,lp.getFrame(PageName.NavatarInvestorManager, 30))) {
				if (nim.verifyErrorMessageOnPage(nimerrorMsg,nim.getErrorMessageBeforeGivingInternalUserAccess(60),"Error Message before giving internal user access on NIM page")) {
					log(LogStatus.PASS, "Error Message is verified on NIM tab from user: "+nimerrorMsg, YesNo.No);
				} else {
					sa.assertTrue(false,"Error Message is not verified on NIM tab from user: "+nimerrorMsg);
					sa.assertTrue(false, "Error Message is not verified on NIM tab from user: "+nimerrorMsg);
				}
			}else {
				log(LogStatus.FAIL, "Not able to switch in nim page frame so cannot check error message :"+nimerrorMsg, YesNo.Yes);
				sa.assertTrue(false, "Not able to switch in nim page frame so cannot check error message: "+nimerrorMsg);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on NIM Tab so cannot check error message :"+nimerrorMsg, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on NIM Tab so cannot check error message :"+nimerrorMsg);
		}
		switchToDefaultContent(driver);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
					if (fund.clickOnDocumentLinkInFrontOfTask(environment, mode, Smoke_Task3Name)) {
						String errorMsg=FundsPageErrorMessage.ErrorMsgBeforeRegistration;
						switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 20));	
						if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(50), "tag document button")) {
							
							String parentId=switchOnWindow(driver);
							if(parentId!=null) {
								WebElement ele= fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(60);
								if(ele!=null) {
									String msg=fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(20).getText().trim();
									if (msg.contains(errorMsg)) {
										appLog.info("successfully verified tag docs error message : "+errorMsg);
									}
									else {
										log(LogStatus.ERROR, "could not verify tag docs error message: "+errorMsg, YesNo.Yes);
										sa.assertTrue(false, "could not verify tag docs error message: "+errorMsg);
									}
								}else {
									log(LogStatus.FAIL, "Error Message is nnot visible so cannot verify error message : "+errorMsg, YesNo.Yes);
									sa.assertTrue(false, "Error Message is nnot visible so cannot verify error message : "+errorMsg);
								}
								driver.close();
								driver.switchTo().window(parentId);
								
							}else {
								log(LogStatus.FAIL, "No new window is found so cannot verify error message : "+errorMsg, YesNo.Yes);
								sa.assertTrue(false, "No new window is found so cannot verify error message : "+errorMsg);
							}
//							if (isAlertPresent(driver)) {
//								String msg=switchToAlertAndGetMessage(driver, 20, action.GETTEXT);
//								switchToAlertAndAcceptOrDecline(driver, 20, action.ACCEPT);
//								if (msg.contains(errorMsg)) {
//									appLog.info("successfully verified tag docs error message : "+errorMsg);
//								}
//								else {
//									log(LogStatus.ERROR, "could not verify tag docs error message: "+errorMsg+" from user", YesNo.Yes);
//									sa.assertTrue(false, "could not verify tag docs error message: "+errorMsg+" from user");
//								}
//							}
//							else {
//								log(LogStatus.ERROR, "Alert is not found so cannot check error message : "+errorMsg+" from user", YesNo.Yes);
//								sa.assertTrue(false, "Alert is not found so cannot check error message : "+errorMsg+" from user");
//							}
							switchToDefaultContent(driver);
							if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
							}
						}else {
							log(LogStatus.ERROR, "tag docs button is not clickable", YesNo.Yes);
							sa.assertTrue(false, "tag docs button is not clickable");
						}
					}
					else {
						log(LogStatus.ERROR, "could not click on document link in front of task", YesNo.Yes);
						sa.assertTrue(false, "could not click on document link in front of task");
					}
				}
				else {
					log(LogStatus.ERROR, "tasks detail page tab is not clickable", YesNo.Yes);
					sa.assertTrue(false, "tasks detail page tab is not clickable");
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot check error message on task tab", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot check error message on task tab");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc034_RegisterSuperAdminAndCheckErrorMessageOnFundPage(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		NIMPageBusinessLayer nim = new NIMPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		List<String> sideMenusList = new ArrayList<String>();
		String sideMenusOnNIM = "Internal Users<break>Folder Templates<break>Manage Approvals<break>Watermarking<break>File Distributor Settings<break>Profiles";
		lp.CRMLogin(superAdminUserName, adminPassword, appName);
		if(lp.clickOnTab(environment, mode, TabName.NIMTab)) {
			log(LogStatus.PASS, "clicked on NIM tab", YesNo.No);
			if(superAdminRegistered.equalsIgnoreCase("No")) {
				if(nim.NIMRegistration(userType.SuperAdmin, null,null)) {
					log(LogStatus.PASS, "Super Admin is registered successfully ", YesNo.No);
					ThreadSleep(5000);
					driver.switchTo().frame(nim.getNIMTabParentFrame_Lightning());
					ThreadSleep(2000);
					switchToFrame(driver, 60, nim.getFrame(PageName.NavatarInvestorManager,30));
					List<WebElement> sideMenus = nim.getnimPageSideMenus();
					for (int i = 0; i < sideMenus.size(); i++) {
						sideMenusList.add(sideMenus.get(i).getText().trim());
					}
					if (compareMultipleListWithoutAssertion(sideMenusOnNIM,
							sideMenusList)) {
						log(LogStatus.PASS, "All Menus are verified", YesNo.No);
					} else {
						log(LogStatus.FAIL, "All side menus are not veriified", YesNo.Yes);
						sa.assertTrue(false, "All side menus are not veriified");
					}
				}else {
					log(LogStatus.FAIL, "Not able to register superadmin", YesNo.Yes);
					sa.assertTrue(false, "Not able to register superadmin");
				}
				
			}else {
				ThreadSleep(10000);
				driver.switchTo().frame(nim.getNIMTabParentFrame_Lightning());
				ThreadSleep(5000);
				switchToFrame(driver, 60, nim.getFrame(PageName.NavatarInvestorManager,30));
				List<WebElement> sideMenus = nim.getnimPageSideMenus();
				for (int i = 0; i < sideMenus.size(); i++) {
					sideMenusList.add(sideMenus.get(i).getText().trim());
				}
				if (compareMultipleListWithoutAssertion(sideMenusOnNIM,
						sideMenusList)) {
					log(LogStatus.PASS, "All Menus are verified", YesNo.No);
				} else {
					log(LogStatus.FAIL, "All side menus are not veriified", YesNo.Yes);
					sa.assertTrue(false, "All side menus are not veriified");
				}
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on NIM Tab so cannot register SuperAdmin", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on NIM Tab so cannot register SuperAdmin");
		}
		switchToDefaultContent(driver);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
					if (fund.clickOnDocumentLinkInFrontOfTask(environment, mode, Smoke_Task3Name)) {
						String errorMsg=FundsPageErrorMessage.NoWorkSpaceBuildErrorMsg;
						switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 20));	
						if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
							String parentId=switchOnWindow(driver);
							if(parentId!=null) {
								WebElement ele= fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(60);
								if(ele!=null) {
									String msg=fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(20).getText().trim();
									if (msg.contains(errorMsg)) {
										appLog.info("successfully verified tag docs error message : "+errorMsg);
									}
									else {
										log(LogStatus.ERROR, "could not verify tag docs error message: "+errorMsg, YesNo.Yes);
										sa.assertTrue(false, "could not verify tag docs error message: "+errorMsg);
									}
								}else {
									log(LogStatus.FAIL, "Error Message is nnot visible so cannot verify error message : "+errorMsg, YesNo.Yes);
									sa.assertTrue(false, "Error Message is nnot visible so cannot verify error message : "+errorMsg);
								}
								driver.close();
								driver.switchTo().window(parentId);
								
							}else {
								log(LogStatus.FAIL, "No new window is found so cannot verify error message : "+errorMsg, YesNo.Yes);
								sa.assertTrue(false, "No new window is found so cannot verify error message : "+errorMsg);
							}
							if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
							}
						}else {
							log(LogStatus.ERROR, "tag docs button is not clickable", YesNo.Yes);
							sa.assertTrue(false, "tag docs button is not clickable");
						}
					}
					else {
						log(LogStatus.ERROR, "could not click on document link in front of task", YesNo.Yes);
						sa.assertTrue(false, "could not click on document link in front of task");
					}
				}
				else {
					log(LogStatus.ERROR, "tasks detail page tab is not clickable", YesNo.Yes);
					sa.assertTrue(false, "tasks detail page tab is not clickable");
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot check error message on task tab", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot check error message on task tab");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}

	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc035_provideInternalUserAccessAndCheckErrorMessage(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		NIMPageBusinessLayer nim = new NIMPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		lp.CRMLogin(superAdminUserName, adminPassword, appName);
		if(lp.clickOnTab(environment, mode, TabName.NIMTab)) {
			log(LogStatus.PASS, "clicked on NIM tab", YesNo.No);
		
			if(nim.giveAccessToUserInNIMTabFromAdmin(crmUser1FirstName+" "+crmUser1LastName, accessType.InternalUserAccess)) {
				appLog.info("internal user access has been provided to the user : "+crmUser1FirstName+" "+crmUser1LastName);

			}else {
				appLog.error("Not able to provide internal user access to the user : "+crmUser1FirstName+" "+crmUser1LastName);
				sa.assertTrue(false, "Not able to provide internal user access to the user : "+crmUser1FirstName+" "+crmUser1LastName);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on NIM Tab so cannot register SuperAdmin", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on NIM Tab so cannot register SuperAdmin");
		}
		lp.CRMlogout(environment, mode);
		driver.close();
		config(browserToLaunch);
		lp = new LoginPageBusinessLayer(driver);
		pipe = new PipelinesPageBusinessLayer(driver);
		nim = new NIMPageBusinessLayer(driver);
		fund = new FundsPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				String[] taskName= {Smoke_Task1Name,Smoke_Task3Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
						if (fund.clickOnDocumentLinkInFrontOfTask(environment, mode, taskName[i])) {
							String errorMsg=FundsPageErrorMessage.ErrorMsgBeforeRegisterCRMUser;
							switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 20));	
							if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
								String parentId=switchOnWindow(driver);
								if(parentId!=null) {
									WebElement ele= fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(60);
									if(ele!=null) {
										String msg=fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(20).getText().trim();
										if (msg.contains(errorMsg)) {
											appLog.info("successfully verified tag docs error message : "+errorMsg);
										}
										else {
											log(LogStatus.ERROR, "could not verify tag docs error message: "+errorMsg, YesNo.Yes);
											sa.assertTrue(false, "could not verify tag docs error message: "+errorMsg);
										}
									}else {
										log(LogStatus.FAIL, "Error Message is nnot visible so cannot verify error message : "+errorMsg, YesNo.Yes);
										sa.assertTrue(false, "Error Message is nnot visible so cannot verify error message : "+errorMsg);
									}
									driver.close();
									driver.switchTo().window(parentId);

								}else {
									log(LogStatus.FAIL, "No new window is found so cannot verify error message : "+errorMsg, YesNo.Yes);
									sa.assertTrue(false, "No new window is found so cannot verify error message : "+errorMsg);
								}
								if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
								}
							}else {
								log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
								sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
							}
						}else {
							log(LogStatus.ERROR, "Not able to click on document button of task : "+taskName[i]+" so cannot check error message ", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on document button of task : "+taskName[i]+" so cannot check error message ");
						}

					}else {
						log(LogStatus.ERROR, "tasks detail page tab is not clickable", YesNo.Yes);
						sa.assertTrue(false, "tasks detail page tab is not clickable");
					}
					if(i==0) {
						refresh(driver);
					}
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot check error message on task tab", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot check error message on task tab");
		}
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				String[] taskName= {Smoke_Task1Name,Smoke_Task3Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);

						if(task.docubleClickOnCreatedTask(TaskType.Completed, taskName[i], Smoke_Project1Name)) {
							log(LogStatus.PASS, "clicked on created task "+taskName[i], YesNo.No);
							ThreadSleep(3000);
							if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
								log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
								ThreadSleep(3000);
								String errorMsg=FundsPageErrorMessage.ErrorMsgBeforeRegisterCRMUser;
								switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 20));	
								if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
									String parentId=switchOnWindow(driver);
									if(parentId!=null) {
										WebElement ele= fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(60);
										if(ele!=null) {
											String msg=fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(20).getText().trim();
											if (msg.contains(errorMsg)) {
												appLog.info("successfully verified tag docs error message : "+errorMsg);
											}
											else {
												log(LogStatus.ERROR, "could not verify tag docs error message: "+errorMsg, YesNo.Yes);
												sa.assertTrue(false, "could not verify tag docs error message: "+errorMsg);
											}
										}else {
											log(LogStatus.FAIL, "Error Message is nnot visible so cannot verify error message : "+errorMsg, YesNo.Yes);
											sa.assertTrue(false, "Error Message is nnot visible so cannot verify error message : "+errorMsg);
										}
										driver.close();
										driver.switchTo().window(parentId);

									}else {
										log(LogStatus.FAIL, "No new window is found so cannot verify error message : "+errorMsg, YesNo.Yes);
										sa.assertTrue(false, "No new window is found so cannot verify error message : "+errorMsg);
									}
									switchToFrame(driver, 20, task.getFrame(PageName.TaskRayPage, 20));
									if(task.clickOnCreateTaskCrossIcon()) {
										log(LogStatus.PASS, "clicked on cross icon of task "+taskName[i], YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to click on cross icon so cannot close task popup : "+taskName[i], YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel button so cannot close task popUp: "+taskName[i]);
									}
								}else {
									log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
									sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
								}
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check Error Messsage on tag document window", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check Error Messsage on tag document window");
							}

						}else {
							log(LogStatus.FAIL, "Not able to click on created task "+taskName[i], YesNo.Yes);
							sa.assertTrue(false, "Not able to click on created task "+taskName[i]);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
					}
					if(i==0) {
						refresh(driver);
						ThreadSleep(10000);
						switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
					}
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name+","+Smoke_Task3Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name+","+Smoke_Task3Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check error message on navatar document of task "+Smoke_Task1Name+","+Smoke_Task3Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check error message on navatar document of task "+Smoke_Task1Name+","+Smoke_Task3Name);
		}
		switchToDefaultContent(driver);
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc036_registerCRMUserAndCheckErrorMessage(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		NIMPageBusinessLayer nim = new NIMPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		List<String> sideMenusList = new ArrayList<String>();
		String sideMenusOnNIM = "Internal Users<break>Folder Templates<break>Manage Approvals<break>Watermarking<break>Profiles";
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		if(lp.clickOnTab(environment, mode, TabName.NIMTab)) {
			log(LogStatus.PASS, "clicked on NIM tab", YesNo.No);
			if(nim.NIMRegistration(userType.CRMUser, null,null)) {
				log(LogStatus.PASS, "CRM User is registered successfully ", YesNo.No);
				ThreadSleep(5000);
				driver.switchTo().frame(nim.getNIMTabParentFrame_Lightning());
				ThreadSleep(2000);
				switchToFrame(driver, 60, nim.getFrame(PageName.NavatarInvestorManager,30));
				List<WebElement> sideMenus = nim.getnimPageSideMenus();
				for (int i = 0; i < sideMenus.size(); i++) {
					sideMenusList.add(sideMenus.get(i).getText().trim());
				}
				if (compareMultipleListWithoutAssertion(sideMenusOnNIM,
						sideMenusList)) {
					log(LogStatus.PASS, "All Menus are verified", YesNo.No);
				} else {
					log(LogStatus.FAIL, "All side menus are not veriified", YesNo.Yes);
					sa.assertTrue(false, "All side menus are not veriified");
				}
			}else {
				log(LogStatus.FAIL, "Not able to register CRM User", YesNo.Yes);
				sa.assertTrue(false, "Not able to register CRM User");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on NIM Tab so cannot register CRM User", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on NIM Tab so cannot register CRM User");
		}
		switchToDefaultContent(driver);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				String[] taskName= {Smoke_Task1Name,Smoke_Task3Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
						if (fund.clickOnDocumentLinkInFrontOfTask(environment, mode, taskName[i])) {
							String errorMsg=FundsPageErrorMessage.NoWorkSpaceBuildErrorMsg;
							switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 20));	
							if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
								String parentId=switchOnWindow(driver);
								if(parentId!=null) {
									WebElement ele= fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(60);
									if(ele!=null) {
										String msg=fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(20).getText().trim();
										if (msg.contains(errorMsg)) {
											appLog.info("successfully verified tag docs error message : "+errorMsg);
										}
										else {
											log(LogStatus.ERROR, "could not verify tag docs error message: "+errorMsg, YesNo.Yes);
											sa.assertTrue(false, "could not verify tag docs error message: "+errorMsg);
										}
									}else {
										log(LogStatus.FAIL, "Error Message is nnot visible so cannot verify error message : "+errorMsg, YesNo.Yes);
										sa.assertTrue(false, "Error Message is nnot visible so cannot verify error message : "+errorMsg);
									}
									driver.close();
									driver.switchTo().window(parentId);

								}else {
									log(LogStatus.FAIL, "No new window is found so cannot verify error message : "+errorMsg, YesNo.Yes);
									sa.assertTrue(false, "No new window is found so cannot verify error message : "+errorMsg);
								}
								if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
								}
							}else {
								log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
								sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
							}
						}else {
							log(LogStatus.ERROR, "Not able to click on document button of task : "+taskName[i]+" so cannot check error message ", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on document button of task : "+taskName[i]+" so cannot check error message ");
						}

					}else {
						log(LogStatus.ERROR, "tasks detail page tab is not clickable", YesNo.Yes);
						sa.assertTrue(false, "tasks detail page tab is not clickable");
					}
					if(i==0) {
						refresh(driver);
					}
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot check error message on task tab", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot check error message on task tab");
		}
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				String[] taskName= {Smoke_Task1Name,Smoke_Task3Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);

						if(task.docubleClickOnCreatedTask(TaskType.Completed, taskName[i], Smoke_Project1Name)) {
							log(LogStatus.PASS, "clicked on created task "+taskName[i], YesNo.No);
							ThreadSleep(3000);
							if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
								log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
								ThreadSleep(3000);
								String errorMsg=FundsPageErrorMessage.NoWorkSpaceBuildErrorMsg;
								switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 20));	
								if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
									String parentId=switchOnWindow(driver);
									if(parentId!=null) {
										WebElement ele= fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(60);
										if(ele!=null) {
											String msg=fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(20).getText().trim();
											if (msg.contains(errorMsg)) {
												appLog.info("successfully verified tag docs error message : "+errorMsg);
											}
											else {
												log(LogStatus.ERROR, "could not verify tag docs error message: "+errorMsg, YesNo.Yes);
												sa.assertTrue(false, "could not verify tag docs error message: "+errorMsg);
											}
										}else {
											log(LogStatus.FAIL, "Error Message is nnot visible so cannot verify error message : "+errorMsg, YesNo.Yes);
											sa.assertTrue(false, "Error Message is nnot visible so cannot verify error message : "+errorMsg);
										}
										driver.close();
										driver.switchTo().window(parentId);

									}else {
										log(LogStatus.FAIL, "No new window is found so cannot verify error message : "+errorMsg, YesNo.Yes);
										sa.assertTrue(false, "No new window is found so cannot verify error message : "+errorMsg);
									}
									switchToFrame(driver, 20, task.getFrame(PageName.TaskRayPage, 20));
									if(task.clickOnCreateTaskCrossIcon()) {
										log(LogStatus.PASS, "clicked on cross icon of task "+taskName[i], YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to click on cross icon so cannot close task popup : "+taskName[i], YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel button so cannot close task popUp: "+taskName[i]);
									}
								}else {
									log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
									sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
								}
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check Error Messsage on tag document window", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check Error Messsage on tag document window");
							}

						}else {
							log(LogStatus.FAIL, "Not able to click on created task "+taskName[i], YesNo.Yes);
							sa.assertTrue(false, "Not able to click on created task "+taskName[i]);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
					}
					if(i==0) {
						refresh(driver);
						ThreadSleep(10000);
						switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
					}
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name+","+Smoke_Task3Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name+","+Smoke_Task3Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check error message on navatar document of task "+Smoke_Task1Name+","+Smoke_Task3Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check error message on navatar document of task "+Smoke_Task1Name+","+Smoke_Task3Name);
		}
		switchToDefaultContent(driver);
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}

	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc037_1_buildWorkSpaceFRW(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID,adminPassword, appName);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				
				String Size=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Size);
				String vintageyear=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Vintage_Year);
				String contact=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Contact);
				String phone=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Phone);
				String email=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Email);
				String description=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Description);
				String templateName=ExcelUtils.readData("Foldertemp", 0, 1);
				
				String[] data= {Size,vintageyear,contact,phone,email,description};
				if(fund.buildWorkspace(data,WorkSpaceAction.CREATEFOLDERTEMPLATE,templateName,"FolderTemp",SmokeINS1, Workspace.FundraisingWorkspace,60)) {
					appLog.info("FundRaising work is build successfully.");
				}else {
					appLog.error("Not able to bulid fundraising workspace on fund: "+Smoke_Fund1);
					sa.assertTrue(false, "Not able to bulid fundraising workspace on fund: "+Smoke_Fund1);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1+" so cannot build fundraising workspace", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1+" so cannot build fundraising workspace");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot build fundraising workspace", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot build fundraising workspace");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc037_2_uploadDocumentInFRW(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		
		String commonFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.CommonPath);
		String internalFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.InternalPath);
		String sharedFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.SharedPath);
		String standardFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.StandardPath);
		
		String common_docpath="UploadFiles\\FundraisingWorkspaceFiles\\Common";
		String Internal_docpath="UploadFiles\\FundraisingWorkspaceFiles\\Internal";
		String Shared_docpath="UploadFiles\\FundraisingWorkspaceFiles\\Shared";
		String Standard_docpath="UploadFiles\\FundraisingWorkspaceFiles\\Standard";
		lp.CRMLogin(crmUser1EmailID,adminPassword, appName);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				
				if(fund.uploadFile(commonFolderPath,null, common_docpath,null,UploadFileActions.Upload, Workspace.FundraisingWorkspace, PageName.FundsPage, 30)) {
					log(LogStatus.PASS, "File is upload successfullly in folder : "+commonFolderPath, YesNo.No);
					
				}else {
					log(LogStatus.ERROR, "Not able to upload file in folder : "+commonFolderPath, YesNo.Yes);
					sa.assertTrue(false, "Not able to upload file in folder : "+commonFolderPath);
				}
				
				if(fund.uploadFile(internalFolderPath,null,Internal_docpath,null,UploadFileActions.Upload, Workspace.FundraisingWorkspace, PageName.FundsPage, 30)) {
					log(LogStatus.PASS, "File is upload successfullly in folder : "+internalFolderPath, YesNo.No);
					
				}else {
					log(LogStatus.ERROR, "Not able to upload file in folder : "+internalFolderPath, YesNo.Yes);
					sa.assertTrue(false, "Not able to upload file in folder : "+internalFolderPath);
				}
				
				if(fund.uploadFile(sharedFolderPath,null, Shared_docpath,null,UploadFileActions.Upload, Workspace.FundraisingWorkspace, PageName.FundsPage, 30)) {
					log(LogStatus.PASS, "File is upload successfullly in folder : "+sharedFolderPath, YesNo.No);
					
				}else {
					log(LogStatus.ERROR, "Not able to upload file in folder : "+sharedFolderPath, YesNo.Yes);
					sa.assertTrue(false, "Not able to upload file in folder : "+sharedFolderPath);
				}
				
				
				if(fund.uploadFile(standardFolderPath,SmokeINS1, Standard_docpath,null,UploadFileActions.Upload, Workspace.FundraisingWorkspace, PageName.FundsPage, 30)) {
					log(LogStatus.PASS, "File is upload successfullly in folder : "+standardFolderPath, YesNo.No);
					
				}else {
					log(LogStatus.ERROR, "Not able to upload file in folder : "+standardFolderPath, YesNo.Yes);
					sa.assertTrue(false, "Not able to upload file in folder : "+standardFolderPath);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1+" so cannot upload file in folders", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1+" so cannot upload file in folders");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot upload file in folders", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot upload file in folders");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc038_verifyFolderStructureInTagDocument(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		String dependOnTC="PESmokeTc037_2_uploadDocumentInFRW";
		String commonFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.CommonPath);
		String internalFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.InternalPath);
		String sharedFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.SharedPath);
		String standardFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.StandardPath);
		
		
		String commonFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileCommon);
		String internalFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileInternal);
		String sharedFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileShared);
		String standardFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileStandard);
		
		
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				String[] taskName= {Smoke_Task1Name,Smoke_Task3Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
						if (fund.clickOnDocumentLinkInFrontOfTask(environment, mode, taskName[i])) {
							switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 20));	
							if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
								String parentId=switchOnWindow(driver);
								if(parentId!=null) {
							
									if(traverseImport(driver, commonFolderPath, commonFileName)) {
										log(LogStatus.PASS, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.No);
									}else {
										log(LogStatus.FAIL, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.Yes);
										sa.assertTrue(false, "folder path is verified "+commonFolderPath+" file path : "+commonFileName);
									}
									
									if(traverseImport(driver, internalFolderPath, internalFileName)) {
										log(LogStatus.PASS, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.No);
									}else {
										log(LogStatus.FAIL, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.Yes);
										sa.assertTrue(false, "folder path is verified "+internalFolderPath+" file path : "+internalFileName);
									}
									
									if(traverseImport(driver, sharedFolderPath, sharedFileName)) {
										log(LogStatus.PASS, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.No);
									}else {
										log(LogStatus.FAIL, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.Yes);
										sa.assertTrue(false, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName);
									}
									
									if(traverseImport(driver, SmokeINS1+"/"+standardFolderPath,standardFileName)) {
										log(LogStatus.PASS, "folder path is verified "+SmokeINS1+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.No);
									}else {
										log(LogStatus.FAIL, "folder path is verified "+SmokeINS1+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.Yes);
										sa.assertTrue(false, "folder path is verified "+SmokeINS1+"/"+standardFolderPath+" file path : "+standardFileName);
									}
									driver.close();
									driver.switchTo().window(parentId);

								}else {
									log(LogStatus.FAIL, "No new window is found so cannot verify folder structure", YesNo.Yes);
									sa.assertTrue(false, "No new window is found so cannot verify folder structure");
								}
								if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
								}
							}else {
								log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
								sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
							}
						}else {
							log(LogStatus.ERROR, "Not able to click on document button of task : "+taskName[i]+" so cannot check folder structure and upload file", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on document button of task : "+taskName[i]+" so cannot check folder structure and upload file");
						}

					}else {
						log(LogStatus.ERROR, "tasks detail page tab is not clickable", YesNo.Yes);
						sa.assertTrue(false, "tasks detail page tab is not clickable");
					}
					if(i==0) {
						refresh(driver);
					}
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot check folder structure and upload file", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot check folder structure and upload file");
		}
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				String[] taskName= {Smoke_Task1Name,Smoke_Task3Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);

						if(task.docubleClickOnCreatedTask(TaskType.Completed, taskName[i], Smoke_Project1Name)) {
							log(LogStatus.PASS, "clicked on created task "+taskName[i], YesNo.No);
							ThreadSleep(3000);
							if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
								log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
								ThreadSleep(3000);
								switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 20));	
								if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
									String parentId=switchOnWindow(driver);
									if(parentId!=null) {
										
										if(traverseImport(driver, commonFolderPath, commonFileName)) {
											log(LogStatus.PASS, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.No);
										}else {
											log(LogStatus.FAIL, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.Yes);
											sa.assertTrue(false, "folder path is verified "+commonFolderPath+" file path : "+commonFileName);
										}
										
										if(traverseImport(driver, internalFolderPath, internalFileName)) {
											log(LogStatus.PASS, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.No);
										}else {
											log(LogStatus.FAIL, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.Yes);
											sa.assertTrue(false, "folder path is verified "+internalFolderPath+" file path : "+internalFileName);
										}
										
										if(traverseImport(driver, sharedFolderPath, sharedFileName)) {
											log(LogStatus.PASS, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.No);
										}else {
											log(LogStatus.FAIL, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.Yes);
											sa.assertTrue(false, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName);
										}
										
										if(traverseImport(driver, SmokeINS1+"/"+standardFolderPath,standardFileName)) {
											log(LogStatus.PASS, "folder path is verified "+SmokeINS1+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.No);
										}else {
											log(LogStatus.FAIL, "folder path is verified "+SmokeINS1+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.Yes);
											sa.assertTrue(false, "folder path is verified "+SmokeINS1+"/"+standardFolderPath+" file path : "+standardFileName);
										}
										driver.close();
										driver.switchTo().window(parentId);

									}else {
										log(LogStatus.FAIL, "No new window is found so cannot check folder structure and upload file", YesNo.Yes);
										sa.assertTrue(false, "No new window is found so cannot check folder structure and upload file");
									}
									switchToFrame(driver, 20, task.getFrame(PageName.TaskRayPage, 20));
									if(task.clickOnCreateTaskCrossIcon()) {
										log(LogStatus.PASS, "clicked on cross icon of task "+taskName[i], YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to click on cross icon so cannot close task popup : "+taskName[i], YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel button so cannot close task popUp: "+taskName[i]);
									}
								}else {
									log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
									sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
								}
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check folder structure and upload file on tag document window", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check folder structure and upload file on tag document window");
							}

						}else {
							log(LogStatus.FAIL, "Not able to click on created task "+taskName[i], YesNo.Yes);
							sa.assertTrue(false, "Not able to click on created task "+taskName[i]);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
					}
					if(i==0) {
						refresh(driver);
						ThreadSleep(10000);
						switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
					}
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name+","+Smoke_Task3Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name+","+Smoke_Task3Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check error message on navatar document of task "+Smoke_Task1Name+","+Smoke_Task3Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check error message on navatar document of task "+Smoke_Task1Name+","+Smoke_Task3Name);
		}
		switchToDefaultContent(driver);
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc039_tagDocumentsOnFundPage(String environment, String mode) {
		String dependOnTC="PESmokeTc037_2_uploadDocumentInFRW";
		String commonFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.CommonPath);
		String internalFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.InternalPath);
		
		
		String commonFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileCommon);
		String internalFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileInternal);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
	
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				
				
					if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
						ThreadSleep(3000);
						if(click(driver, pipe.getDocumentButton(Smoke_Task1Name, 30), Smoke_Task1Name+" document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on "+Smoke_Task1Name+" document button", YesNo.No);
							
							if(pipe.tagDocuments(environment, mode, PageName.FundsPage, commonFolderPath, commonFolderPath.split("/")[1],null, SelectOption.selectOrDeSelectAll, null).isEmpty()) {
								log(LogStatus.PASS, "Documents is tagged successfully from folder : "+commonFolderPath+" files : "+commonFileName, YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to tag documents from "+RelatedTab.Tasks+" on task "+Smoke_Task1Name+" from folder "+commonFolderPath, YesNo.Yes);
								sa.assertTrue(false, "Not able to tag documents from "+RelatedTab.Tasks+" on task "+Smoke_Task1Name+" from folder : "+commonFolderPath);
							}
							
							if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.FundsPage,commonFileName).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task1Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task1Name, YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task1Name);
							}
							
							if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
							}
							if(click(driver,pipe.getHeaderSideButtons(Header.CompletedTask, HeaderSideButton.Reload, 10), "reload button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "Clicked on "+Smoke_Task1Name+" reload button", YesNo.No);
								 
//									Smoke_Task1End_Date =getDateFormate("MMM d, yyyy",Smoke_Task1End_Date);
								   String[] headerNameList2= {"Name","Status","End Date","Owner","Documents"};
								   
								   String[][] DataList2= {{Smoke_Task1Name,"Completed",Smoke_Task1End_Date,externalUserFirstName+" "+externalUserLastName,"3","Documents"}};
									
								   SoftAssert TaskCompletedData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Tasks,Header.CompletedTask, headerNameList2,DataList2);						
								   sa.combineAssertions(TaskCompletedData);
							
							
							}else {
								log(LogStatus.FAIL, "Not able to click on reload button so cannot verify "+Smoke_Task1Name+" data ", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on reload button so cannot verify "+Smoke_Task1Name+" data ");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on document link of task "+Smoke_Task1Name+" so cannot tag document from folder "+commonFolderPath,YesNo.Yes);
							sa.assertTrue(false, "Not able to click on document link of task "+Smoke_Task1Name+" so cannot tag document from folder "+commonFolderPath);
						}
					}else {
						log(LogStatus.ERROR, "Not able to click on tasks tab so cannot tag document from "+commonFolderPath+" and "+internalFolderPath, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on tasks tab so cannot tag document from "+commonFolderPath+" and "+internalFolderPath);
					}
					refresh(driver);
					if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
						ThreadSleep(3000);
						if(click(driver, pipe.getDocumentButton(Smoke_Task3Name, 30), Smoke_Task3Name+" document button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on "+Smoke_Task3Name+" document button", YesNo.No);
							
							if(pipe.tagDocuments(environment, mode, PageName.FundsPage, internalFolderPath, internalFolderPath.split("/")[1],null, SelectOption.selectOrDeSelectAll, null).isEmpty()) {
								log(LogStatus.PASS, "Documents is tagged successfully from folder : "+internalFolderPath+" files : "+internalFileName, YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to tag documents from "+RelatedTab.Tasks+" on task "+Smoke_Task3Name+" from folder "+internalFolderPath, YesNo.Yes);
								sa.assertTrue(false, "Not able to tag documents from "+RelatedTab.Tasks+" on task "+Smoke_Task3Name+" from folder : "+internalFolderPath);
							}
							
							if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.FundsPage,internalFileName).isEmpty()) {
								log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+Smoke_Task3Name, YesNo.No);
							}else {
								log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task3Name, YesNo.Yes);
								sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+Smoke_Task3Name);
							}
							
							if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
							}
							if(click(driver,pipe.getHeaderSideButtons(Header.CompletedTask, HeaderSideButton.Reload, 10), "reload button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "Clicked on "+Smoke_Task3Name+" reload button", YesNo.No);
								 
								
								   String[] headerNameList2= {"Name","Status","End Date","Owner","Documents"};
								   
								   String[][] DataList2= {{Smoke_Task3Name,"Completed","",crmUser1FirstName+" "+crmUser1LastName,"3","Documents"}};
									
								   SoftAssert TaskCompletedData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Tasks,Header.CompletedTask, headerNameList2,DataList2);						
								   sa.combineAssertions(TaskCompletedData);
							
							
							}else {
								log(LogStatus.FAIL, "Not able to click on reload button so cannot verify "+Smoke_Task3Name+" data ", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on reload button so cannot verify "+Smoke_Task3Name+" data ");
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on document link of task "+Smoke_Task3Name+" so cannot tag document from folder "+commonFolderPath,YesNo.Yes);
							sa.assertTrue(false, "Not able to click on document link of task "+Smoke_Task3Name+" so cannot tag document from folder "+commonFolderPath);
						}
					}else {
						log(LogStatus.ERROR, "Not able to click on tasks tab so cannot tag document from "+commonFolderPath+" and "+internalFolderPath, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on tasks tab so cannot tag document from "+commonFolderPath+" and "+internalFolderPath);
					}
				
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1+" so cannot tag document from "+commonFolderPath+" and "+internalFolderPath, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1+" so cannot tag document from "+commonFolderPath+" and "+internalFolderPath);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot tag document in task "+Smoke_Task1Name+" and "+Smoke_Task3Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot tag document in task "+Smoke_Task1Name+" and "+Smoke_Task3Name);
		}
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
					String[] taskName= {Smoke_Task1Name,Smoke_Task3Name};
					String[] fileNames= {commonFileName,internalFileName};
					for(int i=0 ;i<taskName.length; i++) {
						if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
							log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
							ThreadSleep(3000);
							switchToDefaultContent(driver);
							if(task.verifyDocumentCountOnCreatedTask(TaskType.Completed,taskName[i],"3")) {
								log(LogStatus.PASS, "Document count is verified on created task "+taskName[i], YesNo.No);
							}else {
								log(LogStatus.PASS, "Document count is not verified on created task "+taskName[i], YesNo.No);
								sa.assertTrue(false, "Document count is not verified on created task "+taskName[i]);
							}
							switchToFrame(driver, 20, tr.getFrame(PageName.TaskRayPage, 10));
							if(task.docubleClickOnCreatedTask(TaskType.Completed, taskName[i], Smoke_Project1Name)) {
								log(LogStatus.PASS, "clicked on created task "+taskName[i], YesNo.No);
								ThreadSleep(3000);
								if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
									log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
									ThreadSleep(3000);
									switchToDefaultContent(driver);
									if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage, fileNames[i]).isEmpty()) {
										log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+taskName[i]+" on taskRay page", YesNo.No);
									}else {
										log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+fileNames[i]+" on taskRay page", YesNo.Yes);
										sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+fileNames[i]+" on taskRay page");
									}
									if(task.clickOnCreateTaskCrossIcon()) {
										log(LogStatus.PASS, "clicked on cross icon of task "+taskName[i], YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to click on cross icon so cannot close task popup : "+taskName[i], YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel button so cannot close task popUp: "+taskName[i]);
									}
								}else {
									log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot verify tagged document on "+taskName[i], YesNo.Yes);
									sa.assertTrue(false, "Not able to click on navatar documents tab so cannot verify tagged document on "+taskName[i]);
								}

							}else {
								log(LogStatus.FAIL, "Not able to click on created task "+taskName[i], YesNo.Yes);
								sa.assertTrue(false, "Not able to click on created task "+taskName[i]);
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
							sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
						}
						if(i==0) {
							refresh(driver);
							ThreadSleep(10000);
							switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
						}
					}
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check Navatar Document tag or document tab under created task");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc040_tagDocumentsOnTaskRayPage(String environment, String mode) {
		String dependOnTC="PESmokeTc037_2_uploadDocumentInFRW";
		String sharedFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.SharedPath);
		String standardFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.StandardPath);
		String sharedFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileShared);
		String standardFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileStandard);
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		TaskRayBusinessLayer tr = new TaskRayBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		String[] taskName = {Smoke_Task1Name,Smoke_Task3Name};
		String[] documentName = {sharedFileName,standardFileName};
		String[] folderPath= {sharedFolderPath,SmokeINS1+"/"+standardFolderPath};
		int[] splitNumber= {1,2};
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, tr.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
					for(int i=0 ;i<taskName.length; i++) {
						if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
							log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);
							ThreadSleep(3000);
							if(task.docubleClickOnCreatedTask(TaskType.Completed, taskName[i], Smoke_Project1Name)) {
								log(LogStatus.PASS, "clicked on created task "+taskName[i], YesNo.No);
								ThreadSleep(3000);
								if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
									log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
									ThreadSleep(3000);
									
									if(pipe.tagDocuments(environment, mode, PageName.TaskRayPage, folderPath[i], folderPath[i].split("/")[splitNumber[i]],null, SelectOption.selectOrDeSelectAll, null).isEmpty()) {
										log(LogStatus.PASS, "Documents is tagged successfully from folder : "+folderPath[i]+" files : "+documentName[i], YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to tag documents from navatar documents on task "+taskName[i]+" from folder "+folderPath[i], YesNo.Yes);
										sa.assertTrue(false, "Not able to tag documents from navatar documents on task "+taskName[i]+" from folder : "+folderPath[i]);
									}
									
									if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.TaskRayPage,documentName[i]).isEmpty()) {
										log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+taskName[i], YesNo.No);
									}else {
										log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+taskName[i], YesNo.Yes);
										sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+taskName[i]);
									}
									
									if(task.clickOnCreateTaskCrossIcon()) {
										log(LogStatus.PASS, "clicked on cross icon of task "+taskName[i], YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to click on cross icon so cannot close task popup : "+taskName[i], YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel button so cannot close task popUp: "+taskName[i]);
									}
									
									if(task.verifyDocumentCountOnCreatedTask(TaskType.Completed,taskName[i],"5")) {
										log(LogStatus.PASS, "Document count is verified on created task "+taskName[i], YesNo.No);
									}else {
										log(LogStatus.PASS, "Document count is not verified on created task "+taskName[i], YesNo.No);
										sa.assertTrue(false, "Document count is not verified on created task "+taskName[i]);
									}
									
								}else {
									log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot verify tag document on "+taskName[i], YesNo.Yes);
									sa.assertTrue(false, "Not able to click on navatar documents tab so cannot verify tag document on "+taskName[i]);
								}

							}else {
								log(LogStatus.FAIL, "Not able to click on created task "+taskName[i], YesNo.Yes);
								sa.assertTrue(false, "Not able to click on created task "+taskName[i]);
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
							sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
						}
						if(i==0) {
							refresh(driver);
							ThreadSleep(10000);
							switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
						}
					}
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot tag document in "+taskName[0]+" "+taskName[1], YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot tag document in "+taskName[0]+" "+taskName[1]);
		}
		switchToDefaultContent(driver);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
//					Smoke_Task1End_Date =getDateFormate("MMM d, yyyy",Smoke_Task1End_Date);
					String[] taskEndDate = {Smoke_Task1End_Date,""};
					String[] oWnerName= {externalUserFirstName+" "+externalUserLastName,crmUser1FirstName+" "+crmUser1LastName};
					for(int i=0; i<taskName.length; i++) {
						if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
							ThreadSleep(3000);
							
							String[] headerNameList2= {"Name","Status","End Date","Owner","Documents"};
							
							String[][] DataList2= {{taskName[i],"Completed",taskEndDate[i],oWnerName[i],"5","Documents"}};
							
							SoftAssert TaskCompletedData=pipe.verifyDataOnDueDiligenceAndDealEvaluationPage(environment,mode,RelatedTab.Tasks,Header.CompletedTask, headerNameList2,DataList2);						
							sa.combineAssertions(TaskCompletedData);
							
							
							if(click(driver, pipe.getDocumentButton(taskName[i], 30), taskName[i]+" document button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on "+taskName[i]+" document button", YesNo.No);
								
								
								if(pipe.verifyTaggedDocumentOnNavatarDocumentPopUp(environment, mode, PageName.FundsPage,documentName[i]).isEmpty()) {
									log(LogStatus.PASS, "Tagged Documents are verified on navatar document popUp for "+taskName[i], YesNo.No);
								}else {
									log(LogStatus.PASS, "Tagged Documents are not verified on navatar document popUp for "+taskName[i], YesNo.Yes);
									sa.assertTrue(false, "Tagged Documents are not verified on navatar document popUp for "+taskName[i]);
								}
								
								if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on navatar document pop up cancel button", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to click on navatar document cancel button so cannot close navatar document pop up", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on navatar document cancel button so cannot close navatar document pop up");
								}
							
							}else {
								log(LogStatus.FAIL, "Not able to click on document link of task "+taskName[i]+" so cannot verify document "+documentName[i],YesNo.Yes);
								sa.assertTrue(false, "Not able to click on document link of task "+taskName[i]+" so cannot verify document "+documentName[i]);
							}
							
						}else {
							log(LogStatus.ERROR, "Not able to click on tasks tab so cannot verify tagged document in task "+taskName[i], YesNo.Yes);
							sa.assertTrue(false, "Not able to click on tasks tab so cannot verify tagged document in task "+taskName[i]);
						}
						if(i==0) {
							refresh(driver);
						}
					}
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1+" so cannot verify tagged document in task "+taskName[0]+","+taskName[1], YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1+" so cannot verify tagged document in task "+taskName[0]+","+taskName[1]);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot verify tagged document in task "+Smoke_Task1Name+" and "+Smoke_Task3Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot verify tagged document in task "+Smoke_Task1Name+" and "+Smoke_Task3Name);
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc041_1_buildWorkSpaceINW(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		lp.CRMLogin(crmUser1EmailID,adminPassword, appName);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				
				String Size=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Size);
				String vintageyear=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Vintage_Year);
				String contact=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Contact);
				String phone=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Phone);
				String email=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Email);
				String description=ExcelUtils.readData(testCasesFilePath,"Funds",excelLabel.Variable_Name, "SmokeFund1", excelLabel.Fund_Description);
				String templateName=ExcelUtils.readData("Foldertemp", 0, 1);
				
				String[] data= {Size,vintageyear,contact,phone,email,description};
				if(fund.buildWorkspace(data,WorkSpaceAction.CREATEFOLDERTEMPLATE,templateName,"FolderTemp",SmokeINS1+"/"+Smoke_LP1Name, Workspace.InvestorWorkspace,60)) {
					appLog.info("Investor work is build successfully.");
				}else {
					appLog.error("Not able to bulid Investor workspace on fund: "+Smoke_Fund1);
					sa.assertTrue(false, "Not able to bulid Investor workspace on fund: "+Smoke_Fund1);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1+" so cannot build Investor workspace", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1+" so cannot build Investor workspace");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot build Investor workspace", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot build Investor workspace");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc041_2_uploadDocumentInINV(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		
		String commonFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.CommonPath);
		String internalFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.InternalPath);
		String sharedFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.SharedPath);
		String standardFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.StandardPath);
		
		String common_docpath="UploadFiles\\InvestorWorkspaceFiles\\Common";
		String Internal_docpath="UploadFiles\\InvestorWorkspaceFiles\\Internal";
		String Shared_docpath="UploadFiles\\InvestorWorkspaceFiles\\Shared";
		String Standard_docpath="UploadFiles\\InvestorWorkspaceFiles\\Standard";
		lp.CRMLogin(crmUser1EmailID,adminPassword, appName);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				
				if(fund.uploadFile(commonFolderPath,null, common_docpath,null,UploadFileActions.Upload, Workspace.InvestorWorkspace, PageName.FundsPage, 30)) {
					log(LogStatus.PASS, "File is upload successfullly in folder : "+commonFolderPath, YesNo.No);
					
				}else {
					log(LogStatus.ERROR, "Not able to upload file in folder : "+commonFolderPath, YesNo.Yes);
					sa.assertTrue(false, "Not able to upload file in folder : "+commonFolderPath);
				}
				
				if(fund.uploadFile(internalFolderPath,null,Internal_docpath,null,UploadFileActions.Upload, Workspace.InvestorWorkspace, PageName.FundsPage, 30)) {
					log(LogStatus.PASS, "File is upload successfullly in folder : "+internalFolderPath, YesNo.No);
					
				}else {
					log(LogStatus.ERROR, "Not able to upload file in folder : "+internalFolderPath, YesNo.Yes);
					sa.assertTrue(false, "Not able to upload file in folder : "+internalFolderPath);
				}
				
				if(fund.uploadFile(sharedFolderPath,null, Shared_docpath,null,UploadFileActions.Upload, Workspace.InvestorWorkspace, PageName.FundsPage, 30)) {
					log(LogStatus.PASS, "File is upload successfullly in folder : "+sharedFolderPath, YesNo.No);
					
				}else {
					log(LogStatus.ERROR, "Not able to upload file in folder : "+sharedFolderPath, YesNo.Yes);
					sa.assertTrue(false, "Not able to upload file in folder : "+sharedFolderPath);
				}
				
				
				if(fund.uploadFile(standardFolderPath,SmokeINS1+"/"+Smoke_LP1Name, Standard_docpath,null,UploadFileActions.Upload, Workspace.InvestorWorkspace, PageName.FundsPage, 30)) {
					log(LogStatus.PASS, "File is upload successfullly in folder : "+standardFolderPath, YesNo.No);
					
				}else {
					log(LogStatus.ERROR, "Not able to upload file in folder : "+standardFolderPath, YesNo.Yes);
					sa.assertTrue(false, "Not able to upload file in folder : "+standardFolderPath);
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1+" so cannot upload file in folders", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1+" so cannot upload file in folders");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot upload file in folders", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot upload file in folders");
		}
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}

	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc042_checkFolderStructureOnDocumentPopUp(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		String dependOnTC="PESmokeTc041_2_uploadDocumentInINV";
		String commonFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.CommonPath);
		String internalFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.InternalPath);
		String sharedFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.SharedPath);
		String standardFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.StandardPath);
		
		
		String commonFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileCommon);
		String internalFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileInternal);
		String sharedFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileShared);
		String standardFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileStandard);
		
		
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				String[] taskName= {Smoke_Task1Name,Smoke_Task3Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
						if (fund.clickOnDocumentLinkInFrontOfTask(environment, mode, taskName[i])) {
							switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 20));	
							if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
								log(LogStatus.PASS, "Clicked on Tag document button", YesNo.No);
								String parentId=switchOnWindow(driver);
								if(parentId!=null) {
									int j=0;
									String[] ss = {"Please select WorkSpace Label Text","Fundraising workspace text and radio button","Investor workspace text and radio button","cancel button","continue button"};
									WebElement[] ele= {fund.getPleaseSelectWorkSpaceLabelText(30),fund.getWorkSpaceRadioButtonOnTagDocumentPopUp(Workspace.FundraisingWorkspace, 30),fund.getWorkSpaceRadioButtonOnTagDocumentPopUp(Workspace.InvestorWorkspace, 30),
											fund.getCancelButtOnPleaseSelectWorkOnTagDocumentPopUp(20), fund.getContinueButtOnPleaseSelectWorkOnTagDocumentPopUp(20)};
									
									for(WebElement ele1 : ele) {
										if(ele1!=null) {
											log(LogStatus.PASS, ss[j]+" is displaying on tag document popUp	", YesNo.No);
										}else {
											log(LogStatus.FAIL, ss[j]+" is not displaying on tag document popUp	", YesNo.Yes);
											sa.assertTrue(false, ss[j]+" is not displaying on tag document popUp");
										}
										j++;
									}
									if(isSelected(driver, fund.getWorkSpaceRadioButtonOnTagDocumentPopUp(Workspace.FundraisingWorkspace, 30), "fundraising workspace radio button")) {
										log(LogStatus.PASS, "Fundraising workspace radio button is default selected", YesNo.No);
									}else {
										log(LogStatus.PASS, "Fundraising workspace radio button is not selected", YesNo.Yes);
										sa.assertTrue(false, "Fundraising workspace is default selected");
									}
									if(clickUsingJavaScript(driver, fund.getCancelButtOnPleaseSelectWorkOnTagDocumentPopUp(30), "cancel button on please select workspace popup")) {
										log(LogStatus.PASS, "clicked on cancel button and pop up closed ", YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to click on cancel button so cannot close tag document popup", YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel button so cannot close tag document popup");
									}
									driver.switchTo().window(parentId);
									parentId=null;
									switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 20));	
									if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
										log(LogStatus.PASS, "clicked on tag document button", YesNo.No);
										parentId=switchOnWindow(driver);
										if(parentId!=null) {
											ThreadSleep(7000);
											if(click(driver, fund.getWorkSpaceRadioButtonOnTagDocumentPopUp(Workspace.InvestorWorkspace, 40), "investor workSpace radio button", action.SCROLLANDBOOLEAN)) {
												log(LogStatus.PASS, "clicked on investor workspace radio button", YesNo.No);
												if(clickUsingJavaScript(driver, fund.getContinueButtOnPleaseSelectWorkOnTagDocumentPopUp(30), "cancel button on please select workspace popup")) {
													log(LogStatus.PASS, "clicked on continue button and pop up closed ", YesNo.No);
													ThreadSleep(5000);
													if(traverseImport(driver, commonFolderPath, commonFileName)) {
														log(LogStatus.PASS, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.No);
													}else {
														log(LogStatus.FAIL, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.Yes);
														sa.assertTrue(false, "folder path is verified "+commonFolderPath+" file path : "+commonFileName);
													}

													if(traverseImport(driver, internalFolderPath, internalFileName)) {
														log(LogStatus.PASS, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.No);
													}else {
														log(LogStatus.FAIL, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.Yes);
														sa.assertTrue(false, "folder path is verified "+internalFolderPath+" file path : "+internalFileName);
													}

													if(traverseImport(driver, sharedFolderPath, sharedFileName)) {
														log(LogStatus.PASS, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.No);
													}else {
														log(LogStatus.FAIL, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.Yes);
														sa.assertTrue(false, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName);
													}

													if(traverseImport(driver, SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath,standardFileName)) {
														log(LogStatus.PASS, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.No);
													}else {
														log(LogStatus.FAIL, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.Yes);
														sa.assertTrue(false, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName);
													}
												}else {
													log(LogStatus.FAIL, "Not able to click on continue button so cannot close tag document popup", YesNo.Yes);
													sa.assertTrue(false, "Not able to click on continue button so cannot close tag document popup");
												}
											}else {
												log(LogStatus.FAIL, "Not able to click on investor radio button so cannot verify folder structure", YesNo.Yes);
												sa.assertTrue(false, "Not able to click on investor radio button so cannot verify folder structure");
											}
										}else {
											log(LogStatus.FAIL, "No new window is found so cannot verify folder structure", YesNo.Yes);
											sa.assertTrue(false, "No new window is found so cannot verify folder structure");
										}
									}else {
										log(LogStatus.FAIL, "Not able to click on tag document button so cannot check folder structure", YesNo.Yes);
										sa.assertTrue(false, "Not able to click on tag document button so cannot check folder structure");
									}
									driver.close();
									driver.switchTo().window(parentId);
								}else {
									log(LogStatus.FAIL, "No new window is found so cannot verify folder structure", YesNo.Yes);
									sa.assertTrue(false, "No new window is found so cannot verify folder structure");
								}
								if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
								}
							}else {
								log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
								sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
							}
						}else {
							log(LogStatus.ERROR, "Not able to click on document button of task : "+taskName[i]+" so cannot check folder structure and upload file", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on document button of task : "+taskName[i]+" so cannot check folder structure and upload file");
						}

					}else {
						log(LogStatus.ERROR, "tasks detail page tab is not clickable", YesNo.Yes);
						sa.assertTrue(false, "tasks detail page tab is not clickable");
					}
					if(i==0) {
						refresh(driver);
					}
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot check folder structure and upload file", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot check folder structure and upload file");
		}
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				String[] taskName= {Smoke_Task1Name,Smoke_Task3Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);

						if(task.docubleClickOnCreatedTask(TaskType.Completed, taskName[i], Smoke_Project1Name)) {
							log(LogStatus.PASS, "clicked on created task "+taskName[i], YesNo.No);
							ThreadSleep(3000);
							if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
								log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
								ThreadSleep(3000);
								switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 20));	
								if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
									String parentId=switchOnWindow(driver);
									if(parentId!=null) {
										int j=0;
										String[] ss = {"Please select WorkSpace Label Text","Fundraising workspace text and radio button","Investor workspace text and radio button","cancel button","continue button"};
										WebElement[] ele= {fund.getPleaseSelectWorkSpaceLabelText(30),fund.getWorkSpaceRadioButtonOnTagDocumentPopUp(Workspace.FundraisingWorkspace, 30),fund.getWorkSpaceRadioButtonOnTagDocumentPopUp(Workspace.InvestorWorkspace, 30),
												fund.getCancelButtOnPleaseSelectWorkOnTagDocumentPopUp(20), fund.getContinueButtOnPleaseSelectWorkOnTagDocumentPopUp(20)};
										
										for(WebElement ele1 : ele) {
											if(ele1!=null) {
												log(LogStatus.PASS, ss[j]+" is displaying on tag document popUp	", YesNo.No);
											}else {
												log(LogStatus.FAIL, ss[j]+" is not displaying on tag document popUp	", YesNo.Yes);
												sa.assertTrue(false, ss[j]+" is not displaying on tag document popUp");
											}
											j++;
										}
										if(isSelected(driver, fund.getWorkSpaceRadioButtonOnTagDocumentPopUp(Workspace.FundraisingWorkspace, 30), "fundraising workspace radio button")) {
											log(LogStatus.PASS, "Fundraising workspace radio button is default selected", YesNo.No);
										}else {
											log(LogStatus.PASS, "Fundraising workspace radio button is not selected", YesNo.Yes);
											sa.assertTrue(false, "Fundraising workspace is default selected");
										}
										if(clickUsingJavaScript(driver, fund.getCancelButtOnPleaseSelectWorkOnTagDocumentPopUp(30), "cancel button on please select workspace popup")) {
											log(LogStatus.PASS, "clicked on cancel button and pop up closed ", YesNo.No);
										}else {
											log(LogStatus.FAIL, "Not able to click on cancel button so cannot close tag document popup", YesNo.Yes);
											sa.assertTrue(false, "Not able to click on cancel button so cannot close tag document popup");
										}
										driver.switchTo().window(parentId);
										parentId=null;
										switchToFrame(driver, 20, task.getFrame(PageName.TaskRayPage, 20));
										switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 20));	
										if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
											log(LogStatus.PASS, "clicked on tag document button", YesNo.No);
											parentId=switchOnWindow(driver);
											if(parentId!=null) {
												ThreadSleep(7000);
												if(click(driver, fund.getWorkSpaceRadioButtonOnTagDocumentPopUp(Workspace.InvestorWorkspace, 40), "investor workSpace radio button", action.SCROLLANDBOOLEAN)) {
													log(LogStatus.PASS, "clicked on investor workspace radio button", YesNo.No);
													if(clickUsingJavaScript(driver, fund.getContinueButtOnPleaseSelectWorkOnTagDocumentPopUp(30), "cancel button on please select workspace popup")) {
														log(LogStatus.PASS, "clicked on continue button and pop up closed ", YesNo.No);
														ThreadSleep(5000);
														if(traverseImport(driver, commonFolderPath, commonFileName)) {
															log(LogStatus.PASS, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.No);
														}else {
															log(LogStatus.FAIL, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.Yes);
															sa.assertTrue(false, "folder path is verified "+commonFolderPath+" file path : "+commonFileName);
														}

														if(traverseImport(driver, internalFolderPath, internalFileName)) {
															log(LogStatus.PASS, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.No);
														}else {
															log(LogStatus.FAIL, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.Yes);
															sa.assertTrue(false, "folder path is verified "+internalFolderPath+" file path : "+internalFileName);
														}

														if(traverseImport(driver, sharedFolderPath, sharedFileName)) {
															log(LogStatus.PASS, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.No);
														}else {
															log(LogStatus.FAIL, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.Yes);
															sa.assertTrue(false, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName);
														}

														if(traverseImport(driver, SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath,standardFileName)) {
															log(LogStatus.PASS, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.No);
														}else {
															log(LogStatus.FAIL, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.Yes);
															sa.assertTrue(false, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName);
														}
													}else {
														log(LogStatus.FAIL, "Not able to click on continue button so cannot close tag document popup", YesNo.Yes);
														sa.assertTrue(false, "Not able to click on continue button so cannot close tag document popup");
													}
												}else {
													log(LogStatus.FAIL, "Not able to click on investor radio button so cannot verify folder structure", YesNo.Yes);
													sa.assertTrue(false, "Not able to click on investor radio button so cannot verify folder structure");
												}
											}else {
												log(LogStatus.FAIL, "No new window is found so cannot verify folder structure", YesNo.Yes);
												sa.assertTrue(false, "No new window is found so cannot verify folder structure");
											}
										}else {
											log(LogStatus.FAIL, "Not able to click on tag document button so cannot check folder structure", YesNo.Yes);
											sa.assertTrue(false, "Not able to click on tag document button so cannot check folder structure");
										}
										driver.close();
										driver.switchTo().window(parentId);

									}else {
										log(LogStatus.FAIL, "No new window is found so cannot check folder structure and upload file", YesNo.Yes);
										sa.assertTrue(false, "No new window is found so cannot check folder structure and upload file");
									}
									switchToFrame(driver, 20, task.getFrame(PageName.TaskRayPage, 20));
									if(task.clickOnCreateTaskCrossIcon()) {
										log(LogStatus.PASS, "clicked on cross icon of task "+taskName[i], YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to click on cross icon so cannot close task popup : "+taskName[i], YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel button so cannot close task popUp: "+taskName[i]);
									}
								}else {
									log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
									sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
								}
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check folder structure and upload file on tag document window", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check folder structure and upload file on tag document window");
							}

						}else {
							log(LogStatus.FAIL, "Not able to click on created task "+taskName[i], YesNo.Yes);
							sa.assertTrue(false, "Not able to click on created task "+taskName[i]);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
					}
					if(i==0) {
						refresh(driver);
						ThreadSleep(10000);
						switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
					}
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name+","+Smoke_Task3Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name+","+Smoke_Task3Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check folder structure on navatar document of task "+Smoke_Task1Name+","+Smoke_Task3Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check folder structure on navatar document of task "+Smoke_Task1Name+","+Smoke_Task3Name);
		}
		switchToDefaultContent(driver);
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc043_1_clearFundRaisingWorkSpace(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		lp.CRMLogin(superAdminUserName,adminPassword, appName);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				
				scrollDownThroughWebelement(driver, fund.getWorkSpaceLabel(60), "Workspace View.");
				ThreadSleep(10000);
				switchToFrame(driver, 50, fund.getFrame(PageName.FundsPage, 60));
				System.err.println("Switched to frame.");
				scrollDownThroughWebelement(driver, fund.getWorkspaceSectionView(Workspace.FundraisingWorkspace, 60), "Fundraising Workspace View.");
				
				if(fund.clearWorkSpace(Workspace.FundraisingWorkspace, 30)) {
					log(LogStatus.PASS, "Fundraising WorkSpace is cleared successfully", YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to clear fundraising workspace ", YesNo.Yes);
					sa.assertTrue(false, "Not able to clear fundraising workspace ");
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1+" so cannot clear fundraising workspace", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1+" so cannot clear fundraising workspace");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot clear fundraising workspace", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot clear fundraising workspace");
		}
		switchToDefaultContent(driver);
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}

	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc043_2_checkImpactAfterClearFRW(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		String dependOnTC="PESmokeTc041_2_uploadDocumentInINV";
		String commonFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.CommonPath);
		String internalFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.InternalPath);
		String sharedFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.SharedPath);
		String standardFolderPath=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.StandardPath);
		
		
		String commonFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileCommon);
		String internalFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileInternal);
		String sharedFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileShared);
		String standardFileName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,dependOnTC, excelLabel.UploadedFileStandard);
		
		String commonDocName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.UploadedFileCommon);
		String StdDocName=ExcelUtils.readData("FilePath",excelLabel.TestCases_Name,currentlyExecutingTC, excelLabel.UploadedFileStandard);
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				String[] taskName= {Smoke_Task1Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
						if (fund.clickOnDocumentLinkInFrontOfTask(environment, mode, taskName[i])) {
							switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 20));
							
							if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
								log(LogStatus.PASS, "Clicked on Tag document button", YesNo.No);
								String parentId=switchOnWindow(driver);
								if(parentId!=null) {

									if(fund.getPleaseSelectWorkSpaceLabelText(30)==null) {
										log(LogStatus.PASS, "Please select workspace popup is not displaying in tag document popup", YesNo.No);
									}else {
										log(LogStatus.FAIL,"Please select workspace popup is not displaying in tag document popup",YesNo.Yes);
										sa.assertTrue(false, "Please select workspace popup is not displaying in tag document popup");
									}
									if(traverseImport(driver, commonFolderPath, commonFileName)) {
										log(LogStatus.PASS, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.No);
									}else {
										log(LogStatus.FAIL, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.Yes);
										sa.assertTrue(false, "folder path is verified "+commonFolderPath+" file path : "+commonFileName);
									}
									if(traverseImport(driver, internalFolderPath, internalFileName)) {
										log(LogStatus.PASS, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.No);
									}else {
										log(LogStatus.FAIL, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.Yes);
										sa.assertTrue(false, "folder path is verified "+internalFolderPath+" file path : "+internalFileName);
									}
									if(traverseImport(driver, sharedFolderPath, sharedFileName)) {
										log(LogStatus.PASS, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.No);
									}else {
										log(LogStatus.FAIL, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.Yes);
										sa.assertTrue(false, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName);
									}
									if(traverseImport(driver, SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath,standardFileName)) {
										log(LogStatus.PASS, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.No);
									}else {
										log(LogStatus.FAIL, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.Yes);
										sa.assertTrue(false, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName);
									}
									driver.close();
									driver.switchTo().window(parentId);
									
								}else {
									log(LogStatus.FAIL, "No new window is found so cannot verify folder structure", YesNo.Yes);
									sa.assertTrue(false, "No new window is found so cannot verify folder structure");
								}
								if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.NavatarDocumentPopUpOnPipeLinePage, commonDocName)) {
									log(LogStatus.PASS, "Error Message is verified for document "+commonDocName, YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to verify error message for document "+commonDocName, YesNo.Yes);
									sa.assertTrue(false, "Not able to verify error message for document "+commonDocName);
								}
								if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
								}
							}else {
								log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
								sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
							}
						}else {
							log(LogStatus.ERROR, "Not able to click on document button of task : "+taskName[i]+" so cannot check folder structure", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on document button of task : "+taskName[i]+" so cannot check folder structure");
						}

					}else {
						log(LogStatus.ERROR, "tasks detail page tab is not clickable", YesNo.Yes);
						sa.assertTrue(false, "tasks detail page tab is not clickable");
					}
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot check folder structure", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot check folder structure");
		}
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				String[] taskName= {Smoke_Task3Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);

						if(task.docubleClickOnCreatedTask(TaskType.Completed, taskName[i], Smoke_Project1Name)) {
							log(LogStatus.PASS, "clicked on created task "+taskName[i], YesNo.No);
							ThreadSleep(3000);
							if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
								log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
								ThreadSleep(3000);
								switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 20));	
								if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
									String parentId=switchOnWindow(driver);
									if(parentId!=null) {
										if(fund.getPleaseSelectWorkSpaceLabelText(30)==null) {
											log(LogStatus.PASS, "Please select workspace popup is not displaying in tag document popup", YesNo.No);
										}else {
											log(LogStatus.FAIL,"Please select workspace popup is not displaying in tag document popup",YesNo.Yes);
											sa.assertTrue(false, "Please select workspace popup is not displaying in tag document popup");
										}
										if(traverseImport(driver, commonFolderPath, commonFileName)) {
											log(LogStatus.PASS, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.No);
										}else {
											log(LogStatus.FAIL, "folder path is verified "+commonFolderPath+" file path : "+commonFileName, YesNo.Yes);
											sa.assertTrue(false, "folder path is verified "+commonFolderPath+" file path : "+commonFileName);
										}

										if(traverseImport(driver, internalFolderPath, internalFileName)) {
											log(LogStatus.PASS, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.No);
										}else {
											log(LogStatus.FAIL, "folder path is verified "+internalFolderPath+" file path : "+internalFileName, YesNo.Yes);
											sa.assertTrue(false, "folder path is verified "+internalFolderPath+" file path : "+internalFileName);
										}

										if(traverseImport(driver, sharedFolderPath, sharedFileName)) {
											log(LogStatus.PASS, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.No);
										}else {
											log(LogStatus.FAIL, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName, YesNo.Yes);
											sa.assertTrue(false, "folder path is verified "+sharedFolderPath+" file path : "+sharedFileName);
										}

										if(traverseImport(driver, SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath,standardFileName)) {
											log(LogStatus.PASS, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.No);
										}else {
											log(LogStatus.FAIL, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName, YesNo.Yes);
											sa.assertTrue(false, "folder path is verified "+SmokeINS1+"/"+Smoke_LP1Name+"/"+standardFolderPath+" file path : "+standardFileName);
										}
										driver.close();
										driver.switchTo().window(parentId);

									}else {
										log(LogStatus.FAIL, "No new window is found so cannot check folder structure and upload file", YesNo.Yes);
										sa.assertTrue(false, "No new window is found so cannot check folder structure and upload file");
									}
									if(pipe.verifyDocumentOpenAndDownloadFunctionality(environment, mode, PageName.TaskRayPage, StdDocName)) {
										log(LogStatus.PASS, "Error Message is verified for document "+StdDocName, YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to verify error message for document "+StdDocName, YesNo.Yes);
										sa.assertTrue(false, "Not able to verify error message for document "+StdDocName);
									}
									switchToFrame(driver, 20, task.getFrame(PageName.TaskRayPage, 20));
									if(task.clickOnCreateTaskCrossIcon()) {
										log(LogStatus.PASS, "clicked on cross icon of task "+taskName[i], YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to click on cross icon so cannot close task popup : "+taskName[i], YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel button so cannot close task popUp: "+taskName[i]);
									}
								}else {
									log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
									sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
								}
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check folder structure on tag document window", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check folder structure on tag document window");
							}

						}else {
							log(LogStatus.FAIL, "Not able to click on created task "+taskName[i], YesNo.Yes);
							sa.assertTrue(false, "Not able to click on created task "+taskName[i]);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
					}
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task3Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name+","+Smoke_Task3Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check folder structure on navatar document of task "+Smoke_Task3Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check folder structure on navatar document of task "+Smoke_Task1Name+","+Smoke_Task3Name);
		}
		switchToDefaultContent(driver);
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}

	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc044_1_clearInvestorWorkSpace(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		lp.CRMLogin(superAdminUserName,adminPassword, appName);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				
				scrollDownThroughWebelement(driver, fund.getWorkSpaceLabel(60), "Workspace View.");
				ThreadSleep(10000);
				switchToFrame(driver, 50, fund.getFrame(PageName.FundsPage, 60));
				System.err.println("Switched to frame.");
				scrollDownThroughWebelement(driver, fund.getWorkspaceSectionView(Workspace.InvestorWorkspace, 60), "Investor Workspace View.");
				
				if(fund.clearWorkSpace(Workspace.InvestorWorkspace, 30)) {
					log(LogStatus.PASS, "Investor WorkSpace is cleared successfully", YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to clear Investor workspace ", YesNo.Yes);
					sa.assertTrue(false, "Not able to clear Investor workspace ");
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1+" so cannot clear Investor workspace", YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1+" so cannot clear Investor workspace");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot clear Investor workspace", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot clear Investor workspace");
		}
		switchToDefaultContent(driver);
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}

	@Parameters({ "environment", "mode" })
	@Test
	public void PESmokeTc044_2_checkImpactAfterClearINV(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		PipelinesPageBusinessLayer pipe = new PipelinesPageBusinessLayer(driver);
		FundsPageBusinessLayer fund = new FundsPageBusinessLayer(driver);
		TaskRayBusinessLayer task = new TaskRayBusinessLayer(driver);
		String errorMsg=FundsPageErrorMessage.NoWorkSpaceBuildErrorMsg;
		lp.CRMLogin(crmUser1EmailID, adminPassword, appName);
		if(fund.clickOnTab(environment, mode, TabName.FundsTab)) {
			log(LogStatus.PASS, "Clicked on fund tab", YesNo.No);
			if(fund.clickOnCreatedFund(environment, mode, Smoke_Fund1)) {
				log(LogStatus.PASS, "clicked on fund : "+Smoke_Fund1, YesNo.No);
				String[] taskName= {Smoke_Task1Name,Smoke_Task3Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(click(driver, fund.getRelatedTab(environment, mode, PageName.FundsPage, RelatedTab.Tasks, 30), " tasks tab", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Clicked on Tasks Tab", YesNo.No);
						if (fund.clickOnDocumentLinkInFrontOfTask(environment, mode, taskName[i])) {
							switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 20));
							
							if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
								log(LogStatus.PASS, "Clicked on Tag document button", YesNo.No);
								String parentId=switchOnWindow(driver);
								if(parentId!=null) {
									ThreadSleep(3000);
									WebElement ele= fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(60);
									if(ele!=null) {
										String msg=fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(20).getText().trim();
										if (msg.contains(errorMsg)) {
											appLog.info("successfully verified tag docs error message : "+errorMsg);
										}
										else {
											log(LogStatus.ERROR, "could not verify tag docs error message: "+errorMsg, YesNo.Yes);
											sa.assertTrue(false, "could not verify tag docs error message: "+errorMsg);
										}
									}else {
										log(LogStatus.FAIL, "Error Message is nnot visible so cannot verify error message : "+errorMsg, YesNo.Yes);
										sa.assertTrue(false, "Error Message is nnot visible so cannot verify error message : "+errorMsg);
									}
									driver.close();
									driver.switchTo().window(parentId);
								}else {
									log(LogStatus.FAIL, "No new window is found so cannot verify folder structure", YesNo.Yes);
									sa.assertTrue(false, "No new window is found so cannot verify folder structure");
								}
								if(click(driver, pipe.getCancelButtonOnNavatarDocumentsPopUp(10), "cancel button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on cancel button", YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to click on cancel button so cannot close navatar document pop up", YesNo.Yes);
									sa.assertTrue(false, "Not able to click on cancel button so cannot close navatar document pop up");
								}
							}else {
								log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
								sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
							}
						}else {
							log(LogStatus.ERROR, "Not able to click on document button of task : "+taskName[i]+" so cannot check folder structure", YesNo.Yes);
							sa.assertTrue(false, "Not able to click on document button of task : "+taskName[i]+" so cannot check folder structure");
						}

					}else {
						log(LogStatus.ERROR, "tasks detail page tab is not clickable", YesNo.Yes);
						sa.assertTrue(false, "tasks detail page tab is not clickable");
					}
					if(i==0) {
						refresh(driver);
					}
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created fund : "+Smoke_Fund1, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created fund : "+Smoke_Fund1);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on fund tab so cannot check folder structure", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on fund tab so cannot check folder structure");
		}
		switchToDefaultContent(driver);
		if (lp.clickOnTab(environment, mode, TabName.TaskRayTab)) {
			log(LogStatus.PASS,"Clicked on TaskRay Tab", YesNo.No);
			ThreadSleep(10000);
			switchToFrame(driver, 60, task.getFrame(PageName.TaskRayPage, 60));
			if(task.clickOnBoardFilters(BoardFilters.My_Projects)) {
				log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
				String[] taskName= {Smoke_Task1Name};
				for(int i=0 ;i<taskName.length; i++) {
					if(task.clickOnCreatedProjectInTaskRay(Smoke_Project1Name)) {
						log(LogStatus.PASS, "clicked on created project name "+Smoke_Project1Name, YesNo.No);

						if(task.docubleClickOnCreatedTask(TaskType.Completed, taskName[i], Smoke_Project1Name)) {
							log(LogStatus.PASS, "clicked on created task "+taskName[i], YesNo.No);
							ThreadSleep(3000);
							if(task.clickOnNewTaskTabs(environment, mode, taskSubTab.Navatar_Documents, 60)) {
								log(LogStatus.PASS, "clicked on navatar documents tab", YesNo.No);
								ThreadSleep(3000);
								switchToFrame(driver, 20, fund.getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 20));	
								if (clickUsingJavaScript(driver, pipe.getTagOrUploadDocumentsButton(30), "tag document button")) {
									String parentId=switchOnWindow(driver);
									if(parentId!=null) {
										ThreadSleep(3000);
										WebElement ele= fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(60);
										if(ele!=null) {
											String msg=fund.getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(20).getText().trim();
											if (msg.contains(errorMsg)) {
												appLog.info("successfully verified tag docs error message : "+errorMsg);
											}
											else {
												log(LogStatus.ERROR, "could not verify tag docs error message: "+errorMsg, YesNo.Yes);
												sa.assertTrue(false, "could not verify tag docs error message: "+errorMsg);
											}
										}else {
											log(LogStatus.FAIL, "Error Message is nnot visible so cannot verify error message : "+errorMsg, YesNo.Yes);
											sa.assertTrue(false, "Error Message is nnot visible so cannot verify error message : "+errorMsg);
										}
										driver.close();
										driver.switchTo().window(parentId);

									}else {
										log(LogStatus.FAIL, "No new window is found so cannot check folder structure and upload file", YesNo.Yes);
										sa.assertTrue(false, "No new window is found so cannot check folder structure and upload file");
									}
									switchToFrame(driver, 20, task.getFrame(PageName.TaskRayPage, 20));
									if(task.clickOnCreateTaskCrossIcon()) {
										log(LogStatus.PASS, "clicked on cross icon of task "+taskName[i], YesNo.No);
									}else {
										log(LogStatus.FAIL, "Not able to click on cross icon so cannot close task popup : "+taskName[i], YesNo.Yes);
										sa.assertTrue(false, "Not able to click on cancel button so cannot close task popUp: "+taskName[i]);
									}
								}else {
									log(LogStatus.ERROR, "tag docs button is not clickable of task "+taskName[i], YesNo.Yes);
									sa.assertTrue(false, "tag docs button is not clickable of task : "+taskName[i]);
								}
							}else {
								log(LogStatus.FAIL, "Not able to click on navatar documents tab so cannot check folder structure on tag document window", YesNo.Yes);
								sa.assertTrue(false, "Not able to click on navatar documents tab so cannot check folder structure on tag document window");
							}

						}else {
							log(LogStatus.FAIL, "Not able to click on created task "+taskName[i], YesNo.Yes);
							sa.assertTrue(false, "Not able to click on created task "+taskName[i]);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on created project "+Smoke_Project1Name, YesNo.Yes);
						sa.assertTrue(false, "Not able to click on created project "+Smoke_Project1Name);
					}
					
				}
				
			}else {
				log(LogStatus.FAIL, "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task3Name, YesNo.Yes);
				sa.assertTrue(false,  "Not able to click on my projects side tab so cannot click on created task "+Smoke_Task1Name+","+Smoke_Task3Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on taskRay Tab so cannot check folder structure on navatar document of task "+Smoke_Task3Name, YesNo.Yes);
			sa.assertTrue(false, "Not able to click on taskRay Tab so cannot check folder structure on navatar document of task "+Smoke_Task1Name+","+Smoke_Task3Name);
		}
		switchToDefaultContent(driver);
		lp.CRMlogout(environment, mode);
		sa.assertAll();
	}
	
}
